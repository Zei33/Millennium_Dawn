﻿add_namespace = United_Kingdom
add_namespace = United_Kingdom_News

#Future of the Falklands
country_event = {
	id = United_Kingdom.1
	title = United_Kingdom.1.t
	desc = United_Kingdom.1.d
	picture = GFX_falkland_islands

	is_triggered_only = yes

	option = {	#No need to change.
		name = United_Kingdom.1.a
		log = "[GetDateText]: [This.GetName]: United_Kingdom.1.a executed"
		ai_chance = {
			base = 90
			# Give it to the subject maybe
			modifier = {
				factor = 0.5
				ARG = { is_subject_of = ENG }
			}
		}
	}

	option = {	#Return the Falklands
		name = United_Kingdom.1.b
		log = "[GetDateText]: [This.GetName]: United_Kingdom.1.b executed"
		add_war_support = -0.05
		news_event = { id = United_Kingdom_News.1 days = 1 }
		if = {
			limit = { has_full_control_of_state = 26 }
			ARG = { transfer_state = 26 }
		}
		ai_chance = {
			base = 10
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
	}
}

#Britain returns the Falklands
news_event = {
	id = United_Kingdom_News.1
	title = United_Kingdom_News.1.t
	desc = United_Kingdom_News.1.d
	picture = GFX_argentinian_flag

	is_triggered_only = yes
	major = yes

	option = {
		name = United_Kingdom_News.1.a
		log = "[GetDateText]: [This.GetName]: United_Kingdom_News.1.a executed"
	}
}