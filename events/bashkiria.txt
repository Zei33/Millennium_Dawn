﻿##########################
#  Bashkiria Events  #
##########################

add_namespace = bashkiriya
add_namespace = bashkiriya_export

country_event = {
	id = bashkiriya.0
	immediate = { log = "[GetDateText]: [Root.GetName]: event bashkiriya.0" }
	title = bashkiriya.0.t
	desc = bashkiriya.0.d
	picture = GFX_blizzard

	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = bashkiriya.0.a
		log = "[GetDateText]: [This.GetName]: bashkiriya.0.a executed"
		add_political_power = 25
	}
}

country_event = {
	id = bashkiriya.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event bashkiriya.1" }
	title = bashkiriya.1.t
	desc = bashkiriya.1.d
	picture = GFX_hospital

	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = bashkiriya.1.a
		log = "[GetDateText]: [This.GetName]: bashkiriya.1.a executed"
		add_political_power = 25
	}
}
country_event = {
	id = bashkiriya.2
	immediate = { log = "[GetDateText]: [Root.GetName]: event bashkiriya.2" }
	title = bashkiriya.2.t
	desc = bashkiriya.2.d
	picture = GFX_SOV_generic

	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Rus
		name = bashkiriya.2.a
		log = "[GetDateText]: [This.GetName]: bashkiriya.2.a executed"
		set_country_flag = BSH_we_choose_rus
		ai_chance = {
			base = 100
		}
	}
	option = {	#Ne Rus
		name = bashkiriya.2.b
		log = "[GetDateText]: [This.GetName]: bashkiriya.2.b executed"
		set_country_flag = BSH_we_choose_indep
		ai_chance = {
			base = 0
		}
	}
}
country_event = {
	id = bashkiriya.3
	immediate = { log = "[GetDateText]: [Root.GetName]: event bashkiriya.3" }
	title = bashkiriya.3.t
	desc = bashkiriya.3.d
	picture = GFX_SOV_generic

	fire_only_once = yes
	is_triggered_only = yes

	option = {	#War
		name = bashkiriya.3.a
		log = "[GetDateText]: [This.GetName]: bashkiriya.3.a executed"
		declare_war_on = {
			target = BSH
			type = annex_everything
		}
		ai_chance = {
			base = 100
		}
	}
	option = {	#Net
		name = bashkiriya.3.a
		log = "[GetDateText]: [This.GetName]: bashkiriya.3.b executed"
		ai_chance = {
			base = 0
		}
	}
}
country_event = {
	id = bashkiriya.4
	immediate = { log = "[GetDateText]: [Root.GetName]: event bashkiriya.4" }
	title = bashkiriya.4.t
	desc = bashkiriya.4.d
	picture = GFX_USA_generic

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = bashkiriya.4.a
		log = "[GetDateText]: [This.GetName]: bashkiriya.4.a executed"
		BSH = {
			add_ideas = USA_usaid
		set_temp_variable = { temp_opinion = 5 }
		change_international_bankers_opinion = yes
	}
		ai_chance = {
			base = 100
		}
	}
	option = {	#Net
		name = bashkiriya.4.a
		log = "[GetDateText]: [This.GetName]: bashkiriya.4.b executed"
		ai_chance = {
			base = 0
		}
	}
}
country_event = {
	id = bashkiriya_export.2
	title = bashkiriya_export.2.t
	desc = bashkiriya_export.2.d
	picture = GFX_handshake_black

	is_triggered_only = yes

	option = {
		name = bashkiriya_export.2.a
		log = "[GetDateText]: [This.GetName]: bashkiriya_export.2.a executed"
		set_temp_variable = { treasury_change = -0.5 }
		modify_treasury_effect = yes
			add_timed_idea = {
				idea = BSH_buses
				days = 1080
			}
		
		BSH = {
			clr_country_flag = BSH_pending_offer
			country_event = { id = bashkiriya_export.7	days = 1 }
		}
		ai_chance = {
			base = 1

			modifier = {
				THIS = { has_opinion = { target = BSH value > 10 } }
				add = 10
			}
			modifier = {
				is_top5_influencer = yes
				add = 10
			}
		}
	}
	option = {
		name = bashkiriya_export.2.b
		log = "[GetDateText]: [This.GetName]: bashkiriya_export.2.b executed"
		BSH = {
			clr_country_flag = BSH_pending_offer
			country_event = { id = bashkiriya_export.8	days = 1 }
		}
		ai_chance = {
			base = 1

		}
	}
	
}

#Export Choose Buses
country_event = {
	id = bashkiriya_export.7
	title = bashkiriya_export.7.t
	desc = bashkiriya_export.7.d
	
	picture = GFX_message_signing
	is_triggered_only = yes

	option = {
		name = bashkiriya_export.7.a
		log = "[GetDateText]: [This.GetName]: bashkiriya_export.7.a executed"
		set_temp_variable = { treasury_change = 0.5 }
		modify_treasury_effect = yes
		set_temp_variable = { modify_bus = -1000 }
		modify_bus_support = yes
	}
}
#Export Choose Buses
country_event = {
	id = bashkiriya_export.8
	title = bashkiriya_export.8.t
	desc = bashkiriya_export.8.d
	
	picture = GFX_declined_offer_no
	is_triggered_only = yes

	option = {
		name = bashkiriya_export.8.a
		log = "[GetDateText]: [This.GetName]: bashkiriya_export.8.a executed"
		
	}
}
#Bashneft
country_event = {
	id = bashkiriya_export.22
	title = bashkiriya_export.22.t
	desc = bashkiriya_export.22.d
	is_triggered_only = yes
	option = {
		name = bashkiriya_export.22.a
		log = "[GetDateText]: [This.GetName]: bashkiriya_export.22.a executed"
		capital_scope = {
			add_resource = {
				type = oil
				amount = 4
			}
		}
		set_temp_variable = { treasury_change = -3.75 }
		modify_treasury_effect = yes
		BSH = { country_event = { id = bashkiriya_export.23 days = 1 } }
		if = {
			limit = { TAT = { has_autonomy_state = autonomy_republic_rf } }
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = ROOT }
			change_influence_percentage = yes
		}
		set_temp_variable = { percent_change = 3 }
		set_temp_variable = { tag_index = BSH }
		set_temp_variable = { influence_target = ROOT }
		change_influence_percentage = yes
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = bashkiriya_export.22.b
		log = "[GetDateText]: [This.GetName]: bashkiriya_export.22.b executed"
		ai_chance = {
			base = 0
		}
	}
}
#Bashneft
country_event = {
	id = bashkiriya_export.23
	title = bashkiriya_export.23.t
	desc = bashkiriya_export.23.d
	is_triggered_only = yes
	option = {
		name = bashkiriya_export.23.a
		log = "[GetDateText]: [This.GetName]: bashkiriya_export.23.a executed"
		set_temp_variable = { treasury_change = 3.75 }
		modify_treasury_effect = yes
		if = {
			limit = { BSH = { has_autonomy_state = autonomy_republic_rf } }
			set_temp_variable = { modify_subjectavtoritet = 3 }
			modify_subjectavtoritet_support = yes
		}
	}
}


