﻿##########################
#  Chechnya Events  #
##########################

add_namespace = chechnya

country_event = {
	id = chechnya.0
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.0" }
	title = chechnya.0.t
	desc = chechnya.0.d


	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.0.a
		log = "[GetDateText]: [This.GetName]: chechnya.0.a executed"
		CHE = { country_event = diplomatic_response.1 }
		CHE = {
		add_ideas = CHE_religion_turism
		}
		PAL = {
			add_ideas = CHE_religion_turism
		}
		set_temp_variable = { percent_change = 2.11 }
		set_temp_variable = { tag_index = CHE }
		set_temp_variable = { influence_target = PAL }
		change_influence_percentage = yes
		set_temp_variable = { percent_change = 2.11 }
		set_temp_variable = { tag_index = PAL }
		set_temp_variable = { influence_target = CHE }
		change_influence_percentage = yes
		ai_chance = {
			base = 50

		}
	}
	option = {	#Never
		name = chechnya.0.b
		log = "[GetDateText]: [This.GetName]: chechnya.0.b executed"
		CHE = { country_event = diplomatic_response.2 }
		ai_chance = {
			base = 50

		}
	}
}
###Poland Attack suggest
country_event = {
	id = chechnya.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.1" }
	title = chechnya.1.t
	desc = chechnya.1.d

	picture = GFX_chechnya_rage_kadyrov
	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.1.a
		log = "[GetDateText]: [This.GetName]: chechnya.1.a executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -20 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.1
		}
		create_wargoal = {
			type = annex_everything
			target = POL
		}
		ai_chance = {
			base = 10

		}
	}
	option = {	#Never
		name = chechnya.1.b
		log = "[GetDateText]: [This.GetName]: chechnya.1.b executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -10 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.2
		}
		ai_chance = {
			base = 90

		}
	}
}
###Georgia Attack Suggest
country_event = {
	id = chechnya.2
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.2" }
	title = chechnya.2.t
	desc = chechnya.2.d

	picture = GFX_chechnya_rage_kadyrov
	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.2.a
		log = "[GetDateText]: [This.GetName]: chechnya.2.a executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -20 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.1
		}
		create_wargoal = {
			type = annex_everything
			target = GEO
		}
		ai_chance = {
			base = 10

		}
	}
	option = {	#Never
		name = chechnya.2.b
		log = "[GetDateText]: [This.GetName]: chechnya.2.b executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -10 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.2
		}
		ai_chance = {
			base = 90

		}
	}
}
####Attack Belorussia Suggest
country_event = {
	id = chechnya.3
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.3" }
	title = chechnya.3.t
	desc = chechnya.3.d

	picture = GFX_chechnya_rage_kadyrov
	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.3.a
		log = "[GetDateText]: [This.GetName]: chechnya.3.a executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -20 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.1
		}
		create_wargoal = {
			type = annex_everything
			target = BLR
		}
		ai_chance = {
			base = 10

		}
	}
	option = {	#Never
		name = chechnya.3.b
		log = "[GetDateText]: [This.GetName]: chechnya.3.b executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -10 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.2
		}
		ai_chance = {
			base = 90

		}
	}
}

country_event = {
	id = chechnya.4
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.4" }
	title = chechnya.4.t
	desc = chechnya.4.d
	picture = GFX_chehcnya_karydov_tsar

	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.4.a
		log = "[GetDateText]: [This.GetName]: chechnya.4.a executed"
		add_stability = 0.02
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.5
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.5" }
	title = chechnya.5.t
	desc = chechnya.5.d
	picture = GFX_chechnya_ros_gvardia

	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.5.a
		log = "[GetDateText]: [This.GetName]: chechnya.5.a executed"
		add_stability = 0.02
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.6
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.6" }
	title = chechnya.6.t
	desc = chechnya.6.d
	picture = GFX_chechnya_ahmat_battalion

	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.6.a
		log = "[GetDateText]: [This.GetName]: chechnya.6.a executed"
		add_stability = 0.01
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.7
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.7" }
	title = chechnya.7.t
	desc = chechnya.7.d
	picture = GFX_checnya_gudermes

	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.7.a
		log = "[GetDateText]: [This.GetName]: chechnya.7.a executed"
		add_stability = 0.01
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.8
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.8" }
	title = chechnya.8.t
	desc = chechnya.8.d
	picture = GFX_checnya_ahmat_bronemachine

	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.8.a
		log = "[GetDateText]: [This.GetName]: chechnya.8.a executed"
		add_stability = 0.01
		set_technology = { util_vehicle_6 = 1 }
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.9
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.9" }
	title = chechnya.9.t
	desc = chechnya.9.d
	picture = GFX_chehcnya_djahd_mobile

	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.9.a
		log = "[GetDateText]: [This.GetName]: chechnya.9.a executed"
		add_tech_bonus = {
			name = CHE_Chabor
			bonus = 0.10
			uses = 1
			category = CAT_util
		}
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.10
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.10" }
	title = chechnya.10.t
	desc = chechnya.10.d
	picture = GFX_checnya_bolat

	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.10.a
		log = "[GetDateText]: [This.GetName]: chechnya.10.a executed"
		add_tech_bonus = {
			name = CHE_MAC
			bonus = 0.15
			uses = 1
			category = CAT_util
		}
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.11
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.11" }
	title = chechnya.11.t
	desc = chechnya.11.d

	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.11.a
		log = "[GetDateText]: [This.GetName]: chechnya.11.a executed"
		kill_country_leader = yes
		add_stability = -0.25
		SOV = {
			declare_war_on = {
			target = CHE
			type = annex_everything
		}
		}
		hidden_effect = {
		release = CHE
		}
		ai_chance = {
			base = 90

		}
	}

}

country_event = {
	id = chechnya.12
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.12" }
	title = chechnya.12.t
	desc = chechnya.12.d

	picture = GFX_chechnya_rage_kadyrov
	fire_only_once = yes
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.1.a
		log = "[GetDateText]: [This.GetName]: chechnya.12.a executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -20 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.1
		 }
		create_wargoal = {
			type = annex_everything
			target = UKR
		}
		ai_chance = {
			base = 10

		}
	}
	option = {	#Never
		name = chechnya.12.b
		log = "[GetDateText]: [This.GetName]: chechnya.12.b executed"
		CHE = {
			set_temp_variable = { modify_subjectavtoritet = -10 }
			modify_subjectavtoritet_support = yes
			country_event = diplomatic_response.2
		 }
		ai_chance = {
			base = 90

		}
	}
}




country_event = {
	id = chechnya.14
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.13" }
	title = chechnya.14.t
	desc = chechnya.14.d
	picture = GFX_USA_generic
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.14.a
		log = "[GetDateText]: [This.GetName]: chechnya.14.a executed"
		ai_chance = {
		base = 55
		}
		CHE = {
			country_event = { id = chechnya.15 days = 1 }
		}

	}

	option = {	#No
		name = chechnya.14.b
		log = "[GetDateText]: [This.GetName]: chechnya.14.b executed"

		ai_chance = {
			base = 45

		}
	}
}

country_event = {
	id = chechnya.15
	immediate = { log = "[GetDateText]: [Root.GetName]: event chechnya.15" }
	title = chechnya.15.t
	desc = chechnya.15.d
	picture = GFX_USA_generic
	is_triggered_only = yes

	option = {	#Accept
		name = chechnya.13.a
		log = "[GetDateText]: [This.GetName]: chechnya.15.a executed"
		ai_chance = {
		base = 55
		}
		add_ideas = USA_usaid
		set_temp_variable = { temp_opinion = 5 }
		change_international_bankers_opinion = yes
	}
}
country_event = {
	id = chechnya.16
	title = chechnya.16.t
	desc = chechnya.16.d

	is_triggered_only = yes

	option = {
		name = chechnya.16.a
		log = "[GetDateText]: [This.GetName]:chechnya.16.a executed"
		random_list = {
			70 = {
				add_equipment_to_stockpile = {
					type = infantry_weapons1
					amount = 500
					producer = SOV
				}
				add_equipment_to_stockpile = {
					type = command_control_equipment
					amount = 250
					producer = SOV
				}
				add_equipment_to_stockpile = {
					type = util_vehicle_2
					amount = 50
					producer = SOV
				}
			}
			30 = {
				add_equipment_to_stockpile = {
					type = infantry_weapons1
					amount = 100
					producer = SOV
				}
				add_equipment_to_stockpile = {
					type = command_control_equipment
					amount = 50
					producer = SOV
				}
			}
		}
	}

}
country_event = {
	id = chechnya.17
	title = chechnya.17.t
	desc = chechnya.17.d

	is_triggered_only = yes

	option = {
		name = chechnya.17.a
		log = "[GetDateText]: [This.GetName]:chechnya.17.a executed"
		DAG = {
			if = { limit = { NOT = { is_in_array = { ruling_party = 11 } } }
				set_temp_variable = { rul_party_temp = 11 }
				change_ruling_party_effect = yes
				set_politics = {
					ruling_party = fascism
					elections_allowed = yes
				}
			}
			add_popularity = {
				ideology = fascism
				popularity = 0.25
			}
			transfer_state = 671
			country_event = { id = chechnya.23 days = 1 }
		}
		CHE = { puppet = DAG }
	}
}
country_event = {
	id = chechnya.18
	title = chechnya.18.t
	desc = chechnya.18.d

	is_triggered_only = yes

	option = {
		name = chechnya.18.a
		log = "[GetDateText]: [This.GetName]: chechnya.18.a executed"
		SOV = {
			add_timed_idea = { idea = CHE_russian_unstability days = 1090 }
			white_peace = CHE
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = chechnya.18.b
		log = "[GetDateText]: [This.GetName]: chechnya.18.a executed"
		ai_chance = {
			base = 0
		}
	}
}
country_event = {
	id = chechnya.19
	title = chechnya.19.t
	desc = chechnya.19.d

	is_triggered_only = yes

	option = {
		name = chechnya.19.a
		log = "[GetDateText]: [This.GetName]: chechnya.19.a executed"
		add_opinion_modifier = { target = CHE modifier = diplomatic_support }
		reverse_add_opinion_modifier = { target = CHE modifier = diplomatic_support }
		diplomatic_relation = {
			country = CHE
			relation = non_aggression_pact
		}
		CHE = {
			country_event = { id = chechnya.20 days = 1 }
			add_ai_strategy = {
				type = befriend
				id = "SOV"
				value = 100
			}
			add_ai_strategy = {
				type = support
				id = "SOV"
				value = 100
			}
			add_ai_strategy = {
				type = alliance
				id = "SOV"
				value = 150
			}
		}
	}
	option = {
		name = chechnya.19.b
		log = "[GetDateText]: [This.GetName]: chechnya.19.b executed"

	}
}
country_event = {
	id = chechnya.20
	title = chechnya.20.t
	desc = chechnya.20.d

	is_triggered_only = yes

	option = {
		name = chechnya.20.a
		log = "[GetDateText]: [This.GetName]: chechnya.20.a executed"
		add_opinion_modifier = { target = SOV modifier = diplomatic_support }
		reverse_add_opinion_modifier = { target = SOV modifier = diplomatic_support }
		diplomatic_relation = {
			country = SOV
			relation = non_aggression_pact
		}
		SOV = {
			add_ai_strategy = {
				type = befriend
				id = "CHE"
				value = 100
			}
			add_ai_strategy = {
				type = support
				id = "CHE"
				value = 100
			}
			add_ai_strategy = {
				type = alliance
				id = "CHE"
				value = 150
			}
		}
	}
}
country_event = {
	id = chechnya.21
	title = chechnya.21.t
	desc = chechnya.21.d

	is_triggered_only = yes

	option = {
		name = chechnya.20.a
		log = "[GetDateText]: [This.GetName]: chechnya.21.a executed"
		CHE = { puppet = GEO }
	}
}
country_event = {
	id = chechnya.22
	title = chechnya.22.t
	desc = chechnya.22.d

	is_triggered_only = yes

	option = {
		name = chechnya.22.a
		log = "[GetDateText]: [This.GetName]: chechnya.22.a executed"
		add_opinion_modifier = { target = CHE modifier = diplomatic_support }
		reverse_add_opinion_modifier = { target = CHE modifier = diplomatic_support }
		ROOT = {
			add_ai_strategy = {
				type = befriend
				id = "CHE"
				value = 100
			}
			add_ai_strategy = {
				type = support
				id = "CHE"
				value = 100
			}
			add_ai_strategy = {
				type = alliance
				id = "CHE"
				value = 150
			}
			add_ai_strategy = {
				type = send_volunteers_desire
				id = "CHE"
				value = 150
			}
		}
	}
	option = {
		name = chechnya.22.b
		log = "[GetDateText]: [This.GetName]: chechnya.22.a executed"
		add_opinion_modifier = { target = CHE modifier = recent_actions_positive }
		reverse_add_opinion_modifier = { target = CHE modifier = recent_actions_positive }

	}
}
country_event = {
	id = chechnya.23
	title = chechnya.23.t
	desc = chechnya.23.d

	is_triggered_only = yes

	option = {
		name = chechnya.23.a
		log = "[GetDateText]: [This.GetName]: chechnya.23.a executed"
		add_manpower = 1000
		division_template = {
			name = "Volunteer Brigade"
			regiments = {
				Mot_Inf_Bat = { x = 0 y = 0 }
				Special_Forces = { x = 0 y = 1 }
				Special_Forces = { x = 1 y = 0 }

			}
			priority = 2
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 673 }
			create_unit = {
				division = "name = \"Rebels Battalion\" division_template = \"Volunteer Brigade\" start_experience_factor = 0.1"
				owner = ROOT
			}
		}
		SOV = {
			release = ROOT
		}
		CHE = {
			set_autonomy = {
				target = ROOT
				autonomy_state = autonomy_terrorist_branch
			}
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 673 }
			create_unit = {
				division = "name = \"Rebels Battalion\" division_template = \"Volunteer Brigade\" start_experience_factor = 0.1"
				owner = ROOT
			}
		}
		declare_war_on = {
			target = SOV
			type = annex_everything
		}
	}
}
country_event = {
	id = chechnya.24
	title = chechnya.24.t
	desc = chechnya.24.d

	is_triggered_only = yes

	option = {
		name = chechnya.24.a
		log = "[GetDateText]: [This.GetName]: chechnya.24.a executed"
		if = {
			limit = { SOV = { owns_state = 671 } }
			add_political_power = -100
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -500
				producer = CHE
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment
				amount = -100
				producer = CHE
			}
			DAG = {
				country_event = { id = chechnya.23 }
				if = { limit = { NOT = { is_in_array = { ruling_party = 11 } } }
				set_temp_variable = { rul_party_temp = 11 }
				change_ruling_party_effect = yes
				set_politics = {
					ruling_party = fascism
					elections_allowed = yes
				}
				}
				add_popularity = {
					ideology = fascism
					popularity = 0.25
				}
				transfer_state = 671
			}
		}
		if = {
			limit = { SOV = { owns_state = 673 } }
			add_manpower = -1000
			add_political_power = -100
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -500
				producer = CHE
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment
				amount = -100
				producer = CHE
			}
			SOO = {
				country_event = { id = chechnya.23 }
				if = { limit = { NOT = { is_in_array = { ruling_party = 11 } } }
				set_temp_variable = { rul_party_temp = 11 }
				change_ruling_party_effect = yes
				set_politics = {
					ruling_party = fascism
					elections_allowed = yes
				}
				}
				add_popularity = {
					ideology = fascism
					popularity = 0.25
				}
				transfer_state = 673
			}
		}
		if = {
			limit = { SOV = { owns_state = 1126 } }
			add_manpower = -1000
			add_political_power = -100
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -500
				producer = CHE
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment
				amount = -100
				producer = CHE
			}
			ING = {
				country_event = { id = chechnya.23 }
				if = { limit = { NOT = { is_in_array = { ruling_party = 11 } } }
				set_temp_variable = { rul_party_temp = 11 }
				change_ruling_party_effect = yes
				set_politics = {
					ruling_party = fascism
					elections_allowed = yes
				}
				}
				add_popularity = {
					ideology = fascism
					popularity = 0.25
				}
				transfer_state = 1126
			}
		}
		if = {
			limit = { SOV = { owns_state = 674 } }
			add_manpower = -1000
			add_political_power = -100
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -500
				producer = CHE
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment
				amount = -100
				producer = CHE
			}
			KBK = {
				country_event = { id = chechnya.23 }
				if = { limit = { NOT = { is_in_array = { ruling_party = 11 } } }
				set_temp_variable = { rul_party_temp = 11 }
				change_ruling_party_effect = yes
				set_politics = {
					ruling_party = fascism
					elections_allowed = yes
				}
				}
				add_popularity = {
					ideology = fascism
					popularity = 0.25
				}
				transfer_state = 674
			}
		}
		if = {
			limit = { SOV = { owns_state = 1125 } }
			add_manpower = -1000
			add_political_power = -100
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -500
				producer = CHE
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment
				amount = -100
				producer = CHE
			}
			KCC = {
				country_event = { id = chechnya.23 }
				if = { limit = { NOT = { is_in_array = { ruling_party = 11 } } }
				set_temp_variable = { rul_party_temp = 11 }
				change_ruling_party_effect = yes
				set_politics = {
					ruling_party = fascism
					elections_allowed = yes
				}
				}
				add_popularity = {
					ideology = fascism
					popularity = 0.25
				}
				transfer_state = 1125
			}
		}
	}
}
country_event = {
	id = chechnya.25
	title = chechnya.25.t
	desc = chechnya.25.d
	picture = GFX_bin_laden
	is_triggered_only = yes

	option = {
		name = chechnya.25.a
		log = "[GetDateText]: [This.GetName]: chechnya.25.a executed"
		add_popularity = {
			ideology = fascism
			popularity = 0.05
		}
		add_political_power = 100
	}
}
country_event = {
	id = chechnya.26
	title = chechnya.26.t
	desc = chechnya.26.d
	is_triggered_only = yes
	picture = GFX_belarus_silesia
	option = {
		name = chechnya.26.a
		log = "[GetDateText]: [This.GetName]: chechnya.26.a executed"
		unlock_decision_category_tooltip = SIL_bund_category
		SOV = { set_country_flag = silesia_start }
		hidden_effect = {
			POL = {
			set_temp_variable = { modify_silesiabund = 45 }
			modify_silesiabund_support = yes
			}
		}
		ai_chance = {
			base = 75
		}
	}
	option = {
		name = chechnya.26.b
		log = "[GetDateText]: [This.GetName]: chechnya.26.b executed"
		ai_chance = {
			base = 0
		}
	}
}

country_event = {
	id = chechnya.27
	title = chechnya.27.t
	desc = chechnya.27.d
	is_triggered_only = yes
	picture = GFX_belarus_silesia
	option = {
		name = chechnya.27.a
		log = "[GetDateText]: [This.GetName]: chechnya.27.a executed"
		SIL = {
			transfer_state = 115
			transfer_state = 626
		}
		set_temp_variable = { percent_change = 15 }
		set_temp_variable = { tag_index = SOV }
		set_temp_variable = { influence_target = SIL }
		change_influence_percentage = yes
		SIL = {
			add_timed_idea = { idea = SIL_defence_republic days = 120 }
			country_event = { id = chechnya.28 }
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = chechnya.28
	title = chechnya.28.t
	desc = chechnya.28.d
	is_triggered_only = yes
	picture = GFX_belarus_silesia
	option = {
		name = chechnya.28.a
		log = "[GetDateText]: [This.GetName]: chechnya.28.a executed"
		hidden_effect = {
			add_manpower = 3500
		}
		division_template = {
			name = "Silesian Militia"
			regiments = {
				Militia_Bat = { x = 0 y = 0 }
				Militia_Bat = { x = 0 y = 1 }
				Militia_Bat = { x = 1 y = 0 }
				Militia_Bat = { x = 1 y = 1 }
			}
			priority = 2
		}
		SIL = {
			declare_war_on = {
			target = POL
			type = annex_everything
		}
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 115 }
			create_unit = {
				division = "name = \"Silesian Militia\" division_template = \"Silesian Militia\" start_experience_base = 0.1"
				owner = SIL
			}
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 115 }
			create_unit = {
				division = "name = \"Silesian Militia\" division_template = \"Silesian Militia\" start_experience_base = 0.1"
				owner = SIL
			}
		}
		if = {
			limit = {
				POL = { has_country_flag = SIL_chechen_help }
			}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 115 }
			create_unit = {
				division = "name = \"Silesian Militia\" division_template = \"Silesian Militia\" start_experience_base = 0.1"
				owner = SIL
			}
		}
	}
	if = {
		limit = {
			POL = { has_country_flag = SIL_wagner_help }
		}
		SIL = {
		spawn_light_infantry_wagner = yes
	}
}
}
}




