default = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_apc_hull_0_a
				GFX_apc_hull_0_b
				GFX_USA_APC_1
				GFX_ENG_APC_1
				GFX_GER_APC_1
				GFX_CHI_APC_1
				GFX_FRA_APC_1
				GFX_JAP_APC_1
				GFX_SWE_APC_1
				GFX_GRE_APC_1
				GFX_TUR_APC_1
				GFX_PER_APC_1
				GFX_ITA_APC_1
				GFX_KOR_APC_1
				GFX_POL_APC_1
				GFX_apc_hull_0_medium
			}
			models = {
				"KOR_APC_K200_mesh_entity"
				"ENG_APC_FV_105_Sultan_mesh_entity"
				"FRA_APC_VAB_mesh_entity"
				"AAVP7_entity"
				"USA_M113A3_vehicle_entity"
				"btr80_vehicle_entity"
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_apc_hull_1_a
				GFX_apc_hull_1_b
				GFX_USA_APC_3
				GFX_USA_APC_4
				GFX_ENG_APC_3
				GFX_GER_APC_3
				GFX_CHI_APC_3
				GFX_FRA_APC_3
				GFX_JAP_APC_3
				GFX_SWE_APC_3
				GFX_GRE_APC_3
				GFX_TUR_APC_3
				GFX_PER_APC_3
				GFX_ITA_APC_3
				GFX_KOR_APC_3
				GFX_POL_APC_3
				GFX_apc_hull_1_medium
			}
			models = {
				"KOR_APC_K200_mesh_entity"
				"GER_APC_Fuchs_vehicle_entity"
				"ENG_APC_FV_105_Sultan_mesh_entity"
				"AAVP7_entity"
				"FRA_APC_VAB_mesh_entity"
				"USA_M113A3_vehicle_entity"
				"btr80_vehicle_entity"
				"btr80a_vehicle_entity"
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_apc_hull_2_a
				GFX_apc_hull_2_b
				GFX_USA_APC_4
				GFX_USA_APC_5
				GFX_ENG_APC_4
				GFX_ENG_APC_5
				GFX_GER_APC_4
				GFX_GER_APC_5
				GFX_CHI_APC_4
				GFX_FRA_APC_3
				GFX_FRA_APC_4
				GFX_JAP_APC_4
				GFX_SWE_APC_2
				GFX_SWE_APC_4
				GFX_GRE_APC_4
				GFX_TUR_APC_4
				GFX_PER_APC_4
				GFX_ITA_APC_4
				GFX_KOR_APC_4
				GFX_KOR_APC_6
				GFX_POL_APC_4
				GFX_SAF_APC_3
				GFX_SOV_APC_5
				GFX_apc_hull_2_medium
			}
			models = {
				"KOR_APC_K200_mesh_entity"
				"GER_APC_Fuchs_vehicle_entity"
				"FRA_APC_VAB_mesh_entity"
				"Strykerapc_vehicle_entity"
				"btr80_vehicle_entity"
				"btr80a_vehicle_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_apc_hull_3_a
				GFX_apc_hull_3_b
				GFX_ENG_APC_6
				GFX_FRA_APC_6
				GFX_GER_APC_6
				GFX_USA_APC_5
				GFX_ITA_APC_6
				GFX_ITA_APC_7
				GFX_SAF_APC_3
				GFX_SOV_APC_5
				GFX_SWE_APC_6
				GFX_apc_hull_3_medium
			}
			models = {
				"KOR_APC_K200_mesh_entity"
				"GER_APC_Fuchs_vehicle_entity"
				"FRA_APC_VAB_mesh_entity"
				"Strykerapc_vehicle_entity"
				"btr80_vehicle_entity"
				"btr80a_vehicle_entity"
				"boxer_vehicle_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_apc_hull_3_a
				GFX_apc_hull_3_b
				GFX_ENG_APC_7
				GFX_FRA_APC_6
				GFX_GER_APC_7
				GFX_USA_APC_6
				GFX_ITA_APC_6
				GFX_ITA_APC_7
				GFX_KOR_APC_7
				GFX_SWE_APC_6
				GFX_apc_hull_4_medium
			}
			models = {
				"KOR_APC_K200_mesh_entity"
				"GER_APC_Fuchs_vehicle_entity"
				"FRA_APC_VAB_mesh_entity"
				"Strykerapc_vehicle_entity"
				"btr80_vehicle_entity"
				"btr80a_vehicle_entity"
				"boxer_vehicle_entity"
			}
		}
	}

	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_ifv_hull_0_a
				GFX_ifv_hull_0_b
				GFX_USA_IFV_1
				GFX_ENG_IFV_1
				GFX_GER_IFV_1
				GFX_CHI_IFV_1
				GFX_FRA_IFV_1
				GFX_JAP_IFV_1
				GFX_SWE_IFV_1
				GFX_GRE_IFV_1
				GFX_PER_IFV_1
				GFX_POL_IFV_1
			}
			models = {
				"CHI_MD4_light_armor_entity_2"
				"commonwealth_gfx_MD4_light_armor_entity_2"
				"GER_MD4_light_armor_entity"
				"JAP_MD4_light_armor_entity"
				"LAV25_vehicle_entity"
				"SOV_BMP2_entity"
				"SOV_BMP1_entity"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_ifv_hull_1_a
				GFX_ifv_hull_1_b
				GFX_USA_IFV_3
				GFX_ENG_IFV_3
				GFX_GER_IFV_3
				GFX_CHI_IFV_3
				GFX_FRA_IFV_2
				GFX_FRA_IFV_3
				GFX_JAP_IFV_3
				GFX_SWE_IFV_3
				GFX_GRE_IFV_3
				GFX_ITA_IFV_3
				GFX_POL_IFV_3
				GFX_TUR_IFV_3
			}
			models = {
				"CHI_MD4_light_armor_entity_2"
				"commonwealth_gfx_MD4_light_armor_entity"
				"commonwealth_gfx_MD4_light_armor_entity_2"
				"GER_MD4_light_armor_entity"
				"JAP_MD4_light_armor_entity"
				"SOV_BMD2_entity"
				"LAV25_vehicle_entity"
				"USA_M2A2bradley_entity"
				"SOV_BMP1_entity"
				"SOV_BMP2_entity"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_ifv_hull_2_a
				GFX_ifv_hull_2_b
				GFX_USA_IFV_4
				GFX_USA_IFV_5
				GFX_ENG_IFV_4
				GFX_ENG_IFV_5
				GFX_GER_IFV_4
				GFX_CHI_IFV_4
				GFX_FRA_IFV_4
				GFX_FRA_IFV_5
				GFX_JAP_IFV_4
				GFX_SWE_IFV_4
				GFX_SWE_IFV_5
				GFX_GRE_IFV_4
				GFX_ITA_IFV_4
				GFX_POL_IFV_4
				GFX_TUR_IFV_4
				GFX_KOR_IFV_6
			}
			models = {
				"CHI_MD4_light_armor_entity"
				"CHI_MD4_light_armor_entity_2"
				"commonwealth_gfx_MD4_light_armor_entity"
				"GER_MD4_light_armor_entity"
				"JAP_MD4_light_armor_entity"
				"KOR_MD4_light_armor_entity"
				"LAV25_vehicle_entity"
				"SOV_BMP3_entity"
				"UKR_MD4_light_armor_entity"
				"USA_M2A2bradley_entity"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_ifv_hull_3_a
				GFX_ifv_hull_3_b
				GFX_USA_IFV_6
				GFX_ENG_IFV_6
				GFX_FRA_IFV_5
				GFX_GER_IFV_6
				GFX_GRE_IFV_5
				GFX_ITA_IFV_5
				GFX_KOR_IFV_7
				GFX_SWE_IFV_5
				GFX_SWE_IFV_6
			}
			models = {
				"SOV_T15_entity"
				"GER_MD4_light_armor_entity_2"
				"CHI_MD4_light_armor_entity_3"
				"USA_M7A1dunham_entity"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_ifv_hull_3_a
				GFX_ifv_hull_3_b
				GFX_ENG_IFV_6
				GFX_ENG_IFV_7
				GFX_FRA_IFV_8
				GFX_GER_IFV_5
				GFX_GER_IFV_6
				GFX_USA_IFV_6
				GFX_ITA_IFV_5
				GFX_SWE_IFV_6
				GFX_SWE_IFV_7
				GFX_KOR_IFV_7
				GFX_KOR_IFV_8
				GFX_TUR_IFV_7
			}
			models = {
				"SOV_T15_entity"
				"GER_MD4_light_armor_entity_2"
				"CHI_MD4_light_armor_entity_3"
				"USA_M7A1dunham_entity"
			}
		}
	}
}

CHI = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_CHI_APC_1
				GFX_CHI_APC_2
			}
			models = {
			
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_CHI_APC_3
			}
			models = {
			
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_CHI_APC_4
				GFX_CHI_APC_5
			}
			models = {
			"CHI_MD4_light_armor_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_CHI_APC_6
				GFX_CHI_APC_7
			}
			models = {
			"CHI_MD4_light_armor_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_CHI_APC_8
				GFX_CHI_APC_7
			}
			models = {
			"CHI_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_CHI_IFV_1
				GFX_CHI_IFV_2
			}
			models = {
				"CHI_MD4_light_armor_entity"
				"CHI_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_CHI_IFV_3
			}
			models = {
				"CHI_MD4_light_armor_entity"
				"CHI_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_CHI_IFV_4
				GFX_CHI_IFV_5
			}
			models = {
				"CHI_MD4_light_armor_entity"
				"CHI_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_CHI_IFV_5
				GFX_CHI_IFV_6
			}
			models = {
				"CHI_MD4_light_armor_entity"
				"CHI_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_CHI_IFV_7
				GFX_CHI_IFV_8
			}
			models = {
				"CHI_MD4_light_armor_entity"
				"CHI_MD4_light_armor_entity_2"
			}
		}
	}
}

DEN = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_DEN_APC_1
				GFX_DEN_APC_2
			}
			models = {
			
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_DEN_APC_3
			}
			models = {
			
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_DEN_APC_4
				GFX_DEN_APC_5
			}
			models = {
			
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_DEN_APC_6
			}
			models = {
			
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_DEN_APC_7
			}
			models = {
			
			}
		}
	}
}

ENG = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_ENG_APC_1
				GFX_ENG_APC_2
			}
			models = {
				"ENG_APC_FV_105_Sultan_mesh_entity"
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_ENG_APC_3
				GFX_ENG_APC_4
			}
			models = {
				"ENG_APC_FV_105_Sultan_mesh_entity"
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_ENG_APC_5
			}
			models = {
				"ENG_APC_FV_105_Sultan_mesh_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_ENG_APC_6
				GFX_ENG_APC_7
			}
			models = {
				"ENG_APC_FV_105_Sultan_mesh_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_ENG_APC_8
			}
			models = {
				"ENG_APC_FV_105_Sultan_mesh_entity"
			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_ENG_IFV_1
				GFX_ENG_IFV_2
			}
			models = {
				"commonwealth_gfx_MD4_light_armor_entity"
				"commonwealth_gfx_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_ENG_IFV_3
				GFX_ENG_IFV_4
			}
			models = {
				"commonwealth_gfx_MD4_light_armor_entity"
				"commonwealth_gfx_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_ENG_IFV_5
			}
			models = {
				"commonwealth_gfx_MD4_light_armor_entity"
				"commonwealth_gfx_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_ENG_IFV_6
				GFX_ENG_IFV_7
			}
			models = {
				"commonwealth_gfx_MD4_light_armor_entity"
				"commonwealth_gfx_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_ENG_IFV_8
				GFX_ENG_IFV_7
			}
			models = {
				"commonwealth_gfx_MD4_light_armor_entity"
				"commonwealth_gfx_MD4_light_armor_entity_2"
			}
		}
	}
}

FRA = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_FRA_APC_1
				GFX_FRA_APC_2
			}
			models = {
				"FRA_APC_VAB_mesh_entity"
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_FRA_APC_2
				GFX_FRA_APC_3
				GFX_FRA_APC_4
			}
			models = {
				"FRA_APC_VAB_mesh_entity"
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_FRA_APC_4
				GFX_FRA_APC_5
			}
			models = {
				"FRA_APC_VAB_mesh_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_FRA_APC_4
				GFX_FRA_APC_5
				GFX_FRA_APC_6
			}
			models = {
				"FRA_APC_VAB_mesh_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_FRA_APC_6
				GFX_FRA_APC_8
			}
			models = {
				"FRA_APC_VAB_mesh_entity"
			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_FRA_IFV_1
				GFX_FRA_IFV_3
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_FRA_IFV_2
				GFX_FRA_IFV_4
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_FRA_IFV_5
				GFX_FRA_IFV_4
			}
			models = {

			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_FRA_IFV_7
				GFX_FRA_IFV_5
			}
			models = {

			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_FRA_IFV_8
			}
			models = {

			}
		}
	}
}

GER = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_GER_APC_1
				GFX_GER_APC_2
			}
			models = {
				"GER_APC_Fuchs_vehicle_entity"
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_GER_APC_3
				GFX_GER_APC_4
			}
			models = {
				"GER_APC_Fuchs_vehicle_entity"
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_GER_APC_5
				GFX_GER_APC_6
			}
			models = {
				"GER_APC_Fuchs_vehicle_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_GER_APC_6
				GFX_GER_APC_7
			}
			models = {
				"GER_APC_Fuchs_vehicle_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_GER_APC_8
			}
			models = {
				"GER_APC_Fuchs_vehicle_entity"
			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_GER_IFV_1
				GFX_GER_IFV_2
			}
			models = {
				"GER_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_GER_IFV_3
				GFX_GER_IFV_4
			}
			models = {
				"GER_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_GER_IFV_5
			}
			models = {
				"GER_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_GER_IFV_6
				GFX_GER_IFV_7
			}
			models = {
				"GER_MD4_light_armor_entity_2"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_GER_IFV_8
				GFX_GER_IFV_7
				GFX_GER_IFV_6
			}
			models = {
				"GER_MD4_light_armor_entity_2"
			}
		}
	}
}

GRE = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_GRE_APC_1
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_GRE_APC_2
				GFX_GRE_APC_3
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_GRE_APC_4
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_GRE_IFV_1
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_GRE_IFV_3
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_GRE_IFV_4
				GFX_GRE_IFV_5
			}
			models = {

			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_GRE_IFV_5
			}
			models = {

			}
		}
	}
}

ISR = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_ISR_APC_1
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_ISR_APC_2
				GFX_ISR_APC_3
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_ISR_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_ISR_APC_5
				GFX_ISR_APC_6
			}
			models = {

			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_ISR_APC_7
				GFX_ISR_APC_6
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_ISR_IFV_1
				GFX_ISR_IFV_2
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_ISR_IFV_3
				GFX_ISR_IFV_4
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_ISR_IFV_5
			}
			models = {

			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_ISR_IFV_6
			}
			models = {

			}
		}
	}
}

ITA = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_ITA_APC_1
				GFX_ITA_APC_2
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_ITA_APC_3
				GFX_ITA_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_ITA_APC_5
				GFX_ITA_APC_6
			}
			models = {

			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_ITA_APC_6
				GFX_ITA_APC_7
			}
			models = {

			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_ITA_APC_7
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_ITA_IFV_2
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_ITA_IFV_3
				GFX_ITA_IFV_4
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_ITA_IFV_4
				GFX_ITA_IFV_5
			}
			models = {

			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_ITA_IFV_10
			}
			models = {

			}
		}
	}
}

JAP = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_JAP_APC_1
				GFX_JAP_APC_2
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_JAP_APC_3
				GFX_JAP_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_JAP_APC_5
				GFX_JAP_APC_6
			}
			models = {

			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_JAP_APC_6
				GFX_JAP_APC_7
			}
			models = {

			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_JAP_APC_8
				GFX_JAP_APC_7
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_JAP_IFV_1
				GFX_JAP_IFV_2
			}
			models = {
				"JAP_MD4_light_armor_entity"
				"KOR_MD4_light_armor_entity"
				"SOV_BMP3_entity"
				"SOV_T15_entity"
				"UKR_MD4_light_armor_entity"
				"USA_MD4_light_armor_entity"
				"USA_MD4_light_armor_entity"
				"USA_MD4_light_armor_entity"
				"USA_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_JAP_IFV_3
				GFX_JAP_IFV_4
			}
			models = {
				"JAP_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_JAP_IFV_5
				GFX_JAP_IFV_4
			}
			models = {
				"JAP_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_JAP_IFV_6
				GFX_JAP_IFV_7
				GFX_JAP_IFV_5
			}
			models = {
				"JAP_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_JAP_IFV_7
				GFX_JAP_IFV_6
			}
			models = {
				"JAP_MD4_light_armor_entity"
			}
		}
	}
}

KOR = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_KOR_APC_1
				GFX_KOR_APC_2
			}
			models = {
				"KOR_APC_K200_mesh_entity"
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_KOR_APC_3
				GFX_KOR_APC_4
			}
			models = {
				"KOR_APC_K200_mesh_entity"
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_KOR_APC_4
				GFX_KOR_APC_5
			}
			models = {
				"KOR_APC_K200_mesh_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_KOR_APC_6
				GFX_KOR_APC_7
			}
			models = {
				"KOR_APC_K200_mesh_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_KOR_APC_8
				GFX_KOR_APC_7
			}
			models = {
				"KOR_APC_K200_mesh_entity"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_KOR_IFV_4
				GFX_KOR_IFV_5
			}
			models = {
				"KOR_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_KOR_IFV_5
				GFX_KOR_IFV_6
				GFX_KOR_IFV_7
			}
			models = {
				"KOR_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_KOR_IFV_8
				GFX_KOR_IFV_7
			}
			models = {
				"KOR_MD4_light_armor_entity"
			}
		}
	}
}

NKO = {
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_NKO_APC_1
			}
			models = {

			}
		}
	}
}

PER = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_PER_APC_1
				GFX_PER_APC_2
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_PER_APC_3
				GFX_PER_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_PER_APC_4
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_PER_IFV_1
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_PER_IFV_2
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_PER_IFV_2
			}
			models = {
			
			}
		}
	}
}

POL = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_POL_APC_1
				GFX_POL_APC_2
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_POL_APC_3
				GFX_POL_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_POL_APC_5
				GFX_POL_APC_6
			}
			models = {

			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_POL_APC_6
				GFX_POL_APC_7
			}
			models = {

			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_POL_APC_7
				GFX_POL_APC_8
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_POL_IFV_1
				GFX_POL_IFV_2
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_POL_IFV_3
				GFX_POL_IFV_4
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_POL_IFV_4
				GFX_POL_IFV_5
			}
			models = {

			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_POL_IFV_5
				GFX_POL_IFV_6
			}
			models = {

			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_POL_IFV_7
				GFX_POL_IFV_8
			}
			models = {

			}
		}
	}
}

RAJ = {
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_RAJ_IFV_2
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_RAJ_IFV_2
			}
			models = {

			}
		}
	}
}

SAF = {
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_SAF_APC_2
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_SAF_APC_3
			}
			models = {

			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_SAF_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_SAF_APC_6
			}
			models = {

			}
		}
	}
}

SOV = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_SOV_APC_1
				GFX_SOV_APC_2
			}
			models = {
				"SOV_MD4_light_armor_entity"
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_SOV_APC_3
				GFX_SOV_APC_4
			}
			models = {
				"SOV_MD4_light_armor_entity"
				"btr80_vehicle_entity"
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_SOV_APC_5
			}
			models = {
				"SOV_MD4_light_armor_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_SOV_APC_6
				GFX_SOV_APC_7
				GFX_SOV_APC_5
			}
			models = {
				"SOV_MD4_light_armor_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_SOV_APC_8
				GFX_SOV_APC_9
			}
			models = {
				"SOV_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_SOV_IFV_1
			}
			models = {
				"SOV_MD4_light_armor_entity"
				"SOV_BMP2_entity"
				"SOV_BMP3_entity"
				"SOV_T15_entity"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_SOV_IFV_2
				GFX_SOV_IFV_3
			}
			models = {
				"SOV_MD4_light_armor_entity"
				"SOV_BMP2_entity"
				"SOV_BMP3_entity"
				"SOV_T15_entity"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_SOV_IFV_4
				GFX_SOV_IFV_5
			}
			models = {
				"SOV_MD4_light_armor_entity"
				"SOV_BMP2_entity"
				"SOV_BMP3_entity"
				"SOV_T15_entity"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_SOV_IFV_6
				GFX_SOV_IFV_7
			}
			models = {
				"SOV_MD4_light_armor_entity"
				"SOV_BMP2_entity"
				"SOV_BMP3_entity"
				"SOV_T15_entity"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_SOV_IFV_8
				GFX_SOV_IFV_7
			}
			models = {
				"SOV_MD4_light_armor_entity"
				"SOV_BMP2_entity"
				"SOV_BMP3_entity"
				"SOV_T15_entity"
			}
		}
	}
}

SWE = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_SWE_APC_1
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_SWE_APC_2
				GFX_SWE_APC_3
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_SWE_APC_3
				GFX_SWE_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_SWE_APC_5
				GFX_SWE_APC_6
			}
			models = {

			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_SWE_APC_6
				GFX_SWE_APC_7
				GFX_SWE_APC_8
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_SWE_IFV_1
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_SWE_IFV_2
				GFX_SWE_IFV_3
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_SWE_IFV_4
				GFX_SWE_IFV_5
			}
			models = {

			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_SWE_IFV_6
				GFX_SWE_IFV_7
			}
			models = {

			}
		}
	}
		ifv_hull_4 = {
		pool = {
			icons = {
				GFX_SWE_IFV_7
				GFX_SWE_IFV_8
			}
			models = {

			}
		}
	}
}

SYR = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_SYR_APC_3
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_SYR_APC_4
			}
			models = {

			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_SYR_IFV_1
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_SYR_IFV_2
				GFX_SYR_IFV_3
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_SYR_IFV_4
				GFX_SYR_IFV_5
			}
			models = {

			}
		}
	}
}

TUR = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_TUR_APC_1
				GFX_TUR_APC_2
			}
			models = {

			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_TUR_APC_3
				GFX_TUR_APC_4
			}
			models = {

			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_TUR_APC_5
				GFX_TUR_APC_6
			}
			models = {

			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_TUR_APC_6
				GFX_TUR_APC_7
			}
			models = {

			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_TUR_APC_8
				GFX_TUR_APC_7
			}
			models = {

			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_TUR_IFV_3
				GFX_TUR_IFV_4
			}
			models = {

			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_TUR_IFV_5
			}
			models = {

			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_TUR_IFV_6
			}
			models = {

			}
		}
	}
		ifv_hull_4 = {
		pool = {
			icons = {
				GFX_TUR_IFV_7
			}
			models = {

			}
		}
	}
}

UKR = {
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_UKR_IFV_1
			}
			models = {
				"UKR_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_UKR_IFV_2
			}
			models = {
				"UKR_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_UKR_IFV_4
			}
			models = {
				"UKR_MD4_light_armor_entity"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_UKR_IFV_5
			}
			models = {
				"UKR_MD4_light_armor_entity"
			}
		}
	}
}

USA = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_USA_APC_1
				GFX_USA_APC_2
			}
			models = {
				"AAVP7_entity"
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_USA_APC_3
				GFX_USA_APC_4
			}
			models = {
				"AAVP7_entity"
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_USA_APC_5
			}
			models = {
				"AAVP7_entity"
				"Strykerapc_vehicle_entity"
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_USA_APC_6
				GFX_USA_APC_7
				GFX_USA_APC_5
			}
			models = {
				"AAVP7_entity"
				"Strykerapc_vehicle_entity"
			}
		}
	}
	apc_hull_4 = {
		pool = {
			icons = {
				GFX_USA_APC_8
				GFX_USA_APC_6
			}
			models = {
				"AAVP7_entity"
			}
		}
	}
	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_USA_IFV_1
				GFX_USA_IFV_2
			}
			models = {
				"USA_MD4_light_armor_entity"
				"LAV25_vehicle_entity"
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_USA_IFV_3
				GFX_USA_IFV_4
			}
			models = {
				"USA_MD4_light_armor_entity"
				"AAVP7_entity"
				"LAV25_vehicle_entity"
				"USA_M2A2bradley_entity"
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
			    GFX_USA_IFV_10_medium
				GFX_USA_IFV_9_medium
				GFX_USA_IFV_5
				GFX_USA_IFV_4
			}
			models = {
				"USA_MD4_light_armor_entity"
				"AAVP7_entity"
				"LAV25_vehicle_entity"
				"USA_M2A2bradley_entity"
				"USA_M2A3bradley_entity"
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_USA_IFV_6
				GFX_USA_IFV_5
				GFX_USA_IFV_10_medium
				GFX_USA_IFV_9_medium
			}
			models = {
				"USA_MD4_light_armor_entity"
				"AAVP7_entity"
				"USA_M2A3bradley_entity"
				"USA_M7A1dunham_entity"
			}
		}
	}
	ifv_hull_4 = {
		pool = {
			icons = {
				GFX_USA_IFV_6
			}
			models = {
				"USA_MD4_light_armor_entity"
				"AAVP7_entity"
				"USA_M7A1dunham_entity"
			}
		}
	}
}
CAN = {
	apc_hull_0 = {
		pool = {
			icons = {
				GFX_CAN_APC_1
				GFX_USA_APC_1
			}
			models = {
			}
		}
	}
	apc_hull_1 = {
		pool = {
			icons = {
				GFX_CAN_APC_3
			}
			models = {
			}
		}
	}
	apc_hull_2 = {
		pool = {
			icons = {
				GFX_CAN_APC_4
			}
			models = {
			}
		}
	}
	apc_hull_3 = {
		pool = {
			icons = {
				GFX_CAN_APC_5
			}
			models = {
			}
		}
	}

	ifv_hull_0 = {
		pool = {
			icons = {
				GFX_CAN_IFV_1
			}
			models = {
			}
		}
	}
	ifv_hull_1 = {
		pool = {
			icons = {
				GFX_CAN_IFV_2
				GFX_CAN_IFV_4
			}
			models = {
			}
		}
	}
	ifv_hull_2 = {
		pool = {
			icons = {
				GFX_CAN_IFV_5
				GFX_CAN_IFV_6
			}
			models = {
			}
		}
	}
	ifv_hull_3 = {
		pool = {
			icons = {
				GFX_CAN_IFV_7
			}
			models = {
			}
		}
	}

}