﻿instant_effect = {
	### Aircraft ###
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 24		#to fill a squadron
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #Chengdu J-7
		amount = 176
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #F-16A Fighting Falcon
		amount = 32
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #Mirage 5
		amount = 52
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #Q-5
		amount = 42
		producer = CHI
	}
}