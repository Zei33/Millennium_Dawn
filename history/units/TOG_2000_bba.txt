﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		variant_name = "Dassault Alpha Jet"
		amount = 5
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Embraer EMB-326" 		# Brazil-specific variant of Aermacchi MB-326
		amount = 4
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "DHC-4 Caribou"			#in place of DHC-5 & Do-27
		amount = 3
		producer = CAN
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Fokker F27 Friendship"		#in place of F-28
		amount = 1
		producer = HOL
	}
}