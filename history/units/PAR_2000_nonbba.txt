﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #EMB-326
		amount = 6
		producer = BRA
	}
}