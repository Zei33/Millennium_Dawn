﻿instant_effect = {

	###Fighters####

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 22
		variant_name = "F-16C Blk 40/42"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		amount = 12
		variant_name = "F-5E Tiger II"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 3
		variant_name = "C-20 Gulfstream"
		producer = USA
	}
}