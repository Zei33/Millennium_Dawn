instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 4
		producer = SOV
		variant_name = "Su-17"
	}
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_2
		amount = 5
		producer = SOV
		variant_name = "Su-24M"
	}
	add_equipment_to_stockpile = {
		type = medium_plane_cas_airframe_2
		amount = 2
		producer = SOV
		variant_name = "Su-25"
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 5
		producer = SOV
		variant_name = "MiG-21s Bis"
	}
	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_1
		amount = 34
		producer = SOV
		variant_name = "MiG-25 Foxbat"
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 4
		producer = SOV
		variant_name = "An-26"
	}

}