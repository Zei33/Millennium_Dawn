﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = medium_plane_cas_airframe_1
		variant_name = "IAR-93 Vultur"
		amount = 73
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-21s Bis"
		amount = 180
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 20
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130J Super Hercules"
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		variant_name = "MiG-29 Fulcrum"
		amount = 18
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-23"
		amount = 40
		producer = SOV
	}

	#Helicopters

	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1			 #IAR 330
		amount = 41
		producer = ROM
	}
}