﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 # ALPHA JET
		amount = 5
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1  #EMB-326
		amount = 4
		producer = BRA
	}
}