﻿
instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Pilatus PC-7"
		amount = 7
		producer = SWI
	}

	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		amount = 13
		variant_name = "F-5 Freedom Fighter"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 3
		variant_name = "C-130 Hercules"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 2
		variant_name = "CASA/IPTN CN-235"
		producer = SPR
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 2
		variant_name = "Short SC.7 Skyvan"
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-212 Aviocar"
		amount = 2
		producer = SPR
	}

	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "BN-2 Defender"
		amount = 12
		producer = ENG
	}
}