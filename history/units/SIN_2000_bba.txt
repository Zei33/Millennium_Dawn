﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "A-4SU Super Skyhawk"
		amount = 50
		producer = SIN
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		variant_name = "F-5 Freedom Fighter"
		amount = 45
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 91
		variant_name = "F-16C Blk 40/42"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_scout_airframe_2
		amount = 4
		variant_name = "E-2 Hawkeye"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 6
		producer = USA
	}
}