units = {
	fleet = {
		name = "Western Fleet"
		naval_base = 4279
		task_force = {
			name = "Western Fleet 2"
			location = 4279
			ship = { name = "Spajik Class" definition = frigate start_experience_factor = 0.30 equipment = { missile_corvette_3 = { amount = 1 owner = IND } } }
		}
	}
}