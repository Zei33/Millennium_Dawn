﻿instant_effect = {

	####################
	## Transports ######
	####################

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 45
		variant_name = "Shaanxi Y-7"
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 68
		variant_name = "Shaanxi Y-8"
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_2 #Ilyushin Il-76M
		amount = 14
		variant_name = "Il-76M"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 45
		variant_name = "An-26"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 15
		variant_name = "Tu-154M"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#includes Y-11 #'s as no variant is available
		amount = 23
		variant_name = "Harbin Y-12"
		producer = CHI
	}

	################################
	### Land Based Fighters ########
	################################

	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		amount = 24
		variant_name = "J-11"
		producer = CHI
	}


	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		amount = 65
		variant_name = "Su-27"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		amount = 40
		variant_name = "Su-30"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 292
		variant_name = "J-8"
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 564
		variant_name = "J-6"
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 766
		variant_name = "J-7"
		producer = CHI
	}


	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 20
		variant_name = "JH-7"
		producer = CHI
	}


	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 300
		variant_name = "Nanchang Q-5"
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 30
		variant_name = "Nanchang Q-5I"
		producer = CHI
	}

	################################
	### Strat Bombers ##############
	################################


	add_equipment_to_stockpile = {
		type = large_plane_airframe_1
		amount = 97
		variant_name = "Harbin H-5"
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = large_plane_airframe_1
		amount = 31
		variant_name = "Xian H-6"
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = large_plane_airframe_1
		amount = 120
		variant_name = "Xian H-6E-F"
		producer = CHI
	}

	#######################
	## AWACS  #############
	#######################

	add_equipment_to_stockpile = {
		type = large_plane_awacs_airframe_1
		amount = 6
		variant_name = "TU-154M ELINT"
		producer = CHI
	}

	####################
	## Patrol ##########
	####################

	add_equipment_to_stockpile = {
		type = large_plane_maritime_patrol_airframe_1
		amount = 4
		variant_name = "Shaanxi Y-8X"
		producer = CHI
	}
}