﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1		#MiG-21
		amount = 24
		variant_name = "MiG-21s Bis"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#An-26
		amount = 8
		variant_name = "An-26"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1		#Mi-17
		amount = 23
		producer = SOV
	}
}
