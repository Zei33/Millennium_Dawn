﻿instant_effect = {

	###Fighters####

	add_equipment_to_stockpile = {
		type = MR_Fighter1			#F-5 E & F
		amount = 12
		producer = USA
	}


	add_equipment_to_stockpile = {
		type = MR_Fighter3			#F-16C/D
		amount = 24
		producer = USA
	}
}