﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #Mirage 2000
		amount = 31
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C130-H
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 #C-212 Aviocar
		amount = 4
		producer = SPR
	}
}