on_actions = {
	on_monthly_CHI = {
		effect = {
			random_list = {
				20 = {
					subtract_from_variable = { CHI.kasmir_china_readiness = 5 }
				}
				80 = {
					# nothing
				}
			}
			# tension is handled in RAJ
			random_list = {
				20 = {
					subtract_from_variable = { GLOBAL.arunachal_tension = 5 }
				}
				80 = {
					# nothing
				}
			}
			random_list = {
				20 = {
					subtract_from_variable = { CHI.arunachal_readiness = 5 }	
				}
				80 = {
					# nothing
				}
			}
			
			# moved out for smooth value calculation
			clamp_variable = {
						var = CHI.kasmir_china_readiness
						min = 0
						max = 100
			}
			clamp_variable = {
						var = GLOBAL.arunachal_tension
						min = 0
						max = 100
			}
			clamp_variable = {
						var = arunachal_readiness
						min = 0
						max = 100
			}
			
			#uighur event chain
			if = {
				limit = {
					OR = {
						check_variable = { global.month = 4 }
						check_variable = { global.month = 8 }
						check_variable = { global.month = 12 }
					}
				}
				random_list = {
					100 = {
						modifier = {
							factor = 0
							NOT = {
								CHI = {
									has_country_flag = borderland_calm_down
								}
							}
						}
						add_political_power = 1
					}
					20 = {
						modifier = {
							factor = 0
							NOT = {
								CHI = {
									controls_state = 592
									check_variable = {
										var = uighur_opinion
										value = 100
										compare = less_than
									}
									check_variable = {
										var = uighur_threat
										value = 50
										compare = greater_than
									}
								}
							}
						}
						country_event = china.9
					}
					20 = {
						modifier = {
							factor = 0
							NOT = {
								CHI = {
									controls_state = 592
									check_variable = {
										var = uighur_opinion
										value = 100
										compare = less_than
									}
									check_variable = {
										var = uighur_threat
										value = 70
										compare = greater_than
									}
								}
							}
						}
						country_event = china.10
					}
					10 = {
						modifier = {
							factor = 0
							NOT = {
								controls_state = 592
								check_variable = {
									var = uighur_opinion
									value = 100
									compare = less_than
								}
							}
						}
						country_event = china.11
					}
					10 = {
						modifier = {
							factor = 0
							NOT = {
								CHI = {
									controls_state = 592
									check_variable = {
										var = uighur_opinion
										value = 100
										compare = less_than
									}
								}
							}
						}
						country_event = china.8
					}
					5 = {
						modifier = {
							factor = 0
							NOT = {
								CHI = {
									controls_state = 592
									check_variable = {
										var = uighur_opinion
										value = 100
										compare = less_than
									}
									check_variable = {
										var = uighur_threat
										value = 90
										compare = greater_than
									}
								}
							}
						}
						modifier = {
							factor = 1.50
							check_variable = {
								var = uighur_threat
								value = 95
								compare = greater_than
							}
						}
						modifier = {
							factor = 1.75
							CHI = { has_war = yes }
						}
						modifier = {
							factor = 1.25
							CHI = { has_stability < 0.4 }
						}
						country_event = china.13
					}
				}

			}
		}
	}
}