####Nationalists scripted effects
Nationalist_policy = {
	if = {
		limit = {
			has_completed_focus = CES_kipchak_chak
		}
		add_state_claim = 152
		add_state_claim = 148
		add_state_claim = 147
		add_state_claim = 997
		add_state_claim = 333
	}
}

Nationalist_policy1 = {
	if = {
		limit = {
			has_completed_focus = CES_free_uighurs
		}
		add_state_claim = 592
	}
}

Nationalist_policy2 = {
	if = {
		limit = {
			has_completed_focus = CES_march_to_siberia
		}
		add_state_claim = 666
		add_state_claim = 1107
		add_state_claim = 671
		add_state_claim = 670
		add_state_claim = 673
		add_state_claim = 674
		add_state_claim = 668
		add_state_claim = 667
		add_state_claim = 645
		add_state_claim = 649
		add_state_claim = 654
		add_state_claim = 653
		add_state_claim = 656
		add_state_claim = 665
		add_state_claim = 663
		add_state_claim = 664
		add_state_claim = 675
		add_state_claim = 1071
		add_state_claim = 652
		add_state_claim = 672
		add_state_claim = 657
		add_state_claim = 655
		add_state_claim = 662
		add_state_claim = 651
		add_state_claim = 660
		add_state_claim = 661
		add_state_claim = 650
		add_state_claim = 1125
		add_state_claim = 1126
		add_state_claim = 1127
		add_state_claim = 1140
	}
}

Nationalist_policy3 = {
	if = {
		limit = {
			has_completed_focus = CES_invade_jap
		}
		add_state_claim = 609
		add_state_claim = 611
		add_state_claim = 610
		add_state_claim = 612
		add_state_claim = 613
		add_state_claim = 615
		add_state_claim = 616
		add_state_claim = 617
		add_state_claim = 618
		add_state_claim = 939
		add_state_claim = 620
		add_state_claim = 621
		add_state_claim = 619
	}
}

Nationalist_policy4 = {
	if = {
		limit = {
			has_completed_focus = CES_those_whorej
			original_tag = KAZ
		}
		add_state_claim = 727
		add_state_claim = 729
		add_state_claim = 728
		add_state_claim = 726
		add_state_claim = 725
		add_state_claim = 723
		add_state_claim = 724
		add_state_claim = 722
		add_state_claim = 721
		add_state_claim = 720
	}

	if = {
		limit = {
			has_completed_focus = CES_those_whorej
			original_tag = KYR
		}
		add_state_claim = 727
		add_state_claim = 729
		add_state_claim = 728
		add_state_claim = 726
		add_state_claim = 725
		add_state_claim = 723
		add_state_claim = 724
		add_state_claim = 722
		add_state_claim = 717
		add_state_claim = 719
		add_state_claim = 716
		add_state_claim = 715
		add_state_claim = 714
		add_state_claim = 718
	}

	if = {
		limit = {
			has_completed_focus = CES_those_whorej
			original_tag = UZB
		}
		add_state_claim = 729
		add_state_claim = 728
		add_state_claim = 723
		add_state_claim = 724
		add_state_claim = 720
		add_state_claim = 721
		add_state_claim = 722
		add_state_claim = 717
		add_state_claim = 719
		add_state_claim = 716
		add_state_claim = 715
		add_state_claim = 714
		add_state_claim = 718
	}
}

Nationalist_policy7 = {
	if = {
		limit = {
			has_completed_focus = CES_back_to_genghis_khan
		}
		add_state_claim = 597
		add_state_claim = 596
	}
}

Nationalist_policy8 = {
	if = {
		limit = {
			has_completed_focus = CES_idelural
		}
		add_state_claim = 664
	}
}

Nationalist_policy9 = {
	if = {
		limit = {
			has_completed_focus = CES_finish_the_deed
		}
		add_state_claim = 1035
	}
}