set_leader_ERI = {
	if = {
		limit = { has_country_flag = ETH_transitional_government_FLAG }
		if = {
			limit = { has_country_flag = set_liberalism }

			create_country_leader = {
				name = "Eritrean Transitional Government"
				desc = ERI_transitional_government_desc
				picture = "Portrait_Eritrea_Government.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					transitional_government
				}
			}
		}
		else_if = {
			limit = { has_country_flag = set_Communist-State }

			create_country_leader = {
				name = "Eritrean Transitional Government"
				desc = ERI_transitional_government_desc
				picture = "Portrait_Eritrea_Government.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					transitional_government
				}
			}
		}
		else_if = {
			limit = { has_country_flag = set_anarchist_communism }

			create_country_leader = {
				name = "Eritrean Transitional Government"
				desc = ERI_transitional_government_desc
				picture = "Portrait_Eritrea_Government.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
					transitional_government
				}
			}
		}
		else_if = {
			limit = { has_country_flag = set_Neutral_conservatism }

			create_country_leader = {
				name = "Eritrean Transitional Government"
				desc = ERI_transitional_government_desc
				picture = "Portrait_Eritrea_Government.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					transitional_government
				}
			}
		}
		else_if = {
			limit = { has_country_flag = set_Monarchist }

			create_country_leader = {
				name = "Eritrean Transitional Government"
				desc = ERI_transitional_government_desc
				picture = "Portrait_Eritrea_Government.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
					transitional_government
				}
			}
		}
	}
	else = {
		if = { limit = { has_country_flag = set_liberalism }

			if = { limit = { check_variable = { liberalism_leader = 0 } }
				add_to_variable = { liberalism_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Ahmed Mohammed Nasser"
					picture = "Portrait_Ahmed_Nasser.dds"
					ideology = liberalism
					traits = {
						western_liberalism
						guerrilla_leader
						military_career
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
				if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
			}
		}
		else_if = { limit = { has_country_flag = set_anarchist_communism }

			if = { limit = { check_variable = { anarchist_communism_leader = 0 } ETH = { NOT = { has_country_flag = ETH_locked_federalisation_FLAG } } }
				add_to_variable = { anarchist_communism_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Yemane Gebreab"
					picture = "Portrait_Yemane_Gebreab.dds"
					ideology = anarchist_communism
					traits = {
						emerging_anarchist_communism
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
				if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
			}
		}
		else_if = { limit = { has_country_flag = set_Caliphate }

			if = { limit = { check_variable = { Caliphate_leader = 0 } }
				add_to_variable = { Caliphate_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Shaikh Khalil Mohammed Amer"
					picture = "khalil_mohammed_amer.dds"
					ideology = Caliphate
					traits = {
						salafist_Caliphate
						pro_taliban
						taliban_militant
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Caliphate_leader = 1 } }
				if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
			}
			else_if = { limit = { check_variable = { Caliphate_leader = 1 } }
				add_to_variable = { Caliphate_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Emir Abul Bara' Hassan Salman"
					picture = "abdul_salman.dds"
					ideology = Caliphate
					traits = {
						salafist_Caliphate
						pro_taliban
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Caliphate_leader = 1 } }
				if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
			}
		}
		else_if = { limit = { has_country_flag = set_neutral_Social }

			if = { limit = { check_variable = { neutral_Social_leader = 0 } }
				add_to_variable = { neutral_Social_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Tewelde Ghebreselassie"
					picture = "tewelde_ghebreselassie.dds"
					ideology = neutral_Social
					traits = {
						neutrality_neutral_Social
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
				if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
			}
		}
		else_if = { limit = { has_country_flag = set_Nat_Autocracy }
			if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } ETH = { NOT = { has_country_flag = ETH_locked_federalisation_FLAG } } }
				add_to_variable = { Nat_Autocracy_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Isaias Afwerki"
					picture = "ERI_Isaias_Afwerki.dds"
					ideology = Nat_Autocracy
					traits = {
						nationalist_Nat_Autocracy
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
				if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
			}
			# EPLF COUP
				else_if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } ETH = { has_country_flag = ETH_locked_federalisation_FLAG } has_country_flag = ERI_EPLF_coup_FLAG }
					add_to_variable = { Nat_Autocracy_leader = 1 }
					hidden_effect = { kill_country_leader = yes }

					create_country_leader = {
						name = "Sebhat Ephrem"
						picture = "sebhat_ephrem.dds"
						ideology = Nat_Autocracy
						traits = {
							nationalist_Nat_Autocracy
							military_career
						}
					}

					if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
					if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
				}
				else_if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } ETH = { has_country_flag = ETH_locked_federalisation_FLAG } has_country_flag = ERI_EPLF_coup_FLAG }
					add_to_variable = { Nat_Autocracy_leader = 1 }
					hidden_effect = { kill_country_leader = yes }

					create_country_leader = {
						name = "Filipos Woldeyohannes"
						picture = "Filipos_Woldeyohannes.dds"
						ideology = Nat_Autocracy
						traits = {
							nationalist_Nat_Autocracy
							military_career
						}
					}

					if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
					if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
				}
			# REGULAR MILITARY LINE OF SUCCESSION
				else_if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } ETH = { NOT = { has_country_flag = ETH_locked_federalisation_FLAG } } }
					add_to_variable = { Nat_Autocracy_leader = 1 }
					hidden_effect = { kill_country_leader = yes }

					create_country_leader = {
						name = "Sebhat Ephrem"
						picture = "sebhat_ephrem.dds"
						ideology = Nat_Autocracy
						traits = {
							nationalist_Nat_Autocracy
							military_career
						}
					}

					if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
					if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
				}
				else_if = { limit = { check_variable = { Nat_Autocracy_leader = 2 } ETH = { NOT = { has_country_flag = ETH_locked_federalisation_FLAG } } }
					add_to_variable = { Nat_Autocracy_leader = 1 }
					hidden_effect = { kill_country_leader = yes }

					create_country_leader = {
						name = "Filipos Woldeyohannes"
						picture = "Filipos_Woldeyohannes.dds"
						ideology = Nat_Autocracy
						traits = {
							nationalist_Nat_Autocracy
							military_career
						}
					}

					if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
					if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
				}
		}
	}
}