
MNT_balance_mon_change = {
	if = {
		limit = { has_idea = MNT_slight_mnt_dominance }
		swap_ideas = {
			remove_idea = MNT_slight_mnt_dominance
			add_idea = MNT_mnt_dominance
		}
	}
	else_if = {
		limit = { has_idea = MNT_dominance_balance }
		swap_ideas = {
			remove_idea = MNT_dominance_balance
			add_idea = MNT_slight_mnt_dominance
		}
	}
	else_if = {
		limit = { has_idea = MNT_slight_ser_dominance }
		swap_ideas = {
			remove_idea = MNT_slight_ser_dominance
			add_idea = MNT_dominance_balance
		}
	}
	else_if = {
		limit = { has_idea = MNT_ser_dominance }
		swap_ideas = {
			remove_idea = MNT_ser_dominance
			add_idea = MNT_slight_ser_dominance
		}
	}
	else_if = {
		limit = { has_idea = MNT_mnt_dominance }
		add_political_power = 100
	}
}
MNT_balance_ser_change = {
	if = {
		limit = { has_idea = MNT_slight_ser_dominance }
		swap_ideas = {
			remove_idea = MNT_slight_ser_dominance
			add_idea = MNT_ser_dominance
		}
	}
	else_if = {
		limit = { has_idea = MNT_dominance_balance }
		swap_ideas = {
			remove_idea = MNT_dominance_balance
			add_idea = MNT_slight_ser_dominance
		}
	}
	else_if = {
		limit = { has_idea = MNT_slight_mnt_dominance }
		swap_ideas = {
			remove_idea = MNT_slight_mnt_dominance
			add_idea = MNT_dominance_balance
		}
	}
	else_if = {
		limit = { has_idea = MNT_mnt_dominance }
		swap_ideas = {
			remove_idea = MNT_mnt_dominance
			add_idea = MNT_slight_mnt_dominance
		}
	}
	else_if = {
		limit = { has_idea = MNT_ser_dominance }
		add_political_power = 100
	}
}
MNT_economy_change = {
	if = {
		limit = { has_idea = MNT_legacy_of_90 }
		swap_ideas = {
			remove_idea = MNT_legacy_of_90
			add_idea = MNT_legacy_of_9
		}
	}
	else_if = {
		limit = { has_idea = MNT_legacy_of_9 }
		swap_ideas = {
			remove_idea = MNT_legacy_of_9
			add_idea = MNT_legacy_of
		}
	}
	else_if = {
		limit = { has_idea = MNT_legacy_of }
		swap_ideas = {
			remove_idea = MNT_legacy_of
			add_idea = MNT_legacy
		}
	}
	else_if = {
		limit = { has_idea = MNT_legacy }
		remove_ideas = MNT_legacy
	}
}

MNT_military_change = {
	if = {
		limit = { has_idea = MNT_military_dead }
		swap_ideas = {
			remove_idea = MNT_military_dead
			add_idea = MNT_military_creating
		}
	}
	else_if = {
		limit = { has_idea = MNT_military_creating }
		swap_ideas = {
			remove_idea = MNT_military_creating
			add_idea = MNT_military_established
		}
	}
	else_if = {
		limit = { has_idea = MNT_military_established }
			remove_ideas = MNT_military_established
			
	}
}