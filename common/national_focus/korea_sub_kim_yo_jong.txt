	shared_focus = {
		id = NKO_kim_yo_jong
		icon = kim_yo_jong

		x = -2
		y = 12
		relative_position_id = NKO_Kim_Jong_il_Death

		cost = 10
		allow_branch = { always = no }

		available = {
			NOT = {
				has_country_leader = {
					name = "Kim Jong-il"
					ruling_only = yes
				}
			}
			has_country_flag = Kim_Jong_il_Death_tt
			#has_global_flag = game_rule_NKO_Yo_jong
			NOT = { has_completed_focus = NKO_Junta }
			NOT = { has_completed_focus = NKO_Kim_Jong_Nam }
			NOT = { has_completed_focus = NKO_Kim_Jong_un }
		}

		completion_reward = {
			create_country_leader = {
				name = "Kim Yo-jong"
				picture = "Kim Yo-jong.dds"
				desc = KIM_YO_JONG_LEADER_DESC
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					career_politician
					Reformer
					likeable
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_kim_yo_jong executed"
		}

		ai_will_do = {
			base = 1
		}
	}
	shared_focus = {
		id = NKO_juche_for_new_era
		icon = juche

		x = -6
		y = 1
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_kim_yo_jong }
		cost = 10

		completion_reward = {
			add_political_power = 50
			log = "[GetDateText]: [This.GetName]: focus NKO_juche_for_new_era executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_for_well_being_of_people
		icon = money

		x = -7
		y = 2
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_juche_for_new_era }
		cost = 10

		completion_reward = {
			add_stability = 0.02
			log = "[GetDateText]: [This.GetName]: focus NKO_for_well_being_of_people executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_for_peace_of_people
		icon = peace

		x = -5
		y = 2
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_juche_for_new_era }
		cost = 10

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA FOCUS_FILTER_INTERNAL_FACTION }

		completion_reward = {
			set_temp_variable = { temp_opinion = -5 }
			change_the_military_opinion = yes
			KOR = {
				support_for_reunification_1 = yes
				add_war_support = -0.01
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_for_peace_of_people executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_quality_first
		icon = consumer_goods

		x = -8
		y = 3
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_for_well_being_of_people }
		cost = 10

		completion_reward = {
			add_political_power = -150
			decrease_corruption = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_quality_first executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_chosun_dream
		icon = Choson_Dream

		x = -8
		y = 4
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_quality_first }
		cost = 10

		completion_reward = {
			add_ideas = NKO_Choson_Dream
			log = "[GetDateText]: [This.GetName]: focus NKO_chosun_dream executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_privatize_civil_industry
		icon = liberalization_policies

		x = -9
		y = 6
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_chosun_dream }
		mutually_exclusive = { focus = NKO_expand_autonomy_of_state_owned_factory }
		cost = 10

		search_filters = { FOCUS_FILTER_INTERNAL_FACTION FOCUS_FILTER_EXPENDITURE }

		completion_reward = {
			set_temp_variable = { treasury_change = 5 }
			modify_treasury_effect = yes
			set_temp_variable = { temp_opinion = 5 }
			change_the_donju_opinion = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_privatize_civil_industry executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_expand_autonomy_of_state_owned_factory
		icon = central_planned_economy2

		x = -7
		y = 6
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_chosun_dream }
		mutually_exclusive = { focus = NKO_privatize_civil_industry }
		cost = 10

		completion_reward = {
			set_temp_variable = { temp_opinion = 5 }
			change_communist_cadres_opinion = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_expand_autonomy_of_state_owned_factory executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_affluent_society
		icon = generic_banks

		x = -8
		y = 8
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_privatize_civil_industry focus = NKO_expand_autonomy_of_state_owned_factory }
		cost = 10

		completion_reward = {
			add_stability = 0.02
			add_popularity = {
				ideology = communism
				popularity = 0.05
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_affluent_society executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_inter_korean_summit
		icon = two_korea

		x = -3
		y = 3
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_for_peace_of_people }
		cost = 10


		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }

		completion_reward = {
			KOR = {
				add_opinion_modifier = {
					target = NKO
					modifier = political_outreach
				}
				support_for_reunification_1 = yes
				add_war_support = -0.01
			}
			add_war_support = -0.02
			log = "[GetDateText]: [This.GetName]: focus NKO_inter_korean_summit executed"
		}

		ai_will_do = {
			base = 1
		}
	}


	shared_focus = {
		id = NKO_close_DMZ
		icon = pro_immigration1

		x = -4
		y = 4
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_inter_korean_summit }
		cost = 10

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }

		completion_reward = {
			KOR = {
				add_opinion_modifier = {
					target = NKO
					modifier = political_outreach
				}
				support_for_reunification_1 = yes
				add_war_support = -0.02
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_close_DMZ executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_common_trade_belt_of_korea
		icon = trade_with_south_korea

		x = -5
		y = 5
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_close_DMZ }
		cost = 10

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }

		available = {
			NOT = { has_war_with = KOR }
			country_exists = KOR
			KOR = {
				has_opinion = {
					target = NKO
					value > 69
				}
				custom_trigger_tooltip = {
					tooltip = NKO_KOR_hostile_action_tt
					NOT = { has_opinion_modifier = recent_actions_negative }
				}
			}
		}

		completion_reward = {
			add_ideas = NKO_common_trade_belt_of_korea
			KOR = {
				add_ideas = NKO_common_trade_belt_of_korea
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_common_trade_belt_of_korea executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_cease_propaganda_broadcasts_yo_jong
		icon = national_unity

		x = -2
		y = 4
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_inter_korean_summit }
		cost = 10


		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }

		available = {
			NOT = { has_war_with = KOR }
			country_exists = KOR
			KOR = {
				has_opinion = {
					target = NKO
					value > -76
				}
				custom_trigger_tooltip = {
					tooltip = NKO_KOR_hostile_action_tt
					NOT = { has_opinion_modifier = recent_actions_negative }
				}
			}
			OR = {
				KOR = { KOR_has_progressive_government = yes }
				KOR = {
					custom_trigger_tooltip = {
						tooltip = KOR_democratic_party_governing_tt
						is_in_array = { ruling_party = 3  }
					}
				}
				NKO = { has_government = democratic }
				NKO = { has_government = neutrality }
			}
		}

		completion_reward = {
			KOR = {
				remove_ideas = KOR_northern_propaganda_broadcasts
				remove_opinion_modifier = {
					target = NKO
					modifier = propaganda_broadcasts
				}
				support_for_reunification_2 = yes
				add_war_support = -0.02
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_cease_propaganda_broadcasts_yo_jong executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_conciliatory_rhetoric
		icon = concessions

		x = -3
		y = 5
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_cease_propaganda_broadcasts_yo_jong }
		prerequisite = { focus = NKO_close_DMZ }
		cost = 10

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }

		available = {
			NOT = { has_war_with = KOR }
			country_exists = KOR
		}

		completion_reward = {
			add_opinion_modifier = {
				target = KOR
				modifier = diplomatic_proximity
			}
			KOR = {
				add_opinion_modifier = {
					target = NKO
					modifier = diplomatic_proximity
				}
				support_for_reunification_2 = yes
				add_war_support = -0.02
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_conciliatory_rhetoric executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_offer_negotiations_yo_jong
		icon = DPRK-ROK_shake_flags

		x = -3
		y = 6
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_conciliatory_rhetoric }
		prerequisite = { focus = NKO_close_DMZ }
		cost = 10

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }

		available = {
			NOT = { has_war_with = KOR }
			country_exists = KOR
			KOR = {
				has_opinion = {
					target = NKO
					value > -51
				}
				custom_trigger_tooltip = {
					tooltip = NKO_KOR_hostile_action_tt
					NOT = { has_opinion_modifier = recent_actions_negative }
				}
			}
			OR = {
				KOR = { KOR_has_progressive_government = yes }
				KOR = {
					custom_trigger_tooltip = {
						tooltip = KOR_democratic_party_governing_tt
						is_in_array = { ruling_party = 3  }
					}
				}
				NKO = { has_government = democratic }
				NKO = { has_government = neutrality }
			}
		}

		completion_reward = {
			every_country = {
				news_event = {
					id = korea.6
				}
			}
			custom_effect_tooltip = negotiation_result_tt
			hidden_effect = {
				KOR = {
					country_event = {
						id = korea.33
						days = 30
					}
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_offer_negotiations executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_propose_north_led_unification_yo_jong
		icon = juche

		x = -4
		y = 8
		relative_position_id = NKO_kim_yo_jong
		cost = 10

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }
		prerequisite = { focus = NKO_offer_negotiations_yo_jong }
		mutually_exclusive = { focus = NKO_propose_unity_government_yo_jong }

		available = {
			NOT = { has_war_with = KOR }
			country_exists = KOR
			KOR = {
				has_opinion = {
					target = NKO
					value > -26
				}
				custom_trigger_tooltip = {
					tooltip = NKO_KOR_hostile_action_tt
					NOT = { has_opinion_modifier = recent_actions_negative }
				}
			}
			OR = {
				KOR = { KOR_has_progressive_government = yes }
				KOR = {
					custom_trigger_tooltip = {
						tooltip = KOR_democratic_party_governing_tt
						is_in_array = { ruling_party = 3  }
					}
				}
				NKO = { has_government = democratic }
				NKO = { has_government = neutrality }
			}
			custom_trigger_tooltip = {
				tooltip = NKO_KOR_Koren_War_Ceasefire_tt
				NOT = { has_opinion_modifier = NKO_KOR_Koren_War_Ceasefire }
			}
		}

		completion_reward = {
			set_country_flag = NKO_proposed_north_reunification
			custom_effect_tooltip = negotiation_result_tt
			hidden_effect = {
				KOR = {
					country_event = {
						id = korea.34
						days = 30
					}
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_propose_north_led_unification executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_propose_unity_government_yo_jong
		icon = neutrality

		x = -2
		y = 8
		relative_position_id = NKO_kim_yo_jong
		cost = 10

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }
		prerequisite = { focus = NKO_offer_negotiations_yo_jong }
		mutually_exclusive = { focus = NKO_propose_north_led_unification_yo_jong }

		available = {
			NOT = { has_war_with = KOR }
			country_exists = KOR
			KOR = {
				has_opinion = {
					target = NKO
					value > -26
				}
				custom_trigger_tooltip = {
					tooltip = NKO_KOR_hostile_action_tt
					NOT = { has_opinion_modifier = recent_actions_negative }
				}
			}
			OR = {
				KOR = { KOR_has_progressive_government = yes }
				KOR = {
					custom_trigger_tooltip = {
						tooltip = KOR_democratic_party_governing_tt
						is_in_array = { ruling_party = 3  }
					}
				}
				NKO = { has_government = democratic }
				NKO = { has_government = neutrality }
			}
			custom_trigger_tooltip = {
				tooltip = NKO_KOR_Koren_War_Ceasefire_tt
				NOT = { has_opinion_modifier = NKO_KOR_Koren_War_Ceasefire }
			}
		}

		completion_reward = {
			set_country_flag = NKO_proposed_unity_reunification
			custom_effect_tooltip = negotiation_result_tt
			hidden_effect = {
				KOR = {
					country_event = {
						id = korea.34
						days = 30
					}
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_propose_unity_government_yo_jong executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_pave_the_way_for_reunification_yo_jong
		icon = legislation

		x = -3
		y = 9
		relative_position_id = NKO_kim_yo_jong
		cost = 10

		available = {
			NOT = { has_war_with = KOR }
			country_exists = KOR
			KOR = {
				has_opinion = {
					target = NKO
					value > -1
				}
				custom_trigger_tooltip = {
					tooltip = NKO_KOR_hostile_action_tt
					NOT = { has_opinion_modifier = recent_actions_negative }
				}
			}
			OR = {
				KOR = { KOR_has_progressive_government = yes }
				KOR = {
					custom_trigger_tooltip = {
						tooltip = KOR_democratic_party_governing_tt
						is_in_array = { ruling_party = 3  }
					}
				}
				NKO = { has_government = democratic }
				NKO = { has_government = neutrality }
			}
			KOR = {
				OR = {
					custom_trigger_tooltip = {
						tooltip = KOR_agreed_NKO_led_reunification_tt
						has_country_flag = KOR_agreed_NKO_led_reunification
					}
					custom_trigger_tooltip = {
						tooltip = NKO_agreed_neutral_reunification_tt
						has_country_flag = KOR_agreed_neutral_reunification
					}
				}
			}
		}

		search_filters = { FOCUS_FILTER_KOREAN_PENINSULA }
		prerequisite = { focus = NKO_propose_unity_government_yo_jong focus = NKO_propose_north_led_unification_yo_jong }

		completion_reward = {
			annex_country = {
				target = KOR
				transfer_troops = yes
			}
			add_ideas = KOR_reunification_shocks
			add_ideas = KOR_divided_military
			add_ideas = KOR_mass_migration
			add_ideas = KOR_fragmented_governance
			add_ideas = KOR_uneven_economy
			decrease_economic_growth = yes
			set_country_flag = korea_peninsula_reunited
			log = "[GetDateText]: [This.GetName]: focus NKO_pave_the_way_for_reunification_yo_jong executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_superiority_of_DPRK_model
		icon = align_to_north_korea

		x = -6
		y = 3
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_for_well_being_of_people }
		prerequisite = { focus = NKO_for_peace_of_people }
		cost = 10

		search_filters = { FOCUS_FILTER_FOREIGN_POLICY }

		completion_reward = {
			every_other_country = {
				add_opinion_modifier = {
					target = NKO
					modifier = friendship
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_superiority_of_DPRK_model executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_strong_prosperous_nation
		icon = kim_il_sung

		x = -6
		y = 4
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_superiority_of_DPRK_model }
		cost = 10

		search_filters = { FOCUS_FILTER_INFLUENCE }

		completion_reward = {
			every_other_country = {
				limit = {
					NOT = {
						has_idea = Non_State_Actor
						has_idea = Lacks_International_Recognition
						has_idea = rival_government
					}
				}
				set_temp_variable = { percent_change = 0.5 }
				set_temp_variable = { tag_index = NKO.id }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_strong_prosperous_nation executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_affirmative_politics
		icon = women_issues

		x = 1
		y = 1
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_kim_yo_jong }
		cost = 10

		completion_reward = {
			add_popularity = {
				ideology = communism
				popularity = 0.05
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_affirmative_politics executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_women_in_grassroots_cadres
		icon = military_women

		x = 0
		y = 2
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_affirmative_politics }
		cost = 10

		search_filters = { FOCUS_FILTER_SOCIAL_CONSERVATISM }

		completion_reward = {
			custom_effect_tooltip = NKO_agricultural_reform_tip
			if = {
				limit = {
					has_completed_focus = NKO_affirmative_politics
					has_completed_focus = NKO_women_in_workers
				}
				if = {
					limit = { has_idea = closed_nation }
					swap_ideas = {
						remove_idea = closed_nation
						add_idea = censored_nation_07
					}
				}
				else = {
					CHI_censored_nation_level_up = yes
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_women_in_grassroots_cadres executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_women_in_workers
		icon = women_in_the_economy

		x = 2
		y = 2
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_affirmative_politics }
		cost = 10

		search_filters = { FOCUS_FILTER_SOCIAL_CONSERVATISM }

		completion_reward = {
			custom_effect_tooltip = NKO_agricultural_reform_tip
			if = {
				limit = {
					has_completed_focus = NKO_affirmative_politics
					has_completed_focus = NKO_women_in_grassroots_cadres
				}
				if = {
					limit = { has_idea = closed_nation }
					swap_ideas = {
						remove_idea = closed_nation
						add_idea = censored_nation_07
					}
				}
				else = {
					CHI_censored_nation_level_up = yes
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_women_in_workers executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_promote_younger_cadres
		icon = communism3

		x = 1
		y = 3
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_women_in_grassroots_cadres }
		prerequisite = { focus = NKO_women_in_workers }
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }

		completion_reward = {
			if = {
				limit = { has_idea = closed_nation }
				swap_ideas = {
					remove_idea = closed_nation
					add_idea = censored_nation_07
				}
			}
			else = {
				CHI_censored_nation_level_up = yes
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_promote_younger_cadres executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_abolish_life_time_system_for_Cadres_in_leadership
		icon = iron_rice_bowl

		x = 0
		y = 4
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_promote_younger_cadres }
		cost = 10

		search_filters = { FOCUS_FILTER_SOCIAL_CONSERVATISM }

		completion_reward = {
			if = {
				limit = { has_idea = closed_nation }
				swap_ideas = {
					remove_idea = closed_nation
					add_idea = censored_nation_07
				}
			}
			else = {
				CHI_censored_nation_level_up = yes
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_abolish_life_time_system_for_Cadres_in_leadership executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_collective_leadership_system
		icon = Laurel_Slovakia

		x = 0
		y = 6
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_abolish_life_time_system_for_Cadres_in_leadership }
		cost = 10

		search_filters = { FOCUS_FILTER_SOCIAL_CONSERVATISM }

		completion_reward = {
			if = {
				limit = { has_idea = closed_nation }
				swap_ideas = {
					remove_idea = closed_nation
					add_idea = censored_nation_07
				}
			}
			else = {
				CHI_censored_nation_level_up = yes
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_collective_leadership_system executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_grassroots_self_governance_system
		icon = revolutionary_government

		x = 2
		y = 4
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_promote_younger_cadres }
		cost = 10

		search_filters = { FOCUS_FILTER_SOCIAL_CONSERVATISM }

		completion_reward = {
			if = {
				limit = { has_idea = closed_nation }
				swap_ideas = {
					remove_idea = closed_nation
					add_idea = censored_nation_07
				}
			}
			else = {
				CHI_censored_nation_level_up = yes
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_grassroots_self_governance_system executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_chinese_style_democracy
		icon = authoritarian_democracy

		x = 2
		y = 6
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_grassroots_self_governance_system }
		cost = 10

		search_filters = { FOCUS_FILTER_SOCIAL_CONSERVATISM }

		completion_reward = {
			if = {
				limit = { has_idea = closed_nation }
				swap_ideas = {
					remove_idea = closed_nation
					add_idea = censored_nation_07
				}
			}
			else = {
				CHI_censored_nation_level_up = yes
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_chinese_style_democracy executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_allow_donju_to_join_WPK
		icon = donju

		x = 1
		y = 7
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_chinese_style_democracy }
		prerequisite = { focus = NKO_collective_leadership_system }
		cost = 10

		search_filters = { FOCUS_FILTER_INTERNAL_FACTION }

		completion_reward = {
			set_temp_variable = { temp_opinion = 10 }
			change_the_donju_opinion = yes
			increase_corruption = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_allow_donju_to_join_WPK executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_intra_party_democracy
		icon = democratic_communism

		x = 1
		y = 9
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_allow_donju_to_join_WPK }
		cost = 10

		completion_reward = {
			if = {
				limit = { has_idea = closed_nation }
				swap_ideas = {
					remove_idea = closed_nation
					add_idea = censored_nation_07
				}
			}
			else = {
				CHI_censored_nation_level_up = yes
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_intra_party_democracy executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_agricultural_reform
		icon = wheat_field

		x = 5
		y = 1
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_kim_yo_jong }
		cost = 10

		completion_reward = {
			add_political_power = 50
			log = "[GetDateText]: [This.GetName]: focus NKO_agricultural_reform executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_self_employed_agriculture
		icon = farming

		x = 4
		y = 2
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_agricultural_reform }
		mutually_exclusive = { focus = NKO_expand_autonomy_of_agricultural_cooperatives }
		cost = 10

		search_filters = { FOCUS_FILTER_INTERNAL_FACTION }
		completion_reward = {
			increase_Free_Market_Economy = yes
			set_temp_variable = { temp_opinion = -5 }
			change_communist_cadres_opinion = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_self_employed_agriculture executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_expand_autonomy_of_agricultural_cooperatives
		icon = mechanized_agriculture

		x = 6
		y = 2
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_agricultural_reform }
		mutually_exclusive = { focus = NKO_self_employed_agriculture }
		cost = 10

		completion_reward = {
			add_political_power = -50
			increase_social_spending = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_expand_autonomy_of_agricultural_cooperatives executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_increase_agricultural_tax
		icon = focus_trade_republic

		x = 4
		y = 3
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_self_employed_agriculture }
		mutually_exclusive = { focus = NKO_decrease_agricultural_tax }
		cost = 10

		search_filters = { FOCUS_FILTER_INTERNAL_FACTION }
		completion_reward = {
			set_temp_variable = { pop_change = 3 }
			modify_population_tax_rate_effect = yes
			set_temp_variable = { temp_opinion = -5 }
			change_communist_cadres_opinion = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_increase_agricultural_tax executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_decrease_agricultural_tax
		icon = house

		x = 6
		y = 3
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_self_employed_agriculture focus = NKO_expand_autonomy_of_agricultural_cooperatives }
		mutually_exclusive = { focus = NKO_increase_agricultural_tax }
		cost = 10

		completion_reward = {
			set_temp_variable = { pop_change = -3 }
			modify_population_tax_rate_effect = yes
			add_stability = 0.02
			log = "[GetDateText]: [This.GetName]: focus NKO_decrease_agricultural_tax executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_market_economy_system_pilot
		icon = market_economy

		x = 5
		y = 5
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_increase_agricultural_tax focus = NKO_decrease_agricultural_tax }
		cost = 10

		search_filters = { FOCUS_FILTER_EXPENDITURE }

		completion_reward = {
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			increase_Free_Market_Economy = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_market_economy_system_pilot executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_national_ownership_reform
		icon = market_socialism

		x = 4
		y = 7
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_market_economy_system_pilot }
		cost = 10

		search_filters = { FOCUS_FILTER_INTERNAL_FACTION }

		completion_reward = {
			increase_Free_Market_Economy = yes
			set_temp_variable = { temp_opinion = -5 }
			change_communist_cadres_opinion = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_national_ownership_reform executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_complementary_to_state_owned_economy
		icon = economic_prosperity2

		x = 6
		y = 7
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_market_economy_system_pilot }
		cost = 10

		search_filters = { FOCUS_FILTER_INTERNAL_FACTION }

		completion_reward = {
			increase_Free_Market_Economy = yes
			set_temp_variable = { temp_opinion = -5 }
			change_communist_cadres_opinion = yes
			log = "[GetDateText]: [This.GetName]: focus NKO_complementary_to_state_owned_economy executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_jucheised_market_economy
		icon = juche_market_economy

		x = 5
		y = 9
		relative_position_id = NKO_kim_yo_jong

		prerequisite = { focus = NKO_national_ownership_reform focus = NKO_complementary_to_state_owned_economy }
		cost = 10

		search_filters = { FOCUS_FILTER_INTERNAL_FACTION }

		completion_reward = {
			increase_Free_Market_Economy = yes
			set_temp_variable = { temp_opinion = -5 }
			change_communist_cadres_opinion = yes
			if = {
				limit = { has_idea = Socialist_Market_Economy }
				swap_ideas = {
					remove_idea = Socialist_Market_Economy
					add_idea = NKO_Juche_market_economy
				}
			}
			if = {
				limit = { has_idea = Socialist_Market_Economy_free_1 }
				swap_ideas = {
					remove_idea = Socialist_Market_Economy_free_1
					add_idea = NKO_Juche_market_economy
				}
			}
			if = {
				limit = { has_idea = Socialist_Market_Economy_free_2 }
				swap_ideas = {
					remove_idea = Socialist_Market_Economy_free_2
					add_idea = NKO_Juche_market_economy
				}
			}
			if = {
				limit = { has_idea = Free_Market_Economy }
				swap_ideas = {
					remove_idea = Free_Market_Economy
					add_idea = NKO_Juche_market_economy
				}
			}
			if = {
				limit = { has_idea = Socialist_Market_Economy_planned_1 }
				swap_ideas = {
					remove_idea = Socialist_Market_Economy_planned_1
					add_idea = NKO_Juche_market_economy
				}
			}
			if = {
				limit = { has_idea = Socialist_Market_Economy_planned_2 }
				swap_ideas = {
					remove_idea = Socialist_Market_Economy_planned_2
					add_idea = NKO_Juche_market_economy
				}
			}
			if = {
				limit = { has_idea = Planned_Economy }
				swap_ideas = {
					remove_idea = Planned_Economy
					add_idea = NKO_Juche_market_economy
				}
			}
			log = "[GetDateText]: [This.GetName]: focus NKO_jucheised_market_economy executed"
		}

		ai_will_do = {
			base = 1
		}
	}

	shared_focus = {
		id = NKO_happiest_people_in_world
		icon = happest_people_in_world

		x = 0
		y = 10
		relative_position_id = NKO_kim_yo_jong

		cost = 10

		allow_branch = { always = no }
		available = {
			has_completed_focus = NKO_jucheised_market_economy
			has_completed_focus = NKO_strong_prosperous_nation
			has_completed_focus = NKO_intra_party_democracy
			has_completed_focus = NKO_affluent_society
			has_completed_focus = NKO_strong_prosperous_nation
		}

		completion_reward = {
			add_ideas = NKO_idea_happiest_people_in_world
			log = "[GetDateText]: [This.GetName]: focus NKO_happiest_people_in_world executed"
		}

		ai_will_do = {
			base = 1
		}
	}
