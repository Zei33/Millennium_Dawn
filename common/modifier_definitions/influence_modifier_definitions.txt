foreign_influence_defense_modifier = {
	color_type = good
	value_type = percentage
	precision = 2
	category = country
}

foreign_influence_modifier = {
	color_type = good
	value_type = percentage
	precision = 2
	category = country
}

foreign_influence_auto_influence_cap_modifier = {
	color_type = good
	value_type = number
	precision = 1
	category = country
}

influence_coup_modifier = {
	color_type = good
	value_type = percentage
	precision = 1
	category = country
}

foreign_influence_continent_modifier = {
	color_type = good
	value_type = percentage
	precision = 2
	category = country
}

foreign_influence_home_continent_modifier = {
	color_type = good
	value_type = percentage
	precision = 2
	category = country
}

foreign_influence_monthly_domestic_independence_gain_modifier = {
	color_type = good
	value_type = number
	precision = 3
	category = country
}

foreign_influence_monthly_domestic_independence_gain_factor = {
	color_type = good
	value_type = percentage
	precision = 3
	category = country
}
