
ai_focus_defense_USA = {
	research = {
		defensive = 5.0
		CAT_artillery = 5.0
		CAT_sam = 5.0
	}
}

ai_focus_aggressive_USA = {
	research = {
		offensive = 5.0
		CAT_armor = 5.0
	}
}

ai_focus_war_production_USA = {
	research = {
		CAT_nfibers = 10.0
		CAT_3d = 10.0
		CAT_ai = 4.0
		CAT_genes = 4.0
	}
}

ai_focus_military_equipment_USA = {
	research = {
		CAT_inf_wep = 25.0
		CAT_nvg = 5.0
		CAT_cnc = 10.0
		CAT_support_weapons = 10.0
		CAT_special_forces = 5.0

		CAT_at = 15.0
		CAT_aa = 15.0

		CAT_artillery = 8.0
	}
}

ai_focus_military_advancements_USA = {
	research = {
		CAT_land_doctrine = 5
		CAT_western = 40
		CAT_marine = 30
		CAT_airborne = 20
		CAT_tech_doctrines = 10
		CAT_offensive_doctrine = 20
		CAT_defensive_doctrine = 10
		CAT_unconventional = 10
		CAT_training = 5
		CAT_equipment_doctrines = 40

		CAT_l_drone = 1.0
		CAT_nvg = 15.0

		CAT_armor = 15.0
		CAT_mbt = 2.0
		CAT_apc = 4.0
		CAT_ifv = 4.0
		CAT_rec_tank = 2.0
		CAT_util = 10.0

		CAT_heli = 5.0

		# AI Research SAM Missiles only. It's the only one they use so it's important to keep up
		CAT_sam = 5.0
	}
}

ai_focus_peaceful_USA = {
	research = {
		CAT_industry = 10.0
		CAT_ai = 20.0
		CAT_nfibers = 15.0
		CAT_3d = 15.0
		CAT_internet_tech = 18.0
		CAT_computing_tech = 15.0
		CAT_construction_tech = 20.0
		CAT_excavation_tech = 15.0
		CAT_genes = 12.0
		CAT_fuel_oil = 20.0
		CAT_infrastructure = 10
		CAT_nuclear_reactors = 15
		CAT_renewable = 10
	}
}

ai_focus_naval_USA = {
	research = {
		CAT_blue_water_navy = 10.0
		CAT_naval_eqp = 15.0
		CAT_naval_misc = 10.0

		CAT_n_cruiser = 2.0
		CAT_m_cruiser = 2.0
		CAT_destroyer = 4.0
		CAT_frigate = 8.0
		CAT_corvette = 8.0
		Cat_TRANS_SHIP = 6.0

		CAT_n_cv = 10.0
		CAT_cv = 6.0
		CAT_lha = 8.0
		CAT_lpd = 4.0

		CAT_d_sub = 2.0
		CAT_atk_sub = 6.0
		CAT_m_sub = 6.0
	}
}

ai_focus_naval_air_USA = {
	research = {
		CAT_naval_air = 1.0
	}
}

ai_focus_aviation_USA = {
	research = {
		CAT_air_doctrine = 10.0

		CAT_air_eqp = 15.0
		CAT_air_spc = 8.0
		CAT_air_wpn = 8.0

		CAT_h_air = 5.0
		CAT_str_bomber = 7.5
		CAT_cas = 10.0
		CAT_trans_plane = 5.0

		CAT_heli = 2.0
		CAT_trans_heli = 15.0 # NOTE: Is not CAT_air_eqp
		CAT_atk_heli = 3.0

		CAT_fighter = 6.0
		CAT_mr_fighter = 7.0
		CAT_s_fighter = 2.0
		CAT_l_s_fighter = 0.0
		CAT_as_fighter = 2.0
		CAT_a_uav = 25.0
		CAT_sam = 5.0
	}
}
