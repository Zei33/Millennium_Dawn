NOR_kongsberg_materiel_manufacturer = {
	allowed = { original_tag = NOR }
	icon = GFX_idea_kongsberg-logo
	name = NOR_kongsberg_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

NOR_stx_europe_naval_manufacturer = {
	allowed = { original_tag = NOR }
	icon = GFX_idea_STX_Europe_logo
	name = NOR_stx_europe_naval_manufacturer
	include = generic_naval_equipment_organization
}

NOR_umoe_mandal_naval_manufacturer = {
	allowed = { original_tag = NOR }
	icon = GFX_idea_UMOE_Mandal
	name = NOR_umoe_mandal_naval_manufacturer
	include = generic_naval_light_equipment_organization
}