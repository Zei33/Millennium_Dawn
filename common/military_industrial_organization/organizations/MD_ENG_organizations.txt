ENG_bae_land_systems_materiel_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_BAE_Systems
	name = ENG_bae_land_systems_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

ENG_qinetiq_materiel_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_QinetiQ
	name = ENG_bae_land_systems_materiel_manufacturer
	include = generic_aa_at_equipment_organization
}

ENG_bae_land_systems_tank_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_BAE_Systems
	name = ENG_bae_land_systems_tank_manufacturer
	include = generic_tank_equipment_organization
}

ENG_thales_tank_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_Thales
	name = ENG_thales_tank_manufacturer
	include = generic_specialized_tank_organization
}

ENG_agustawestland_tank_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_AgustaWestland
	name = ENG_agustawestland_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

ENG_bae_aerospace_aircraft_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_BAE_Systems
	name = ENG_bae_aerospace_aircraft_manufacturer
	include = generic_air_equipment_organization
}

ENG_bae_systems_marine_naval_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_BAE_Systems
	name = ENG_bae_systems_marine_naval_manufacturer
	include = generic_naval_equipment_organization
}

ENG_bae_systems_submarine_naval_manufacturer = {
	allowed = { original_tag = ENG }
	icon = GFX_idea_BAE_Systems
	name = ENG_bae_systems_submarine_naval_manufacturer
	include = generic_sub_equipment_organization
}