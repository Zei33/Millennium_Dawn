EGY_kader_factory_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_kader_factory_tank_manufacturer
	include = generic_tank_equipment_organization
}

EGY_helwan_machine_tools_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_helwan_machine_tools_tank_manufacturer
	include = generic_tank_equipment_organization
}

EGY_abu_zaabal_tank_factory_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_Abu_Zaabal_Factory
	name = EGY_abu_zaabal_tank_factory_tank_manufacturer
	include = generic_tank_equipment_organization
}

EGY_abhco_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_abhco_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

EGY_kader_factory_materiel_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_kader_factory_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

EGY_maadi_arms_materiel_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_maadi_arms_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

EGY_aoi_aircraft_aircraft_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_aoi_aircraft_aircraft_manufacturer
	include = generic_air_equipment_organization
}

EGY_alexandria_shipyard_naval_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_Alexandria_Shipyard
	name = EGY_alexandria_shipyard_naval_manufacturer
	include = generic_naval_equipment_organization
}

