#Made by batalshi
autonomy_state = {
	id = autonomy_cuban_confederation

	default = no
	is_puppet = no
	use_overlord_color = yes
	min_freedom_level = 0.0
	manpower_influence = 0.1

	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = yes
		can_send_volunteers = yes
	}

	modifier = {
		cic_to_overlord_factor = 0
		mic_to_overlord_factor = 0.75
		can_master_build_for_us = 1
		autonomy_manpower_share = 0.1
		extra_trade_to_overlord_factor = 0.8
		overlord_trade_cost_factor = -0.8
		research_sharing_per_country_bonus_factor = -0.8
	}

	ai_subject_wants_higher = {
		factor = 0.0
	}

	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = yes
	}

	allowed = {
		OVERLORD = {
			original_tag = CUB
		}
		OR = {
		original_tag = HAI
		original_tag = DOM
		original_tag = JAM
		original_tag = COL
		original_tag = PTR
		}
		has_country_flag = antillies_conf_agree
	}

	can_take_level = { always = no }
	can_lose_level = { always = no }
}
