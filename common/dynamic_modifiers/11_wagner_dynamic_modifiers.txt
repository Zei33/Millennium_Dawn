#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

#Wagner DYNAMIC MODIFIERS
# Author(s): Lord Bogdanoff
WAG_wagner_politic_modifier = {
	icon = "GFX_idea_wagner_armies"
	enable = {
		original_tag = WAG
		has_completed_focus = WAG_Utkins_Orchestra
	}
	political_power_factor = WAG_wagner_politic_political_power_factor
	drift_defence_factor = WAG_wagner_politic_drift_defence_factor
	nationalist_drift = WAG_wagner_politic_nationalist_drift
	stability_factor = WAG_wagner_politic_stability_factor
	conversion_cost_civ_to_mil_factor = WAG_wagner_politic_conversion_cost_civ_to_mil_factor
	conscription_factor = WAG_wagner_politic_conscription_factor
	send_volunteers_tension = WAG_wagner_politic_send_volunteers_tension
	max_planning_factor = WAG_wagner_politic_max_planning_factor
	planning_speed = WAG_wagner_politic_planning_speed
	resistance_damage_to_garrison = WAG_wagner_politic_resistance_damage_to_garrison
	resistance_growth = WAG_wagner_politic_resistance_growth
	bureaucracy_cost_multiplier_modifier = WAG_wagner_politic_bureaucracy_cost_multiplier_modifier
}
WAG_Bunt_modifer = {
	enable = { 
		SOV = {
			has_war_with = WAG
		}
	 }
	icon = wagner_armies
	army_speed_factor = 0.2
	army_defence_factor = 0.2
	army_attack_speed_factor = 0.2
	army_org = 10
}
RUS_WAG_Bunt_modifer = {
	enable = { 
		SOV = {
			has_war_with = WAG
		}
	 }
	icon = wagner_armies
	army_speed_factor = -0.2
	army_defence_factor = -0.2
	army_attack_speed_factor = -0.2
	army_org = -1000
}