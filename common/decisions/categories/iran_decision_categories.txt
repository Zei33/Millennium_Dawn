################
##### PER ######
################

PER_Iranian_elections_decision_category = {
	icon = GFX_decision_category_generic_propaganda

	priority = 200

	allowed = {
		original_tag = PER
	}

	scripted_gui = PER_elections_mechanic_GUI

	visible = {
		original_tag = PER
		has_country_flag = PER_election_enabled
		OR = {
			emerging_hardline_shiite_are_in_power = yes
			emerging_moderate_shiite_are_in_power = yes
		}
	}
}
PER_Iranian_president_info = {
	icon = GFX_decision_category_generic_propaganda

	priority = 100

	allowed = {
		original_tag = PER
	}

	scripted_gui = PER_president_info_GUI

	visible = {
		original_tag = PER
		OR = {
			emerging_hardline_shiite_are_in_power = yes
			emerging_moderate_shiite_are_in_power = yes
		}
	}
}
PER_soleimani = {
	icon = GFX_decision_category_irgc

	picture = GFX_decision_category_picture_harash_revenge
	priority = 50

	allowed = {
		original_tag = PER
	}

	visible = {
		original_tag = PER
		has_country_flag = harash_revenge
	}
}
PER_iraq_war_compensation = {
	icon = GFX_decision_category_generic_arms_trade

	priority = 100

	allowed = {
		original_tag = PER
	}

	visible = {
		original_tag = PER
		NOT = {
			has_country_flag = PER_war_compensation_invisible
			has_war_with = IRQ
		}
	}
}

PER_resitance_axis = {
	icon = GFX_decision_generic_civil_support

	picture = GFX_decision_category_picture_resistance_axis
	priority = 150

	allowed = {
		original_tag = PER
	}

	visible = {
		is_in_faction = yes
		is_faction_leader = yes
		has_government = communism
		has_idea = shia
		has_completed_focus = PER_axis_of_resistance
	}
}
PER_IRGC_attack_terrorists = {
	icon = GFX_decision_category_irgc

	priority = 30

	allowed = {
		original_tag = PER
	}

	scripted_gui = PER_IRGC_attack_GUI

	visible_when_empty = yes

	visible = {
		original_tag = PER
		has_completed_focus = PER_islamic_revolutionary_guards_corps
		NOT = {
			OR = {
				exists = no
				ISR = {
					is_ally_with = PER
				}
			}

		}
	}
}
PER_uranium = {
	icon = GFX_decision_steel

	priority = 10

	allowed = {
		original_tag = PER
	}

	visible = {
		#original_tag = PER
		has_completed_focus = PER_search_for_uranium
	}
}

PER_streamline_resource_input = {
	icon = GFX_decision_steel

	priority = 10

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_streamline_resource_input
		has_country_flag = PER_allow_resource_stuff
	}
}

PER_brain_drain_removal = {
	icon = political_actions

	priority = 30

	allowed = {
		original_tag = PER
	}

	visible = {
		original_tag = PER
		has_country_flag = PER_incapable_brain_drain
	}
}
PER_self_humiliation = {
	icon = generic_propaganda

	priority = 150

	scripted_gui = PER_islamic_propaganda

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_start_the_islamic_propaganda
		OR = {
			has_idea = PER_self_humiliation_idea_1
			has_idea = PER_self_humiliation_idea_2
			has_idea = PER_self_humiliation_idea_3
			has_idea = PER_self_humiliation_idea_4
			has_idea = PER_self_humiliation_idea_5
			has_idea = PER_self_humiliation_idea_6
			has_idea = PER_self_humiliation_idea_7
			has_idea = PER_self_humiliation_idea_8
			has_idea = PER_self_humiliation_idea_9
			has_idea = PER_self_humiliation_idea_10
		}
	}
}

PER_border_fortification = {
	icon = decision_fortification

	priority = 10

	allowed = {
		original_tag = PER
	}

	visible = {
		original_tag = PER
		has_completed_focus = PER_defensive_infrastructure
	}
}

PER_turkish_war = {
	icon = GFX_decision_category_generic_foreign_policy

	priority = 10

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_rush_to_the_turkey
		has_war_with = TUR
	}
}
PER_resistance_economy = {
	icon = null

	priority = 40

	allowed = {
		original_tag = PER
	}

	visible = {
		has_country_flag = PER_resistance_econ
	}
}
PER_domestic_politics = {
	icon = political_actions
	priority = 200

	allowed = {
		original_tag = PER
	}
	visible_when_empty = yes
	visible = {
		NOT = {
			OR = {
				emerging_hardline_shiite_are_in_power = yes
				emerging_moderate_shiite_are_in_power = yes
			}
		}
	}
}
PER_the_second_revolution = {
	icon = GFX_decision_generic_prepare_civil_war
	priority = 120

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_the_revolution
	}
}
PER_resistance_movement = {
	icon = GFX_decision_generic_prepare_civil_war
	scripted_gui = Iran_civil_war_gui
	priority = 180
	visible_when_empty = yes
	allowed = {
		original_tag = PER
	}
	visible = {
		check_variable = { from.PER_state_modifier_var < 27 }
	}
}
PER_balance_the_army = {
	icon = GFX_decision_generic_prepare_civil_war
	scripted_gui = Iran_factions_civil_war_gui
	priority = 127

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_the_revolution
		has_country_flag = PER_persian_revolution
	}
}
PER_gcc_response = {
	icon = GFX_decision_generic_form_nation
	priority = 120
	scripted_gui = Iran_cold_war_gui
	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_a_run_for_their_money
		has_country_flag = PER_cold_war
		OR = {
			neutrality_neutral_conservatism_are_in_power = yes
			nationalist_monarchists_are_in_power = yes
			western_liberals_are_in_power = yes
			western_social_democrats_are_in_power = yes
			western_conservatism_are_in_power = yes
			western_autocrats_are_in_power = yes
		}
	}
}
PER_mek_popularity = {
	icon = GFX_decision_generic_political_discourse
	priority = 120

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_rajavi_cult
		emerging_anarchist_communism_are_in_power = yes
	}
}
PER_all_that_was_ours = {
	icon = GFX_decision_generic_political_discourse
	priority = 120

	allowed = {
		original_tag = PER
	}
	scripted_gui = iranic_unity_gui
	visible = {
		has_completed_focus = PER_diplomatic_approach
		has_country_flag = PER_our_stuff
		has_government = nationalist
		nationalist_right_wing_populists_are_in_power = yes
	}
}
PER_iraqi_kurds = {
	icon = GFX_decision_generic_scorched_earth
	priority = 120

	allowed = {
		original_tag = PER
	}

	visible = {
		has_country_flag = PER_iraqi_kurdish
		nationalist_right_wing_populists_are_in_power = yes
	}
}
PER_cento_expand = {
	icon = GFX_decision_generic_form_nation
	priority = 110

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_cento_expansion
		OR = {
			neutrality_neutral_conservatism_are_in_power = yes
			nationalist_monarchists_are_in_power = yes
			western_liberals_are_in_power = yes
			western_social_democrats_are_in_power = yes
			western_conservatism_are_in_power = yes
			western_autocrats_are_in_power = yes
		}
	}
	visible_when_empty = yes
}
PER_revolutionary_fatigue_save = {
	icon = GFX_decision_generic_form_nation
	priority = 160

	allowed = {
		original_tag = PER
	}
	scripted_gui = PER_revolutionary_recovery
	visible = {
		has_country_flag = PER_starting_the_revolution
	}
}
PER_revolutionary_council_formation = {
	icon = GFX_decision_generic_form_nation
	priority = 160

	allowed = {
		original_tag = PER
	}
	scripted_gui = Iran_factions_civil_war_gui
	visible = {
		has_country_flag = PER_starting_the_revolution
		has_country_flag = PER_some_flag_here
	}
	visible_when_empty = yes
}
PER_majlis = {
	icon = GFX_decision_generic_form_nation
	priority = 190
	scripted_gui = iran_majlis_gui
	allowed = {
		original_tag = PER
	}
	visible = {
		original_tag = PER
	}
	visible_when_empty = yes
}
PER_insurgent_mechanic = {
	icon = GFX_decision_generic_scorched_earth
	priority = 180
	allowed = {
		original_tag = PER
	}
	visible = {
		original_tag = PER
		has_country_flag = PER_artesh_coup
		NOT = {
			has_country_flag = PER_irgc_destroyed
		}
	}
	visible_when_empty = yes
}
PER_artesh_cosmetics = {
	icon = GFX_decision_generic_scorched_earth
	priority = 180

	visible = {
		original_tag = PER
		has_country_flag = PER_artesh_rises
		PER = {
			nationalist_military_junta_are_in_power = yes
		}
	}
	visible_when_empty = yes
}
PER_trade_spies = {
	icon = generic_independence

	priority = 10

	allowed = {
		original_tag = PER
	}

	visible = {
		has_completed_focus = PER_trade_spies_with_money
	}
}
PER_clergy_balance_of_power_category = {
	icon = generic_independence

	priority = 10

	allowed = {
		original_tag = PER
	}
}
PER_iranian_governates = {
	icon = GFX_decision_generic_scorched_earth

	priority = 700

	allowed = {
		original_tag = PER
	}
	visible = {
		PER = {
			NOT = {
				OR = {
					nationalist_military_junta_are_in_power = yes
					emerging_hardline_shiite_are_in_power = yes
					emerging_moderate_shiite_are_in_power = yes
				}
			}
		}
	}
}
PER_Iranian_prime_ministers = {
	icon = GFX_decision_category_generic_propaganda

	priority = 200

	allowed = {
		original_tag = PER
	}

	scripted_gui = Iran_prime_minister_gui
	visible_when_empty = yes
	visible = {
		has_country_flag = PER_iranian_minister_given
	}
}
PER_Iranian_prime_ministers_elections = {
	icon = GFX_decision_category_generic_propaganda

	priority = 200

	allowed = {
		original_tag = PER
	}

	visible_when_empty = yes
	visible = {
		has_country_flag = PER_minister_selection
	}
}
PER_zoroastrian_decisions = {
	priority = 200
	visible_when_empty = yes
	on_map_area = {
		name = PER_PER_zoroastrian_decisions_states_required
		zoom = 250
		targets = { 1147 1039 979 408 1139 1138 399 406 400 1146 977 401 1151 402 1150 1145 403 976 978 407 1073 1148 404 1144 405 1149 }
		target_trigger = {
			original_tag = PER
		}
	}
	visible = {
		has_country_flag = PER_Zoroastrian_spawned
	}

}