SOV_Sovfed_category = {
	allowed = {
		OR = {
			original_tag = SOV
			original_tag = CHE
			original_tag = DPR
			original_tag = LPR
			original_tag = HPR
			original_tag = OPR
			original_tag = BSH
			original_tag = KBK
			original_tag = TAT
			original_tag = TUV
			original_tag = YAK
			original_tag = BRY
			original_tag = KOM
			original_tag = ALT
			original_tag = KHS
			original_tag = KAE
			original_tag = CKK
			original_tag = KHM
			original_tag = YAM
			original_tag = NEE
			original_tag = KCC
			original_tag = ING
			original_tag = ADY
			original_tag = DAG
			original_tag = KLM
			original_tag = CRM
			original_tag = FAR
			original_tag = URA
			original_tag = KUB
			original_tag = GOR
			original_tag = SIB
			original_tag = SOO
			original_tag = FIN
			original_tag = PMR
			original_tag = CHU
			original_tag = MEL
			original_tag = MOV
			original_tag = UDM
			original_tag = MLR
		}
	}
	priority = 100
	icon = GFX_decisions_category_sovfed
}
TAT_tatneft_category = {
	allowed = {
		original_tag = TAT
	}
	priority = 100 
	icon = GFX_decisions_category_tatnfet
	picture = GFX_decision_tatneft_decision
	visible = {
	has_completed_focus = TAT_tatneft
	}
}
SUB_nationalism_category = {
	priority = 90
	icon = GFX_decisions_category_separ
	allowed = {
		OR = {
			original_tag = TAT
			original_tag = BSH
			original_tag = CHE
		}
	}
	visible = {
		ROOT = {
			is_subject = yes
			has_autonomy_state = autonomy_republic_rf
		}
		OR = {
			original_tag = TAT
			original_tag = BSH
			original_tag = CHE
		}
	}
}
SIL_bund_category = {
	priority = 110
	icon = GFX_decisions_category_silesia
	picture = GFX_decision_silesia_decisions
	allowed = {
		OR = {
			original_tag = SOV
			original_tag = CHE
			original_tag = POL
		}
	}
	visible = {
		SOV = { has_country_flag = silesia_start }
		OR = {
			original_tag = SOV
			original_tag = CHE
			original_tag = POL
		}
	}
}
KHM_reunifcation_category = {
	priority = 110
	icon = GFX_decisions_category_khmao
	allowed = {
		original_tag = KHM
	}
	visible = {
		SOV = {
			OR = {	
				exists = no
				NOT = { owns_state = 652 }
			}
		}
		OR = {
		has_idea = KHM_zhir_legacy_rus
		has_idea = KHM_zhir_legacy_rus1
		has_idea = KHM_zhir_legacy_rus2
		has_idea = KHM_zhir_legacy_rus3
		has_idea = KHM_zhir_legacy_rus4
		has_idea = KHM_zhir_legacy_rus5
		}
	}
}
Form_Idel_Ural = {
	priority = 90
	icon = GFX_decisions_category_idel_ural
	picture = GFX_idel_ural_decisions
	allowed = {
		OR = {
		original_tag = BSH
		original_tag = TAT
		}
	}
	visible = {
		OR = {
			original_tag = BSH
			original_tag = TAT
		}
	}
}

