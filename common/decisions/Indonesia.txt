# Author(s): TheUnknownDroid
IND_disasters_mechanic = {
	# Warning Decisions
	IND_disasters_mechanic_532121 = {
		fire_only_once = yes
		activation = { NOT = { has_country_flag = DIS2 } }
		visible = { NOT = { has_country_flag = DIS2 } }
		days_mission_timeout = 1
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_532121"
			country_event = { id = indonesia.5865 hours = 1 }
		}
	}

	IND_disasters_mechanic_1 = { #FLOOD IN KALIMANTAN
		fire_only_once = yes
		activation = { has_idea = IND_rupiah_crisis }
		visible = { always = yes }
		available = {
			640 = {
				dockyard > 0
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_1"
			add_stability = 0.03
			set_country_flag = DIS2
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_1"
			set_country_flag = DIS2
			country_event = { id = indonesia.41 hours = 6 }
		}
	}

	IND_disasters_mechanic_2 = { #FLOOD IN KALIMANTAN
		fire_only_once = yes
		activation = {
			hidden_trigger = { has_country_flag = DIS2 }
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS2
		}
		available = {
			638 = {
				industrial_complex > 1
				bunker > 3
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_2"
			add_stability = 0.03
			set_country_flag = DIS3
			clr_country_flag = DIS2
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_2"
			set_country_flag = DIS3
			clr_country_flag = DIS2
			country_event = { id = indonesia.42 hours = 6 }
		}
	}

	IND_disasters_mechanic_3 = { #FLOOD IN KALIMANTAN
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS3
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS3
		}
		available = {
			636 = {
				infrastructure > 1
				air_base > 0
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_3"
			add_stability = 0.03
			set_country_flag = DIS4
			clr_country_flag = DIS3
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_3"
			set_country_flag = DIS4
			clr_country_flag = DIS3
			country_event = { id = indonesia.43 hours = 6 }
		}
	}

	IND_disasters_mechanic_4 = { #FLOOD IN KALIMANTAN
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS4
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS4
		}
		available = {
			634 = {
				radar_station > 2
				air_base > 5
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_4"
			add_stability = 0.03
			set_country_flag = DIS5
			clr_country_flag = DIS4
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_4"
			set_country_flag = DIS5
			clr_country_flag = DIS4
			country_event = { id = indonesia.44 hours = 6 }
		}
	}

	IND_disasters_mechanic_5 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS5
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS5
		}
		available = {
			635 = {
				bunker > 2
				arms_factory > 1
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_5"
			add_stability = 0.03
			set_country_flag = DIS6
			clr_country_flag = DIS5
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_5"
			set_country_flag = DIS6
			clr_country_flag = DIS5
			IND = {
				country_event = {
					id = indonesia.45
					hours = 6
				}
			}
			# MAKE ACEH DECLARE WAR ON INDONESIA
		}
	}

	IND_disasters_mechanic_6 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS6
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS6
		}

		available = {
			623 = {
				is_owned_and_controlled_by = IND
				rail_way > 4
				offices > 1
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_6"
			add_stability = 0.03
			set_country_flag = DIS7
			clr_country_flag = DIS6
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_6"
			set_country_flag = DIS7
			clr_country_flag = DIS6
			IND = {
				country_event = {
					id = indonesia.46
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_7 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS7
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS7
		}

		available = {
			639 = {
				is_owned_and_controlled_by = IND
				arms_factory > 1
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_7"
			add_stability = 0.03
			set_country_flag = DIS8
			clr_country_flag = DIS7
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_7"
			set_country_flag = DIS8
			clr_country_flag = DIS7
			IND = {
				country_event = {
					id = indonesia.47
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_8 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS8
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS8
		}
		available = {
			631 = {
				is_owned_and_controlled_by = IND
				radar_station > 5
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_8"
			add_stability = 0.03
			set_country_flag = DIS9
			clr_country_flag = DIS8
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_8"
			set_country_flag = DIS9
			clr_country_flag = DIS8
			IND = {
				country_event = {
					id = indonesia.48
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_9 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS9
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS9
		}

		available = {
			637 = {
				is_owned_and_controlled_by = IND
				infrastructure > 1
				naval_base > 3
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_9"
			add_stability = 0.03
			set_country_flag = DIS10
			clr_country_flag = DIS9
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_9"
			set_country_flag = DIS10
			clr_country_flag = DIS9
			IND = {
				country_event = {
					id = indonesia.49
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_10 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS10
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS10
		}

		available = {
			635 = {
				is_owned_and_controlled_by = IND
				internet_station > 0
				supply_node > 0
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_10"
			add_stability = 0.03
			set_country_flag = DIS11
			clr_country_flag = DIS10
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_10"
			set_country_flag = DIS11
			clr_country_flag = DIS10
			IND = {
				country_event = {
					id = indonesia.51
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_11 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS11
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS11
		}

		available = {
			627 = {
				is_owned_and_controlled_by = IND
				industrial_complex > 0
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_11"
			add_stability = 0.03
			set_country_flag = DIS12
			clr_country_flag = DIS11
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_11"
			set_country_flag = DIS12
			clr_country_flag = DIS11
			IND = {
				country_event = {
					id = indonesia.52
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_12 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS12
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS12
		}

		available = {
			640 = {
				is_owned_and_controlled_by = IND
				arms_factory > 1
				industrial_complex > 1
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_12"
			add_stability = 0.03
			set_country_flag = DIS13
			clr_country_flag = DIS12
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_12"
			set_country_flag = DIS13
			clr_country_flag = DIS12
			IND = {
				country_event = {
					id = indonesia.53
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_13 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS13
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS13
		}

		available = {
			629 = {
				is_owned_and_controlled_by = IND
				naval_base > 4
				dockyard > 2
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_13"
			add_stability = 0.03
			set_country_flag = DIS14
			clr_country_flag = DIS13
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_13"
			set_country_flag = DIS14
			clr_country_flag = DIS13
			IND = {
				country_event = {
					id = indonesia.54
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_14 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS14
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS14
		}
		allowed = {
			original_tag = IND
		}
		available = {
			638 = {
				is_owned_and_controlled_by = IND
				air_base > 9
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_14"
			add_stability = 0.03
			set_country_flag = DIS15
			clr_country_flag = DIS14
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_14"
			set_country_flag = DIS15
			clr_country_flag = DIS14
			IND = {
				country_event = {
					id = indonesia.56
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_15 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS15
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS15
		}

		available = {
			639 = {
				is_owned_and_controlled_by = IND
				infrastructure > 2
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_15"
			add_stability = 0.03
			set_country_flag = DIS16
			clr_country_flag = DIS15
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_15"
			set_country_flag = DIS16
			clr_country_flag = DIS15
			IND = {
				country_event = {
					id = indonesia.57
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_16 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS16
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS16
		}

		available = {
			639 = {
				is_owned_and_controlled_by = IND
				offices > 0
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_16"
			add_stability = 0.03
			set_country_flag = DIS17
			clr_country_flag = DIS16
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_16"
			set_country_flag = DIS17
			clr_country_flag = DIS16
			IND = {
				country_event = {
					id = indonesia.58
					hours = 6
				}
			}
		}
	}

	IND_disasters_mechanic_17 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS17
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_country_flag = DIS17
			}

		available = {
			638 = {
				is_owned_and_controlled_by = IND
				arms_factory > 1
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_17"
			add_stability = 0.03
			set_country_flag = DIS18
			clr_country_flag = DIS17
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_17"
			set_country_flag = DIS18
			clr_country_flag = DIS17
			country_event = { id = indonesia.59 hours = 6 }
		}
	}

	IND_disasters_mechanic_18 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = {
				has_country_flag = DIS18
			}
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = {
			has_idea = IND_disaster_mechanic_18
		}

		available = {
			634 = {
				is_owned_and_controlled_by = IND
				synthetic_refinery > 0
				fuel_silo > 2
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_18"
			add_stability = 0.03
			set_country_flag = DIS19
			clr_country_flag = DIS18
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_18"
			set_country_flag = DIS19
			clr_country_flag = DIS18
			country_event = { id = indonesia.61 hours = 6 }
		}
	}

	IND_disasters_mechanic_19 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = { has_country_flag = DIS19 }
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = { has_country_flag = DIS19 }
		available = {
			631 = {
				infrastructure > 4
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_19"
			add_stability = 0.03
			set_country_flag = DIS20
			clr_country_flag = DIS19
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_19"
			set_country_flag = DIS20
			clr_country_flag = DIS19
			country_event = { id = indonesia.62 hours = 6 }
		}
	}

	IND_disasters_mechanic_20 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = { has_country_flag = DIS20 }
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = { has_country_flag = DIS20 }
		available = {
			637 = {
				is_owned_and_controlled_by = IND
				air_base > 4
				industrial_complex > 0
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_20"
			add_stability = 0.03
			set_country_flag = DIS21
			clr_country_flag = DIS20
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_disasters_mechanic_20"
			set_country_flag = DIS21
			clr_country_flag = DIS20
			country_event = { id = indonesia.63 hours = 6 }
		}
	}

	IND_disasters_mechanic_21 = { #RE-INSURGENCY IN ACEH
		fire_only_once = yes
		activation = {
			hidden_trigger = { has_country_flag = DIS21 }
			NOT = {
				has_idea = IND_finished_disaster
			}
		}
		visible = { has_country_flag = D1IS21 }
		available = {
			631 = {
				is_owned_and_controlled_by = IND
				infrastructure > 4
				rail_way > 4
			}
		}
		days_mission_timeout = 450
		is_good = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision complete IND_disasters_mechanic_21"
			add_stability = 0.03
			clr_country_flag = DIS21
			hidden_effect = {
				add_timed_idea = {
					idea = IND_finished_disaster
					days = 100
				}
			}
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout IND_disasters_mechanic_21"
			clr_country_flag = DIS21
			country_event = { id = indonesia.64 hours = 6 }
		}
	}
}

##ETHNIC TENSIONS DECISIONS
##Back Muslims
IND_back_muslim = {
	IND_muslim = {

		visible = {
			has_completed_focus = IND_maluku_back_muslims
		}
		icon = ger_mefo_bills
		fire_only_once = yes
		days_remove = 25
		cost = 50
		modifier = {
			training_time_factor = -0.3
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_muslim"
			639 = { remove_dynamic_modifier = { modifier = IND_islamic_resist } }
			639 = { add_dynamic_modifier = { modifier = IND_islamic_resist2 } }
			set_country_flag = MUS2
		}
	}

	IND_muslim2 = {

		visible = {
			has_completed_focus = IND_maluku_back_muslims
			has_country_flag = MUS2
		}
		icon = generic_nationalism
		fire_only_once = yes
		cost = 50
		days_remove = 25

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_muslim2"
			639 = { remove_dynamic_modifier = { modifier = IND_islamic_resist2 } }
			639 = { add_dynamic_modifier = { modifier = IND_islamic_resist3 } }
			clr_country_flag = MUS2
			set_country_flag = MUS3
		}
	}
	IND_muslim_generic = {
		visible = {
			has_completed_focus = IND_maluku_back_muslims
		}
		icon = generic_prepare_civil_war
		fire_only_once = yes
		cost = 25
		days_remove = 45
		modifier = {
			local_manpower = -0.05
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_muslim_generic"
			set_temp_variable = { treasury_change = 5.65 }
			modify_treasury_effect = yes
			IND = {
				add_equipment_to_stockpile = {
					type = Inf_equipment
					amount = -300
				}
			}
			638 = {
				add_extra_state_shared_building_slots = -1
			}
			639 = {
				add_extra_state_shared_building_slots = 1
			}
		}
	}

	IND_muslim_generic2 = {

		visible = {
			has_completed_focus = IND_maluku_back_muslims
		}
		icon = generic_industry
		fire_only_once = yes
		days_remove = 45
		cost = 25
		modifier = {
			local_manpower = -0.05
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_muslim_generic2"
			set_temp_variable = { treasury_change = 4.65 }
			modify_treasury_effect = yes
			IND = {
				add_equipment_to_stockpile = {
					type = Inf_equipment
					amount = -300
				}
			}
			638 = {
				add_building_construction = {
					type = infrastructure
					level = -1
					instant_build = yes
				}
			}
			639 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			638 = { remove_dynamic_modifier = { modifier = IND_papua_resist  } }
			638 = { add_dynamic_modifier = { modifier = IND_papua_resist_m1  } }
		}
	}
	#
	IND_muslim_generic3 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_muslims
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 25
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_muslim_generic3"
			set_temp_variable = { treasury_change = -2.65 }
			modify_treasury_effect = yes
			639 = {
				add_building_construction = {
					type = bunker
					level = 2
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
			set_temp_variable = { treasury_change = -4.35 }
			modify_treasury_effect = yes
		}
	}
	#
	IND_muslim3 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_muslims
			has_country_flag = MUS3
		}
		icon = generic_army_support
		fire_only_once = yes
		days_remove = 25
		cost = 50
		modifier = {
			training_time_factor = -0.3
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_muslim3"
			639 = { remove_dynamic_modifier = { modifier = IND_islamic_resist3 } }
			639 = { add_dynamic_modifier = { modifier = IND_islamic_resist4 } }
			clr_country_flag = MUS3
			set_country_flag = MUS4
		}
	}
	IND_muslim4 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_muslims
			has_country_flag = MUS4
		}
		icon = generic_political_discourse
		cost = 50
		fire_only_once = yes
		days_remove = 25
		modifier = {
			training_time_factor = -0.3
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_muslim4"
			639 = { remove_dynamic_modifier = { modifier = IND_islamic_resist4 } }
			clr_country_flag = MUS4
			set_country_flag = MUS5
		}
	}
}
##BACK CHRISTIANS
IND_back_papua = {
	IND_papua = {
		allowed = {
			tag = IND
		}
		icon = ger_mefo_bills
		visible = {
			has_completed_focus = IND_maluku_back_chrisitians
		}
		fire_only_once = yes
		days_remove = 35
		modifier = {
			training_time_factor = -0.3
		}
		cost = 50
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_papua"
			638 = { remove_dynamic_modifier = { modifier = IND_papua_resist } }
			638 = { add_dynamic_modifier = { modifier = IND_papua_resist2 } }
			set_country_flag = CHR2
		}
	}
	#
	IND_papua2 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_chrisitians
			has_country_flag = CHR2
		}
		icon = generic_nationalism
		fire_only_once = yes
		days_remove = 35
		cost = 50
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_papua2"
			638 = { remove_dynamic_modifier = { modifier = IND_papua_resist2 } }
			638 = { add_dynamic_modifier = { modifier = IND_papua_resist3 } }
			clr_country_flag = CHR2
			set_country_flag = CHR3
		}
	}
	#
	IND_papua_generic = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_chrisitians
		}
		icon = generic_prepare_civil_war
		fire_only_once = yes
		cost = 25
		days_remove = 45
		modifier = {
			local_manpower = -0.05
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_papua_generic"
			set_temp_variable = { treasury_change = 5.65 }
			modify_treasury_effect = yes
			IND = {
				add_equipment_to_stockpile = {
					type = Inf_equipment
					amount = -300
				}
			}
			639 = {
				add_extra_state_shared_building_slots = -1
			}
			638 = {
				add_extra_state_shared_building_slots = 1
			}
		}
	}
	#
	IND_papua_generic2 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_chrisitians
		}
		icon = generic_industry
		fire_only_once = yes
		cost = 25
		days_remove = 45
		modifier = {
			local_manpower = -0.05
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_papua_generic2"
			set_temp_variable = { treasury_change = 5.65 }
			modify_treasury_effect = yes
			IND = {
				add_equipment_to_stockpile = {
					type = Inf_equipment
					amount = -300
				}
			}
			638 = {
				add_building_construction = {
					type = infrastructure
					level = -1
					instant_build = yes
				}
			}
			639 = { remove_dynamic_modifier = { modifier = IND_islamic_resist  } }
			639 = { add_dynamic_modifier = { modifier = IND_islamic_resist_c1  } }
		}
	}
	#
	IND_papua_generic3 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_chrisitians
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 25
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_papua_generic3"
			set_temp_variable = { treasury_change = -2.65 }
			modify_treasury_effect = yes
			638 = {
				add_building_construction = {
					type = bunker
					level = 2
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
			set_temp_variable = { treasury_change = -4.35 }
			modify_treasury_effect = yes
		}
	}
	#
	IND_papua3 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_chrisitians
			has_country_flag = CHR3
		}
		icon = generic_army_support
		fire_only_once = yes
		days_remove = 35
		cost = 50
		modifier = {
			training_time_factor = -0.3
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_papua3"
			638 = { remove_dynamic_modifier = { modifier = IND_papua_resist3 } }
			638 = { add_dynamic_modifier = { modifier = IND_papua_resist4 } }
			clr_country_flag = CHR3
			set_country_flag = CHR4
		}
	}
	#
	IND_papua4 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_back_chrisitians
			has_country_flag = CHR4
		}
		icon = generic_political_discourse
		fire_only_once = yes
		days_remove = 35
		cost = 50
		modifier = {
			training_time_factor = -0.3
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_papua4"
			638 = { remove_dynamic_modifier = { modifier = IND_papua_resist4 } }
			clr_country_flag = CHR4
			set_country_flag = CHR5
		}
	}
}
#Favor No  One
IND_back_noone = {
	IND_neutral = {
		allowed = {
			tag = IND
		}
		icon = ger_mefo_bills
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
		}
		fire_only_once = yes
		days_remove = 35
		cost = 50
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral"
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.05 }
			set_temp_variable = { temp_outlook_increase = 0.05 }
			add_relative_party_popularity = yes
			set_temp_variable = { treasury_change = -2.48 }
			modify_treasury_effect = yes
			set_country_flag = NEU2
		}
	}
	#
	IND_neutral2 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
			has_country_flag = NEU2
		}
		icon = generic_nationalism
		fire_only_once = yes
		days_remove = 35
		cost = 50
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral2"
			639 = { remove_dynamic_modifier = { modifier = IND_islamic_resist } }
			639 = { add_dynamic_modifier = { modifier = IND_islamic_resist2 } }
			clr_country_flag = NEU2
			set_country_flag = NEU3
		}
	}
	IND_neutral_generic = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
		}
		icon = generic_prepare_civil_war
		fire_only_once = yes
		cost = 25
		days_remove = 50
		modifier = {
		local_manpower = 0.10
			training_time_factor = -0.15
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral_generic"
			add_stability = -0.01
			639 = {
				add_extra_state_shared_building_slots = -1
			}
			638 = {
				add_extra_state_shared_building_slots = -1
			}
		}
	}
	#
	IND_neutral_generic2 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
		}
		icon = generic_industry
		fire_only_once = yes
		cost = 25
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral_generic2"
			set_temp_variable = { treasury_change = -3.65 }
			modify_treasury_effect = yes
			IND = {
				add_equipment_to_stockpile = {
					type = Inf_equipment
					amount = -300
				}
			}
			638 = {
				add_building_construction = {
					type = naval_base
					level = 2
					instant_build = yes
				}
			}
		}
	}
	#
	IND_neutral_generic3 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 50
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral_generic3"
			set_temp_variable = { treasury_change = -3.65 }
			modify_treasury_effect = yes
			638 = {
				add_building_construction = {
					type = bunker
					level = 2
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
		}
	}
	#
	IND_neutral3 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
			has_country_flag = NEU3
		}
		icon = generic_army_support
		fire_only_once = yes
		days_remove = 35
		cost = 50
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral3"
			638 = { remove_dynamic_modifier = { modifier = IND_papua_resist } }
			638 = { add_dynamic_modifier = { modifier = IND_papua_resist2 } }
			clr_country_flag = NEU3
			set_country_flag = NEU4
		}
	}
	#
	IND_neutral4 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
			has_country_flag = NEU4
		}
		icon = generic_political_discourse
		fire_only_once = yes
		days_remove = 35
		cost = 50
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral4"
			638 = { remove_dynamic_modifier = { modifier = IND_papua_resist2 } }
			638 = { add_dynamic_modifier = { modifier = IND_papua_resist3 } }
			clr_country_flag = NEU4
			set_country_flag = NEU5
		}
	}
	#
	IND_neutral5 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_maluku_just_fix_it
			has_country_flag = NEU5
		}
		icon = generic_political_discourse
		fire_only_once = yes
		days_remove = 35
		cost = 50
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_neutral5"
			639 = { remove_dynamic_modifier = { modifier = IND_islamic_resist2 } }
			639 = { add_dynamic_modifier = { modifier = IND_islamic_resist3 } }
			clr_country_flag = NEU5
			set_country_flag = NEU6
		}
	}
}
##Help Aceh Decisions
IND_help_aceh = {
	IND_aceh = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_send_aid_package
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 50
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_aceh"
			set_temp_variable = { treasury_change = -5.65 }
			modify_treasury_effect = yes
			622 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
		}
	}
	#
	IND_aceh2 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_send_aid_package
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 50
		days_remove = 70
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_aceh2"
			set_temp_variable = { treasury_change = -7.65 }
			modify_treasury_effect = yes
			622 = {
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
		}
	}
	#
	IND_aceh3 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_send_aid_package
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 50
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_aceh3"
			add_stability = 0.03
			add_manpower = -5000
		}
	}
	#
	IND_aceh4 = {

		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_send_aid_package
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 50
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_aceh4"
			add_popularity = {
				ideology = fascism
				popularity = -0.03
			}
			add_manpower = -2000
		}
	}
	IND_aceh5 = {
		allowed = {
			tag = IND
		}
		visible = {
			has_completed_focus = IND_send_aid_package
		}
		icon = generic_civil_support
		fire_only_once = yes
		cost = 50
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove IND_aceh5"
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.05 }
			add_relative_party_popularity = yes
			set_temp_variable = { treasury_change = -2.75 }
			modify_treasury_effect = yes
		}
	}
}

