leader_traits = {

	the_great_mediator = {
		random = no
		political_power_factor = 0.05
		stability_factor = 0.05
		ai_focus_defense_factor = 0.2

		ai_will_do = {
			factor = 1
		}
	}

	beacon_of_islam = {
		random = no
		stability_factor = 0.10
		neutrality_drift = 0.05
		fascism_drift = 0.025

		ai_will_do = {
			factor = 1
		}
	}

	national_chief = {
		random = no
		political_power_gain = 0.10
		drift_defence_factor = 0.10

		ai_will_do = {
			factor = 1
		}

	}

	doesnt_understand_economy = {
		random = no
		production_speed_buildings_factor = -0.25
		ai_will_do = {
			factor = 1
		}
	}

	defender_of_rights = {
		random = no
		democratic_drift = 0.10
		party_popularity_stability_factor = 0.05
		ai_will_do = {
			factor = 1
		}
	}
}