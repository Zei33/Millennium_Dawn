﻿NOR = {
	air_wing_names_template = AIR_WING_NAME_NOR_FALLBACK

	#Air wings can only be named through archetype
	small_plane_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	small_plane_strike_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	small_plane_sucide_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	small_plane_cas_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_small_plane_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_small_plane_strike_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_small_plane_suicide_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	medium_plane_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
			"132 Luftving"
		}
	}

	medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
			"F 7 Såtenäs" "F 17 Kallinge" "F 21 Luleå"
		}
	}

	medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}
	medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	medium_plane_suicide_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_medium_plane_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_medium_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	cv_medium_plane_scout_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	large_plane_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	large_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
			"133 Luftving"
		}
	}

	large_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
			"135 Luftving"
		}
	}

	large_plane_cas_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}

	large_plane_awacs_airframe = {
		prefix = ""
		generic = { "Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}


	#############################
	attack_helicopter_hull = {
		prefix = ""
		generic = { "Helikopterskvadron" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}
	Air_UAV_equipment = {
		prefix = ""
		generic = { "UAV-Luftving" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
		}
	}
	guided_missile_equipment = {
		prefix = ""
		generic = { "Luftvernartilleribataljon" }
		generic_pattern = AIR_WING_NAME_NOR_GENERIC
		unique = {
			"Luftvernartilleribataljon Ørland"
		}
	}
}
