##### Iceland NAME LISTS #####

### REGULAR DESTROYER NAMES###
ICE_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { ICE }

	type = ship
	ship_types = { stealth_destroyer destroyer }

	prefix = "INV "
	fallback_name = "DDG-%d"

	unique = {
		"Hraunfossar" "Dettifoss" "Goðafoss" "Hafragilsfoss" "Seljalandsfoss" "Skógafoss" "Gullfoss" "Hengifoss" "Faxi" "Godafoss" "Hlauptungufoss" "Kirkjufellsfoss" "Morsárfoss" "Svartifoss" "Dynjandi" "Háifoss" "Ófærufoss" "Svöðufoss" "Hvítserkur" "Glymur" "Álftafjörður" "Bolungarvík" "Eskifjörður"
	}
}

ICE_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { ICE }
	type = ship
	ship_types = {
		frigate
	}
	prefix = "INV "
	fallback_name = "FFG-%d"

	unique = {
		"Hafnarfjörður" "Höfn" "Ísafjörður" "Keflavík" "Neskaupstaður" "Reykjanesbær" "Reykjavík" "Akureyri" "Borgarnes" "Egilsstaðir" "Garður" "Húsavík" "Kópavogur" "Mosfellsbær" "Patreksfjörður" "Seyðisfjörður" "Vestmannaeyjar" "Bláa Lónið" "Dimmuborgir" "Eldborg" "Fjallsárlón" "Geysir" "Grjótagjá" "Hveravellir" "Jökulsárlón" "Mývatn" "Reykjadalur" "Skaftafell" "Þingvellir" "Ásbyrgi" "Djúpalónssandur" "Dyrhólaey" "Fjaðrárgljúfur" "Kirkjufell" "Látrabjarg" "Vatnajökull" "Hekla" "Kerlingarfjöll" "Snæfellsjökull" "Mýrdalsjökull" "Langjökull" "Drangajökull" "Krýsuvík" "Hverir" "Grænavatn" "Viti"
	}
}

### CORVETTE NAMES ###
ICE_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { ICE }
	prefix = "INV "
	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Djúpavatn" "Kleifarvatn" "Þingvallavatn" "Heimaey" "Eldfell" "Fjörgyn" "Álfheimur" "Svartálfheimur" "Ásgarður" "Vanaheimur" "Muspellheimur" "Helheimur" "Niflheimur" "Jötunheimur" "Midgardur" "Gnipahellir" "Fafnir" "Grendel" "Hati" "Fenrir" "Jormungandr" "Yggdrasil" "Hlidskjalf" "Mímir" "Bifröst" "Mjölnir" "Gungnir" "Dainsleif" "Gramr" "Tyrfingur" "Hofud" "Hrotti" "Lævateinn" "Skofnungur" "Víga-Hrappr" "Ask" "Embla" "Baldur" "Heimdallur" "Freyja" "Freyr" "Sif" "Sleipnir" "Huginn" "Muninn" "Geri" "Freki" "Fenrisúlfur" "Hábrók" "Hræsvelgr" "Níðhöggr" "Fjölnir" "Gullfaxi" "Grani" "Glæsisvellir" "Hildisvíni" "Gríðarvölr" "Gjallarhorn" "Skíðblaðnir" "Naglfar" "Fenrisúlfr" "Dáinsleif" "Þór" "Odinn" "Frigg" "Týr" "Baldr" "Loki" "Sif" "Freyr" "Freya" "Heimdall" "Mímir" "Víðarr" "Váli" "Sigyn" "Hel" "Fenrir" "Jörmungandr" "Níðhöggr" "Huginn" "Muninn" "Gersemi" "Hnoss" "Nanna" "Bragi" "Víði" "Váli" "Forseti" "Sól" "Máni" "Dagr" "Gróa" "Sjöfn" "Lofn" "Syn" "Hlín" "Snotra" "Var" "Vör" "Hildr" "Gefjun" "Sól" "Máni" "Baldr" "Týr" "Heimdallur" "Freyja" "Freyr" "Hel" "Loki" "Odinn"
	}
}

ICE_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { ICE }
	prefix = "INV "
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	fallback_name = "SS-%d"
}