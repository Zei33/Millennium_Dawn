#Written by Hiddengearz, Balanced by Bird, re-balanced by dc
###Reworked as of 11/27/2020 for next update - adding in stealth factors and survivability - lowering damage output
equipments = {
	#Strike Fighter
	Strike_fighter_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { fighter interceptor tactical_bomber }
		allowed_types = {
			interceptor
			tactical_bomber
		}
		allow_mission_type = {
			air_superiority
			cas
			interception
			strategic_bomber
			naval_bomber
			port_strike
			attack_logistics
			recon
			training
		}
		group_by = archetype
		sprite = strike_fighter
		air_map_icon_frame = 2
		interface_category = interface_category_air


		# Fighter
		interface_overview_category_index = 4

		upgrades = {
			strike_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 7
		reliability = 0.875

		# Air vs Navy - high damage / low hit chance / hard to hurt
		naval_strike_attack = 12 #vanilla advanced tac bomber
		naval_strike_targetting = 7

		#Space taken in convoy
		lend_lease_cost = 8

		resources = {
			aluminium = 3
			tungsten = 2
			rubber = 1
		}
		fuel_consumption = 8
		manpower = 30
	}
#
	#Light Strike/Trainers
	L_Strike_fighter_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { fighter interceptor cas tactical_bomber }
		group_by = archetype
		sprite = fighter
		air_map_icon_frame = 1
		interface_category = interface_category_air
		allowed_types = {
			fighter
			interceptor
			tactical_bomber
			cas
		}
		allow_mission_type = {
			air_superiority
			cas
			interception
			strategic_bomber
			naval_bomber
			port_strike
			attack_logistics
			recon
			training
		}

		# Fighter
		interface_overview_category_index = 1

		upgrades = {
			strike_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 6
		reliability = 0.8875

		# Air vs Navy - Medium damage / low hit chance / hard to hurt
		naval_strike_attack = 9.5
		naval_strike_targetting = 8

		#Space taken in convoy
		lend_lease_cost = 8

		resources = {
			aluminium = 2
			rubber = 1
			tungsten = 1
		}
		fuel_consumption = 5.25
		manpower = 15
	}
	CV_L_Strike_fighter_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { fighter interceptor cas tactical_bomber }
		group_by = archetype
		sprite = fighter
		air_map_icon_frame = 1
		interface_category = interface_category_air
		allowed_types = {
			fighter
			interceptor
			tactical_bomber
			cas
		}
		allow_mission_type = {
			air_superiority
			cas
			interception
			strategic_bomber
			naval_bomber
			port_strike
			attack_logistics
			recon
			training
		}

		# Fighter
		interface_overview_category_index = 1

		upgrades = {
			strike_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 6
		reliability = 0.8875

		# Air vs Navy - Medium damage / low hit chance / hard to hurt
		naval_strike_attack = 9.5
		naval_strike_targetting = 8

		#Space taken in convoy
		lend_lease_cost = 8

		resources = {
			aluminium = 2
			rubber = 1
			tungsten = 1
		}
		fuel_consumption = 5.25
		manpower = 15
	}
	AS_Fighter_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { fighter interceptor cas tactical_bomber }
		group_by = archetype
		sprite = mr_fighter
		air_map_icon_frame = 6
		interface_category = interface_category_air
		allowed_types = {
			fighter
			interceptor
			cas
			tactical_bomber
		}
		allow_mission_type = {
			air_superiority
			cas
			interception
			strategic_bomber
			naval_bomber
			port_strike
			attack_logistics
			recon
			training
		}

		# Fighter
		interface_overview_category_index = 1

		upgrades = {
			strike_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 9
		reliability = 0.89

		# Air vs Navy - high damage / high hit chance / easy to hurt
		naval_strike_attack = 13.75
		naval_strike_targetting = 8.25

		#Space taken in convoy
		lend_lease_cost = 8

		resources = {
			aluminium = 1
			tungsten = 1
			rubber = 1
		}
		fuel_consumption = 7
		manpower = 25
	}
	#Multi Role Fighter
	MR_Fighter_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { fighter interceptor cas tactical_bomber }
		group_by = archetype
		sprite = mr_fighter
		air_map_icon_frame = 6
		interface_category = interface_category_air
		allowed_types = {
			fighter
			interceptor
			cas
			tactical_bomber
		}
		allow_mission_type = {
			air_superiority
			cas
			interception
			strategic_bomber
			naval_bomber
			port_strike
			attack_logistics
			recon
			training
		}

		# Fighter
		interface_overview_category_index = 1

		upgrades = {
			strike_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 9
		reliability = 0.89

		# Air vs Navy - high damage / high hit chance / easy to hurt
		naval_strike_attack = 13.75
		naval_strike_targetting = 8.25

		#Space taken in convoy
		lend_lease_cost = 8

		resources = {
			aluminium = 1
			tungsten = 1
			rubber = 1
		}
		fuel_consumption = 7
		manpower = 25
	}
	#Carrier Multi Role Equipment
	CV_MR_Fighter_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { fighter interceptor cas tactical_bomber }
		group_by = archetype
		sprite = cv_multirole
		carrier_capable = yes
		air_map_icon_frame = 4
		interface_category = interface_category_air
		default_carrier_composition_weight = 10
		allowed_types = {
			fighter
			interceptor
			cas
			tactical_bomber
		}
		allow_mission_type = {
			cas
			strategic_bomber
			naval_bomber
			port_strike
			attack_logistics
			recon
			training
		}

		# Fighter
		interface_overview_category_index = 1

		upgrades = {
			strike_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 9
		reliability = 0.88

		# Air vs Ground
		air_ground_attack = 8

		# Air vs Navy - high damage / high hit chance / easy to hurt
		naval_strike_attack = 13.75
		naval_strike_targetting = 8.5

		#Space taken in convoy
		lend_lease_cost = 8

		build_cost_ic = 220
		resources = {
			aluminium = 1
			tungsten = 1
			rubber = 1
		}
		fuel_consumption = 7.5
		manpower = 35
	}
	#UCAV
	Air_UAV_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { scout_plane tactical_bomber suicide }
		group_by = archetype
		sprite = uav
		allowed_types = {
			scout_plane
			tactical_bomber
			suicide
		}
		allow_mission_type = {
			cas
			strategic_bomber
			naval_bomber
			port_strike
			attack_logistics
			recon
			training
			naval_kamikaze
		}

		air_map_icon_frame = 13
		interface_category = interface_category_air

		# Fighter
		interface_overview_category_index = 1

		upgrades = {
			strike_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 3
		reliability = 0.875

		# Air vs Ground
		air_ground_attack = 10

		# Air vs Navy - high damage / high hit chance / easy to hurt
		naval_strike_attack = 10.25
		naval_strike_targetting = 9.75

		#Space taken in convoy
		lend_lease_cost = 8

		build_cost_ic = 202
		resources = {
			aluminium = 1
			tungsten = 1
		}
		fuel_consumption = 0.2
		manpower = 8
	}
}