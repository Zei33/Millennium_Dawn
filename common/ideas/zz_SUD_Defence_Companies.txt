ideas = {

	materiel_manufacturer = {

		designer = yes

		SUD_military_industry_corporation_materiel_manufacturer = {
			allowed = { original_tag = SUD }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SUD_military_industry_corporation_materiel_manufacturer" }

			picture = Military_Industry_Corporation
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.031
			}

			traits = {
				CAT_inf_1

			}
			ai_will_do = {
				factor = 1
			}

		}
	}

	tank_manufacturer = {

		designer = yes

		SUD_military_industry_corporation_tank_manufacturer = {
			allowed = { original_tag = SUD }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SUD_military_industry_corporation_tank_manufacturer" }

			picture = Military_Industry_Corporation
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_armor = 0.031
			}

			traits = {
				CAT_armor_1

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

}
