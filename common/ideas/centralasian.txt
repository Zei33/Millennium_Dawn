ideas = {
	country = {
		#KAZAKH MIGR
		KAZ_high_emigration_5 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = kaz_migr

			modifier = {
				migration_rate_value_factor = -0.95
				research_speed_factor = -0.2
			}
		}

		KAZ_high_emigration_4 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = kaz_migr

			modifier = {
				migration_rate_value_factor = -0.80
				research_speed_factor = -0.18
			}
		}

		KAZ_high_emigration_3 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = kaz_migr

			modifier = {
				migration_rate_value_factor = -0.70
				research_speed_factor = -0.15
			}
		}

		KAZ_high_emigration_2 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = kaz_migr

			modifier = {
				migration_rate_value_factor = -0.50
				research_speed_factor = -0.12
			}
		}

		KAZ_high_emigration_1 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = kaz_migr

			modifier = {
				migration_rate_value_factor = -0.35
				research_speed_factor = -0.07
			}
		}

		KAZ_immigration = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = prc_the_long_march

			modifier = {
				migration_rate_value_factor = 0.20
				research_speed_factor = 0.05
			}
		}

		#UZBEK MIGR
		UZB_high_emigration_5 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = uzb_migr
			modifier = {
				migration_rate_value_factor = -0.95
				research_speed_factor = -0.2

			}
		}
		UZB_high_emigration_4 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = uzb_migr
			modifier = {
				migration_rate_value_factor = -0.80
				research_speed_factor = -0.18
			}
		}

		UZB_high_emigration_3 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = uzb_migr

			modifier = {
				migration_rate_value_factor = -0.70
				research_speed_factor = -0.15
			}
		}

		UZB_high_emigration_2 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = uzb_migr

			modifier = {
				migration_rate_value_factor = -0.50
				research_speed_factor = -0.12
			}
		}

		UZB_high_emigration_1 = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = uzb_migr

			modifier = {
				migration_rate_value_factor = -0.35
				research_speed_factor = -0.07
			}
		}

		UZB_immigration = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = prc_the_long_march

			modifier = {
				migration_rate_value_factor = 0.20
				research_speed_factor = 0.05
			}
		}

		#KYRGYZ MIGR
		KYR_high_emigration_5 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = kyr_migr
			modifier = {
				migration_rate_value_factor = -0.95
				research_speed_factor = -0.2

			}
		}
		KYR_high_emigration_4 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = kyr_migr
			modifier = {
				migration_rate_value_factor = -0.80
				research_speed_factor = -0.18
			}
		}
		KYR_high_emigration_3 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = kyr_migr
			modifier = {
				migration_rate_value_factor = -0.70
				research_speed_factor = -0.15
			}
		}
		KYR_high_emigration_2 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = kyr_migr
			modifier = {
				migration_rate_value_factor = -0.50
				research_speed_factor = -0.12
			}
		}
		KYR_high_emigration_1 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = kyr_migr
			modifier = {
				migration_rate_value_factor = -0.35
				research_speed_factor = -0.07
			}
		}
		KYR_immigration = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = kyr_migr
			modifier = {
				migration_rate_value_factor = 0.20
				research_speed_factor = 0.05
			}
		}

		KAZ_narcotraffic_1 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.05
				consumer_goods_factor = 0.02
				corruption_cost_factor = 0.2
			}
		}

		UZB_narcotraffic_1 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.05
				consumer_goods_factor = 0.02
				corruption_cost_factor = 0.2
			}
		}

		KYR_narcotraffic_1 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.05
				consumer_goods_factor = 0.02
				corruption_cost_factor = 0.2
			}
		}

		TAJ_narcotraffic_1 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.05
				consumer_goods_factor = 0.02
				corruption_cost_factor = 0.2
			}
		}

		KAZ_narcotraffic_2 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.045
				consumer_goods_factor = 0.017
				corruption_cost_factor = 0.17
			}
		}

		UZB_narcotraffic_2 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.045
				consumer_goods_factor = 0.017
				corruption_cost_factor = 0.17
			}
		}

		KYR_narcotraffic_2 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.045
				consumer_goods_factor = 0.017
				corruption_cost_factor = 0.17
			}
		}

		TAJ_narcotraffic_2 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.045
				consumer_goods_factor = 0.017
				corruption_cost_factor = 0.17
			}
		}

		KAZ_narcotraffic_3 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.04
				consumer_goods_factor = 0.015
				corruption_cost_factor = 0.15
			}
		}

		UZB_narcotraffic_3 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.04
				consumer_goods_factor = 0.015
				corruption_cost_factor = 0.15
			}
		}

		KYR_narcotraffic_3 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.04
				consumer_goods_factor = 0.015
				corruption_cost_factor = 0.15
			}
		}

		TAJ_narcotraffic_3 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = opium
			modifier = {
				stability_factor = -0.04
				consumer_goods_factor = 0.015
				corruption_cost_factor = 0.15
			}
		}


		####OTHER
		UZB_regionalism_karalp = {

			allowed = {
				UZB = { owns_state = 727 }
			}

			allowed_civil_war = {
				always = no
			}

			picture = ITA_hardened_irregular_bands
			modifier = {
				stability_factor = -0.1
			}
		}

		KAZ_elbasy_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KAZ_elbasy_idea" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = kolbasy
			modifier = {
				stability_factor = 0.10
				drift_defence_factor = 0.05
			}
		}

		tajik_kyrgyz_Tensions_kyr = {
			picture = sectarian_troubles_fundamentalism
			modifier = {
				defensive_war_stability_factor = 0.5
			}

			targeted_modifier = {
				tag = TAJ
				defense_bonus_against = 0.1
			}
		}
		tajik_kyrgyz_Tensions_taj = {
			picture = sectarian_troubles_fundamentalism
			modifier = {
				defensive_war_stability_factor = 0.5
			}

			targeted_modifier = {
				tag = KYR
				defense_bonus_against = 0.1
			}
		}


		# Tajikistan Migration
		TAJ_high_emigration_5 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = taj_migr
			modifier = {
				migration_rate_value_factor = -0.95
				research_speed_factor = -0.2

			}
		}
		TAJ_high_emigration_4 = {

			allowed = { always = no }

			allowed_civil_war = {
				always = no
			}

			picture = taj_migr
			modifier = {
				migration_rate_value_factor = -0.80
				research_speed_factor = -0.18
			}
		}
		TAJ_high_emigration_3 = {

			allowed = { always = no }
			allowed_civil_war = {
				always = no
			}

			picture = taj_migr
			modifier = {
				migration_rate_value_factor = -0.70
				research_speed_factor = -0.15
			}
		}
		TAJ_high_emigration_2 = {

			allowed = { always = no }
			allowed_civil_war = {
				always = no
			}

			picture = taj_migr
			modifier = {
				migration_rate_value_factor = -0.50
				research_speed_factor = -0.12
			}
		}
		TAJ_high_emigration_1 = {

			allowed = { always = no }
			allowed_civil_war = {
				always = no
			}

			picture = taj_migr
			modifier = {
				migration_rate_value_factor = -0.35
				research_speed_factor = -0.07
			}
		}
		TAJ_immigration = {

			allowed = { always = no }
			allowed_civil_war = {
				always = no
			}

			picture = prc_the_long_march
			modifier = {
				migration_rate_value_factor = 0.20
				research_speed_factor = 0.05
			}
		}

		CES_narcotraffic_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_narcotraffic_idea" }
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = opium

			modifier = {
				stability_factor = -0.05
				consumer_goods_factor = 0.02
				corruption_cost_factor = 0.2
			}
		}

		CES_controlled_narcotraffic_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_controlled_narcotraffic_idea" }
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = opium

			modifier = {
				stability_factor = -0.03
				consumer_goods_factor = 0.01
				corruption_cost_factor = 0.1
			}
		}

		ces_post_soviet_legacy_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ces_post_soviet_legacy_idea" }
			allowed = { always = no }
			cancel = {
				NOT = {
					has_government = communism
					has_government = neutrality
				}
			}
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = democratic_socialism

			modifier = {
				communism_drift = 0.1
				communism_acceptance = 25
			}
		}
		ces_neutral_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ces_post_soviet_legacy_idea" }
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = progressivism

			modifier = {
				neutrality_drift = 0.1
				neutrality_acceptance = 25
				communism_acceptance = 25
			}
		}

		CES_broken_economy_idea_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_broken_economy_idea_1" }

			picture = hyper_inflation

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				production_speed_buildings_factor = -0.15
				industrial_capacity_factory = -0.20
				consumer_goods_factor = 0.10
				corruption_cost_factor = 0.35
			}
		}
		CES_broken_economy_idea_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_broken_economy_idea_2" }

			picture = hyper_inflation

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				production_speed_buildings_factor = -0.10
				industrial_capacity_factory = -0.10
				consumer_goods_factor = 0.03
				corruption_cost_factor = 0.25
			}
		}
		CES_broken_economy_idea_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_broken_economy_idea_3" }

			picture = hyper_inflation

			allowed = { always = no }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_buildings_factor = -0.05
				industrial_capacity_factory = -0.05
				consumer_goods_factor = 0.01
				corruption_cost_factor = 0.15
			}
		}

		CES_outdated_army_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_outdated_army_idea" }
			picture = Disorganization_Military_2
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_attack_factor = -0.20
				army_defence_factor = -0.20
				army_org_factor = -0.15
				personnel_cost_multiplier_modifier = 0.07
			}
		}

		CES_outdated_army_idea_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_outdated_army_idea_1" }
			picture = Disorganization_Military_3
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_attack_factor = -0.15
				army_defence_factor = -0.15
				army_org_factor = -0.10
				personnel_cost_multiplier_modifier = 0.03
			}
		}

		CES_outdated_army_idea_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_outdated_army_idea_2" }
			picture = Disorganization_Military_4
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_attack_factor = -0.10
				army_defence_factor = -0.10
				army_org_factor = -0.05
			}
		}

		CES_outdated_army_idea_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_outdated_army_idea_3" }
			picture = Disorganization_Military_5
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_attack_factor = -0.05
				army_defence_factor = -0.05
			}
		}

		CES_trade_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_trade_idea" }
			allowed = { always = no }

			removal_cost = -1
			picture = trade

			modifier = {
				min_export = 0.15
			}
		}

		CES_industry_focus_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_industry_focus_idea" }

			allowed = {
			}

			allowed_civil_war = {
				always = yes
			}

			picture = industrial_focus

			modifier = {
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.10
			}
		}
		CES_education_focus_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_military_focus_idea" }

			allowed = {
			}

			allowed_civil_war = {
				always = yes
			}

			picture = political_support

			modifier = {
				research_speed_factor = 0.05
				education_cost_multiplier_modifier = -0.05
			}
		}
		CES_social_focus_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_science_focus_idea" }

			allowed = {
			}

			allowed_civil_war = {
				always = yes
			}

			picture = research_bonus

			modifier = {
				health_cost_multiplier_modifier = -0.05
				social_cost_multiplier_modifier = -0.03
			}
		}

		CES_switzerland_investments_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_science_focus_idea" }

			allowed = {
			}

			allowed_civil_war = {
				always = yes
			}

			picture = economic_increase

			modifier = {
				production_speed_buildings_factor = 0.15
				consumer_goods_factor = -0.05
				min_export = 0.1
			}
		}

		CES_develop_turism = {
			allowed = { always = no }
			allowed_civil_war = {
				always = no
			}

			picture = communism11
			on_add = {
				add_to_variable = { var = ROOT_tax_multiplier_var value = 0.05 }
			}
			on_remove = {
				add_to_variable = { var = ROOT_tax_multiplier_var value = -0.05 }
			}
			modifier = {
				production_speed_buildings_factor = 0.15
			}
		}

		CES_agriculture_rebuilt_idea7 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_agriculture_rebuilt_idea7" }

			picture = hyper_inflation

			allowed = { always = no }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_buildings_factor = 0.1
				consumer_goods_factor = 0.05
				social_cost_multiplier_modifier = 0.07
				min_export = 0.05
			}
		}

		CES_central_asian_union_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_central_asian_union_idea" }

			picture = central_asian_union_idea

			allowed = { always = no }
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = 0.05
				war_support_factor = -0.05
				stability_factor = 0.01
				local_resources_factor = 0.10
			}
		}

		CES_raise_from_ash_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_raise_from_ash_idea" }

			picture = anti_soviet_pact

			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }
			modifier = {
				stability_weekly = 0.001
				political_power_factor = 0.05
				communism_drift = -0.01
			}
		}

		CES_partnership_with_unesco_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_partnership_with_unesco_idea" }

			picture = partnership_with_unesco

			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }
			modifier = {
				research_speed_factor = 0.03
				education_cost_multiplier_modifier = -0.1
				political_power_factor = 0.05
			}
		}

		CES_peaceful_prowestern_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_peaceful_prowestern_idea" }

			picture = democracy

			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }
			modifier = {
				democratic_drift = 0.2
			}
		}

		CES_changing_the_course_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_changing_the_course_idea" }

			picture = neutrality

			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }
			modifier = {
				neutrality_drift = 0.2
			}
		}

		CES_peaceful_eastern_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_peaceful_eastern_idea" }

			picture = emerging

			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }
			modifier = {
				communism_drift = 0.2
			}
		}

		CES_border_reform_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_border_reform_idea" }

			picture = ces_border_reform

			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_bunker_factor = 0.05
				monthly_population = 0.05
				stability_factor = 0.03
			}
		}

		CES_western_opposition_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_western_opposition_idea" }
			allowed = { always = no }
			cancel = {
				has_government = fascism
				has_government = democratic
			}
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = democracy

			modifier = {
				stability_factor = -0.05
				democratic_drift = 0.07
				corruption_cost_factor = 0.1
			}
		}
		CES_political_instability_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_political_instability_idea" }
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = opium

			modifier = {
				drift_defence_factor = -0.5
				stability_factor = -0.05
				corruption_cost_factor = 0.1
			}
		}
		CES_political_instability_less_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_political_instability_less_idea" }
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = opium

			modifier = {
				drift_defence_factor = -0.3
				stability_factor = -0.03
				corruption_cost_factor = 0.05
			}
		}
		CES_family_in_politics = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_family_in_politics" }
			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = chained_monarchism

			modifier = {
				political_power_factor = 0.1
				production_speed_buildings_factor = -0.06
				global_building_slots_factor = -0.06
				drift_defence_factor = 0.3
				corruption_cost_factor = 0.3
			}
		}
		CES_independent_politics = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_independent_politics" }
			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = conservatism

			modifier = {
				political_power_factor = -0.1
				production_speed_buildings_factor = 0.05
				global_building_slots_factor = 0.05
				drift_defence_factor = -0.3
				corruption_cost_factor = -0.3
			}
		}
		CES_turkic_council = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_turkic_council" }
			allowed = { always = no }
			cancel = {
				has_government = fascism
			}
			allowed_civil_war = { always = yes }

			removal_cost = -1
			picture = turkic_council_idea

			modifier = {
				political_power_factor = -0.05
				production_speed_buildings_factor = 0.1
				global_building_slots_factor = 0.05
				drift_defence_factor = -0.3
				corruption_cost_factor = -0.3
			}
		}
		CES_idea_eacu_cooperation = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_idea_eacu_cooperation" }
			picture = sov_eaes_idea
			modifier = {
				political_power_factor = 0.15
				war_support_factor = -0.02
				stability_factor = 0.02
				local_resources_factor = 0.10
			}
		}
		###EACO Integration
		CES_idea_eacu_cooperation_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_idea_eacu_cooperation_2" }
			picture = sov_eaes_idea
			modifier = {
				political_power_factor = 0.25
				war_support_factor = -0.05
				stability_factor = 0.05
				local_resources_factor = 0.20
			}
		}

		CES_idea_diplomatic_powerhouse = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CES_idea_diplomatic_powerhouse" }

			picture = flexible_foreign_policy2

			modifier = {
				trade_opinion_factor = 0.125
				justify_war_goal_time = 0.125
				send_volunteer_size = 2
			}
		}
	}
}