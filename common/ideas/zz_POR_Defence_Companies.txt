ideas = {
	materiel_manufacturer = {
		designer = yes
		POR_fabrica_de_braco_de_prata_materiel_manufacturer = {
			allowed = { original_tag = POR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POR_fabrica_de_braco_de_prata_materiel_manufacturer" }
			picture = Fabrica_De_Braco_De_Prata_POR
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf = 0.155
			}

			traits = { CAT_inf_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		POR_bravia_sarl_tank_manufacturer = {
			allowed = { original_tag = POR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POR_bravia_sarl_tank_manufacturer" }
			picture = Bravia_Sarl_POR
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.155
			}

			traits = { CAT_afv_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
