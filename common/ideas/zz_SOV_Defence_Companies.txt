ideas = {
	tank_manufacturer = {
		designer = yes
		SOV_military_industry_company_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_military_industry_company_tank_manufacturer" }
			picture = Military_Industry_Company_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_artillery = 0.155
			}

			traits = {
				CAT_artillery_5

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_rostec_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_tank_manufacturer" }
			picture = Rostec_SOV
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.248
			}
			traits = {
				CAT_armor_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_kurganmashzavod_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_kurganmashzavod_tank_manufacturer" }
			picture = Kurganmashzavod_SOV
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.217
			}

			traits = {
				CAT_afv_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		SOV_rostec_materiel_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_materiel_manufacturer" }
			picture = Rostec_SOV
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf = 0.248
			}

			traits = {
				CAT_inf_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_almaz_antey_materiel_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_almaz_antey_materiel_manufacturer" }

			picture = Almaz_Antey_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.248
			}

			traits = {
				CAT_inf_wep_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_jsc_defense_systems_materiel_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_jsc_defense_systems_materiel_manufacturer" }
			picture = JSC_Defense_systems_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_aa = 0.186
			}

			traits = {
				CAT_aa_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes
		SOV_russian_helicopters_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_russian_helicopters_tank_manufacturer" }
			picture = Russian_Helicopters_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.186
			}

			traits = {
				CAT_heli_6

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_rostec_tank_manufacturer2 = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_tank_manufacturer" }

			picture = Rostec_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.248
			}

			traits = {
				CAT_heli_8

			}
			ai_will_do = {
				factor = 1
			}
		}

		SOV_mil_helicopters_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_mil_helicopters_tank_manufacturer" }

			picture = Mil_Helicopters_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.248
			}

			traits = {
				CAT_heli_8

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		SOV_united_aircraft_corporation_aircraft_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_aircraft_corporation_aircraft_manufacturer" }

			picture = United_Aircraft_Corporation_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.248
			}

			traits = {
				CAT_fixed_wing_8

			}
			ai_will_do = {
				factor = 1
			}
		}

		SOV_irkut_aircraft_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_irkut_aircraft_manufacturer" }

			picture = Irkut_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fighter = 0.216
			}

			traits = {
				CAT_fighter_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}


	naval_manufacturer = {

		designer = yes

		SOV_united_shipbuilding_corporation_naval_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_shipbuilding_corporation_naval_manufacturer" }

			picture = United_Shipbuilding_Corporation_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.186
			}

			traits = {
				CAT_naval_eqp_6

			}
			ai_will_do = {
				factor = 1
			}
		}

		SOV_united_shipbuilding_corporation_naval_manufacturer2 = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_shipbuilding_corporation_naval_manufacturer" }

			picture = United_Shipbuilding_Corporation_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_sub = 0.186
			}

			traits = {
				CAT_sub_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}