ideas = {

	materiel_manufacturer = {
		designer = yes
		CAN_diemaco_materiel_manufacturer = {
			allowed = { original_tag = CAN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CAN_diemaco_materiel_manufacturer" }
			picture = Diemaco_CAN
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.217
			}

			traits = { CAT_inf_wep_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		CAN_general_dynamics_land_systems_tank_manufacturer = {
			allowed = { original_tag = CAN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CAN_general_dynamics_land_systems_tank_manufacturer" }
			picture = General_Dynamics_Land_Systems_CAN
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.248
			}

			traits = { CAT_armor_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		CAN_marine_industries_limited_naval_manufacturer = {
			allowed = { original_tag = CAN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CAN_marine_industries_limited_naval_manufacturer" }

			picture = General_Dynamics_Land_Systems_CAN
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.186
			}

			traits = { CAT_naval_eqp_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

}
