ideas = {

	tank_manufacturer = {
		designer = yes
		BRA_avibras_tank_manufacturer = {
			allowed = { original_tag = BRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_avibras_tank_manufacturer" }
			picture = Avibras_BRA
			cost = 150

			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.217
			}

			traits = {
				CAT_armor_7

			}
			ai_will_do = {
				factor = 1
			}
		}
		BRA_helibras_tank_manufacturer = {
			allowed = { original_tag = BRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_helibras_tank_manufacturer" }

			picture = Helibras_BRA
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.186
			}

			traits = {
				CAT_heli_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		BRA_avibras_materiel_manufacturer = {
			allowed = { original_tag = BRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_avibras_materiel_manufacturer" }

			picture = Avibras_BRA
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_l_drone = 0.217
			}

			traits = {
				CAT_inf_wep_6
			}
			ai_will_do = {
				factor = 1
			}

		}
		BRA_imbel_materiel_manufacturer = {
			allowed = { original_tag = BRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_imbel_materiel_manufacturer" }

			picture = IMBEL_BRA
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.186
			}

			traits = {
				CAT_inf_wep_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		BRA_embraer_aircraft_manufacturer = {
			allowed = { original_tag = BRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_embraer_aircraft_manufacturer" }
			picture = Embraer_BRA
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.155
			}

			traits = {
				CAT_fixed_wing_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		BRA_arsenal_do_marinha_naval_manufacturer = {
			allowed = { original_tag = BRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_arsenal_do_marinha_naval_manufacturer" }

			picture = Arsenal_do_Marinha_BRA

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.124
			}

			traits = {
				CAT_naval_eqp_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

}
