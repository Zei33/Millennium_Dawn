#Made by Lord Bogdanoff
ideas = {
	country = {
		HPR_unproffesional_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_unproffesional_army " }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = bad_army_idea
			modifier = {
				army_defence_factor = -0.30
				army_attack_factor = -0.30
				training_time_army_factor = -0.1
			}
		}
		HPR_proffesional_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_proffesional_army" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.05
				army_attack_factor = 0.05
				training_time_army_factor = 0.09
			}
		}
		HPR_proffesional_army2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_proffesional_army2" }
			allowed = {	always = no	}
			name = HPR_proffesional_army
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.15
				army_attack_factor = 0.15
				training_time_army_factor = 0.2
			}
		}
		HPR_bivaluta = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HPR_bivaluta" }
			picture = dpr_bivaluta
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			modifier = {
				stability_factor = -0.25
				tax_gain_multiplier_modifier = -0.2
				economic_cycles_cost_factor = 0.5
			}
		}
		HPR_no_goverment_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HPR_no_goverment_idea" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_government_idea
			modifier = {
				political_power_gain = -0.05
				foreign_influence_defense_modifier = -0.15
			}
		}
		HPR_mgb = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_mgb" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_mgb_idea
			modifier = {
				encryption_factor = 0.5
				foreign_influence_defense_modifier = 0.10
			}
		}
		HPR_ter_orobona = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_ter_orobona" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_teroborona_idea
			modifier = {
				mobilization_speed = 0.04
				army_core_defence_factor = 0.03
				war_support_factor = 0.01
				training_time_army_factor = -0.02
			}
		}
		HPR_kubgaz_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_kubgaz_idea" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_kubgaz_idea
			modifier = {
				corporate_tax_income_multiplier_modifier = 0.15
				fossil_pp_fuel_consumption_modifier = -0.15
				local_resources_factor = 0.01
				oil_export_multiplier_modifier = 0.01
			}
		}
		HPR_forts_buildings = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HPR_forts_buildings" }
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				production_speed_bunker_factor = 0.2
			}
		}
		#######ECONOMIC#######
		HPR_avia_zavod = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_aviazavod" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_avia_idea
			equipment_bonus = {
				strategic_bomber_equipment = {
					build_cost_ic = -0.04
					instant = yes
				}
			}
		}
		HPR_bronetank = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_bronetank" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = army_tank_good
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.05
				}
			}
		}
		HPR_priborostroy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_priborostroy" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_pribostroy_idea
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.05
				}
				apc_hull = {
					build_cost_ic = -0.06
				}
			}
		}
		HPR_tractor = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_tractor" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_tractors_idea
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = -0.06
				}
			}
		}
		HPR_agregat = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_agregat" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_agregat_idea
			equipment_bonus = {
				strategic_bomber_equipment = {
					build_cost_ic = -0.06
					instant = yes
				}
			}
		}
		HPR_morozova = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_morozova" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_morozova
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.06
				}
				apc_hull = {
					build_cost_ic = -0.04
				}
			}
		}
		HPR_specmachine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_specmachine" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_specmachine_idea
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = -0.10
				}
			}
		}
		HPR_malichev = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_malichev" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_malicheva
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.09
				}
			}
		}
		HPR_concern = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_concern" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_concern_kharkiv_idea
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.25
				}
				util_vehicle_equipment = {
					build_cost_ic = -0.16
				}
				apc_hull = {
					build_cost_ic = -0.10
				}
				strategic_bomber_equipment = {
					build_cost_ic = -0.10
					instant = yes
				}
			}
		}
		HPR_concern_sov = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_concern_sov" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_concern_kharkiv_idea
			cancel = {
				NOT = { HPR = {	is_subject_of = SOV	} }
			}
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.15
				}
				util_vehicle_equipment = {
					build_cost_ic = -0.10
				}
				apc_hull = {
					build_cost_ic = -0.07
				}
				strategic_bomber_equipment = {
					build_cost_ic = -0.05
					instant = yes
				}
			}
		}
		HPR_concern_sov1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_concern_sov1" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_concern_kharkiv_idea
			name = HPR_concern_sov
			cancel = {
				NOT = { SOV = { owns_state = 696 } }
			}
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.15
				}
				util_vehicle_equipment = {
					build_cost_ic = -0.10
				}
				apc_hull = {
					build_cost_ic = -0.07
				}
				strategic_bomber_equipment = {
					build_cost_ic = -0.05
					instant = yes
				}
			}
		}
		HPR_concern_sov2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add HPR_concern_sov2" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = hpr_concern_kharkiv_idea
			name = HPR_concern_sov
			cancel = {
				NOT = { BLR = { owns_state = 696 } }
			}
			equipment_bonus = {
				mbt_hull = {
					build_cost_ic = -0.15
				}
				util_vehicle_equipment = {
					build_cost_ic = -0.10
				}
				apc_hull = {
					build_cost_ic = -0.07
				}
				strategic_bomber_equipment = {
					build_cost_ic = -0.05
					instant = yes
				}
			}
		}
	}
}