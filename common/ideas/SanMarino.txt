ideas = {
	country = {
		SMA_mafia = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_mafia" }
			picture = spy_political

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				political_power_gain = -0.5
				stability_factor = -0.15
				production_speed_buildings_factor = -0.1
			}
		}

		SMA_small_population = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_small_population" }
			picture = production_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			cancel = {
				265 = {
					OR = {
						is_owned_by = SMA
						any_neighbor_state = {
							NOT = { state = 948 }
							is_owned_by = SMA
						}
					}
				}
				81 = {
					OR = {
						is_owned_by = SMA
						any_neighbor_state = {
							NOT = { state = 929 }
							is_owned_by = SMA
						}
					}
				}
			}

			modifier = {
				production_speed_buildings_factor = -0.15
				civ_facs_worker_requirement_modifier = -0.65
				mil_facs_worker_requirement_modifier = -0.65
				offices_worker_requirement_modifier = -0.65
				resource_sector_workers_modifier = -0.4
			}
		}

		SMA_rights_of_italian_workers_in_san_marino = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_rights_of_italian_workers_in_san_marino" }
			picture = worker_reforms

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				conscription_factor = 0.02
				monthly_population = 0.05
				stability_factor = -0.05
			}
		}

		SMA_trust_in_people = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_trust_in_people" }
			picture = spy_coup

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				agency_upgrade_time = 0.35
				enemy_operative_capture_chance_factor = -0.2
			}
		}

		SMA_traditional_marriage = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_traditional_marriage" }
			picture = christian_idea

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				monthly_population = 0.05
				political_power_factor = 0.05
				stability_factor = 0.05
			}
		}

		SMA_chruch_withnout_autority = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_chruch_withnout_autority" }
			picture = christian_idea

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = -0.05
				consumer_goods_factor = -0.05
			}
		}

		SMA_gmos_banned = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_gmos_banned" }
			picture = agriculture

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				monthly_population = 0.05
			}
		}

		SMA_christian_state_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_christian_state_1" }
			allowed = { always = no }
			picture = christian_idea

			modifier = {
				stability_factor = 0.05
				monthly_population = 0.05
				research_speed_factor = -0.03
			}
		}

		SMA_christian_state_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_christian_state_2" }
			allowed = { always = no }
			picture = christian_idea

			modifier = {
				stability_factor = 0.1
				monthly_population = 0.08
				research_speed_factor = -0.05
			}
		}

		SMA_health_care = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_health_care" }
			picture = health_care

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				health_cost_multiplier_modifier = 0.1
				monthly_population = 0.01
				stability_factor = 0.03
				political_power_factor = 0.10
			}
		}

		SMA_increase_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_increase_army" }
			picture = infantry_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				Military_Spending_cost_factor = -0.05
				supply_combat_penalties_on_core_factor = -0.05
				out_of_supply_factor = -0.04
			}
		}

		SMA_increase_health_care = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_increase_health_care" }
			picture = health_care2

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				health_cost_multiplier_modifier = -0.1
				stability_factor = 0.03
				monthly_population = 0.01
			}
		}

		SMA_increase_police = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_increase_police" }
			picture = police_badge

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				police_cost_multiplier_modifier = -0.1
				stability_factor = 0.05
				political_power_factor = 0.05
			}
		}

		SMA_increase_education = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_increase_education" }
			picture = army_war_college

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				education_cost_multiplier_modifier = -0.1
				stability_factor = 0.01
				political_power_factor = 0.01
			}
		}

		SMA_increase_social_benefits = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_increase_social_benefits" }
			picture = consumer_goods

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				social_cost_multiplier_modifier = -0.1
				stability_factor = 0.02
				political_power_factor = 0.02
			}
		}

		SMA_pro_west = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_pro_west" }
			picture = democracy

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				emerging_outlook_campaign_cost_modifier = 0.03
				western_outlook_campaign_cost_modifier = -0.02
				democratic_drift = 0.01
				political_power_factor = 0.02
			}
		}

		###################################################################################################
		### ECONOMY ### ECONOMY ### ECONOMY ### ECONOMY ### ECONOMY ### ECONOMY ### ECONOMY ### ECONOMY ###
		###################################################################################################

		the_SMA_euro = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea the_SMA_euro" }
			picture = the_euro

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				economic_cycles_cost_factor = 0.2
				trade_opinion_factor = 0.2
				consumer_goods_factor = -0.05
				interest_rate_multiplier_modifier = 5
				econ_cycle_upg_cost_multiplier_modifier = 0.25
			}
		}

		SMA_effective_upor = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_effective_upor" }
			picture = production_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				production_factory_efficiency_gain_factor = 0.25
				production_factory_max_efficiency_factor = 0.1
				production_factory_start_efficiency_factor = 0.1
			}
		}

		########################################################################################
		### CONSTUCTIONS ### CONSTUCTIONS ### CONSTUCTIONS ### CONSTUCTIONS ### CONSTUCTIONS ###
		########################################################################################

		SMA_buildings = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_buildings" }
			picture = consumer_goods

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				production_speed_buildings_factor = 0.35
				tax_gain_multiplier_modifier = -0.35
				tax_rate_change_multiplier_modifier = 0.35
				investment_duration_modifier = 0.25
				investment_cost_modifier = 0.25
			}
		}

		SMA_foregin = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_buildings" }
			picture = consumer_goods

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				tax_rate_change_multiplier_modifier = 0.15
				investment_duration_modifier = -0.25
				investment_cost_modifier = -0.25
			}
		}

		#############################################################################################
		### ARMY ### ARMY ### ARMY ### ARMY ### ARMY ### ARMY ### ARMY ### ARMY ### ARMY ### ARMY ###
		#############################################################################################
		SMA_recruits = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_recruits" }
			picture = manpower_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			cancel = {
				NOT = {
					has_idea = draft_army
				}
			}

			modifier = {
				conscription = 0.15
				army_attack_factor = -0.1
				army_defence_factor = -0.1
				army_strength_factor = 0.2
				breakthrough_factor = 0.2
			}
		}

		SMA_contract_service = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_contract_service" }
			picture = Disorganization_Military_3

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			cancel = {
				NOT = {
					has_idea = volunteer_army
				}
			}

			modifier = {
				army_attack_factor = 0.15
				army_defence_factor = 0.15
				army_speed_factor = 0.05
			}
		}

		###################################################################################################
		### NO WARS ### NO WARS ### NO WARS ### NO WARS ### NO WARS ### NO WARS ### NO WARS ### NO WARS ###
		###################################################################################################

		SMA_no_wars_iii = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_no_wars_iii" }
			picture = army_corruption

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_attack_factor = -0.35
				army_defence_factor = -0.35
				army_org_factor = -0.25
				personnel_cost_multiplier_modifier = 0.25
				conscription = -0.5
			}
		}
		SMA_no_wars_ii = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_no_wars_ii" }
			picture = army_corruption2

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_attack_factor = -0.25
				army_defence_factor = -0.25
				army_org_factor = -0.15
				personnel_cost_multiplier_modifier = 0.15
				conscription = -0.2
			}
		}
		SMA_no_wars_i = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_no_wars_i" }
			picture = army_corruption3

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_attack_factor = -0.35
				army_defence_factor = -0.35
				army_org_factor = -0.25
				personnel_cost_multiplier_modifier = 0.05
			}
		}
		SMA_army_of_sacred_marino = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_army_of_sacred_marino" }
			picture = stake_on_army

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				army_defence_factor = 0.05
				army_org_factor = 0.15
				conscription_factor = 0.05
			}
		}

		###############################################################################################################
		### BALANCE ### BALANCE ### BALANCE ### BALANCE ### BALANCE ### BALANCE ### BALANCE ### BALANCE ### BALANCE ###
		###############################################################################################################

		SMA_increased_tourism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_increased_tourism" }
			picture = inflation3

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = 0.30
				corporate_tax_income_multiplier_modifier = -0.35
				custom_modifier_tooltip = balance_tourism_TT
			}
		}
		SMA_balance_tourism_ii = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_balance_tourism_ii" }
			picture = inflation3

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = 0.20
				corporate_tax_income_multiplier_modifier = -0.25
				custom_modifier_tooltip = balance_tourism_ii_TT
			}
		}
		SMA_balance_tourism_i = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_balance_tourism_i" }
			picture = inflation

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = 0.15
				corporate_tax_income_multiplier_modifier = -0.20
				custom_modifier_tooltip = balance_tourism_i_TT
			}
		}
		SMA_balance_balance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_balance_balance" }
			picture = foreign_capital

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = 0.10
				corporate_tax_income_multiplier_modifier = 0.10
				custom_modifier_tooltip = balance_balance_TT
			}
		}
		SMA_balance_economy_i = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_balance_economy_i" }
			picture = hyper_inflation

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = -0.20
				corporate_tax_income_multiplier_modifier = 0.15
				custom_modifier_tooltip = balance_economy_i_TT
			}
		}
		SMA_balance_economy_ii = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_balance_economy_ii" }
			picture = hyper_inflation3

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = -0.25
				corporate_tax_income_multiplier_modifier = 0.20
				custom_modifier_tooltip = balance_economy_ii_TT
			}
		}
		SMA_increased_economy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_increased_economy" }
			picture = hyper_inflation3

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = -0.35
				corporate_tax_income_multiplier_modifier = 0.30
				custom_modifier_tooltip = balance_economy_TT
			}
		}
		# Tourism
		SMA_emphass_on_tourism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_emphass_on_tourism" }
			picture = tourism

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				population_tax_income_multiplier_modifier = 0.3
				political_power_gain = 0.25
				consumer_goods_factor = 0.05
			}
		}
		SMA_bastion_of_beautiful = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea the_bastion_of_beautiful" }
			picture = tourism

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				monthly_population = 0.2
				political_power_gain = 0.5
				consumer_goods_factor = 0.05
			}
		}

		#######################################################################################################
		### RESEARCH CENTER ### RESEARCH CENTER ### RESEARCH CENTER ### RESEARCH CENTER ### RESEARCH CENTER ###
		#######################################################################################################

		SMA_center_i = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_center_i" }
			picture = research_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = 0.01
			}
		}
		SMA_center_ii = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_center_ii" }
			picture = research_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = 0.015
			}
		}
		SMA_center_iii = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_center_iii" }
			picture = research_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = 0.02
			}
		}
		SMA_center = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_center" }
			picture = research_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = 0.08
			}
		}

		############################################################################################################
		### CHANGE TEXTBOOKS ### CHANGE TEXTBOOKS ### CHANGE TEXTBOOKS ### CHANGE TEXTBOOKS ### CHANGE TEXTBOOKS ###
		############################################################################################################

		# Lang
		SMA_lang_ita = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_lang_ita" }
			picture = idea_ETH_italian_settlers

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				stability_factor = -0.05
				war_support_factor = 0.05
				interest_rate_multiplier_modifier = 5
			}
		}
		SMA_lang_romanion = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_lang_romanion" }
			picture = WIP_idea

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				stability_factor = 0.05
				war_support_factor = -0.05
				interest_rate_multiplier_modifier = -5
			}
		}

		# History
		SMA_hist_peace = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_hist_peace" }
			picture = idea_SWI_spirit_of_helvetia_defensive_peace

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				stability_factor = 0.05
				production_speed_buildings_factor = 0.02
				war_support_factor = -0.05
				conscription_factor = -0.05
			}
		}
		SMA_hist_mil = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_hist_mil" }
			picture = idea_SWI_spirit_of_helvetia_aggressive_peace

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				stability_factor = -0.05
				production_speed_buildings_factor = -0.02
				war_support_factor = 0.05
				conscription_factor = 0.05
			}
		}

		# Education
		SMA_ed_work = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_ed_work" }
			picture = GENERIC_public_works

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = -0.02
				monthly_population = 0.05
				conscription_factor = 0.02
				production_speed_buildings_factor = 0.05
			}
		}
		SMA_ed_res = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_ed_res" }
			picture = duplicate_research

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = 0.05
				monthly_population = -0.05
				conscription_factor = -0.05
				production_speed_buildings_factor = 0.02
			}
		}
		SMA_ed_mil = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_ed_mil" }
			picture = planning_bonus

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				research_speed_factor = -0.05
				monthly_population = 0.02
				conscription_factor = 0.05
				production_speed_buildings_factor = -0.02
			}
		}
		SMA_pro_lgbtqa_stance_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_pro_lgbtqa_stance_idea" }
			allowed = { always = no }
			picture = conservatism

			modifier = {
				MONTHLY_POPULATION = -0.05
				democratic_drift = 0.05
				stability_factor = -0.05
			}
		}
		SMA_half_pro_lgbtqa_stance_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_half_pro_lgbtqa_stance_idea" }
			allowed = { always = no }
			picture = conservatism

			modifier = {
				MONTHLY_POPULATION = -0.025
				democratic_drift = 0.025
				stability_factor = -0.025
			}
		}
		SMA_raising_taxes_idea = {

			allowed = {
				original_tag = SMA
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = market_dynamics

			modifier = {
				production_speed_buildings_factor = 0.1
				industrial_capacity_factory = 0.1
				economic_cycles_cost_factor = -0.3
				consumer_goods_factor = -0.03
			}
		}
		SMA_improved_administration = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_improved_administration" }
			allowed = { always = no }
			picture = political_power_bonus

			modifier = {
				political_power_factor = 0.2
			}
		}
		SMA_liberal_state = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_liberal_state" }
			allowed = { always = no }
			picture = flexible_foreign_policy2

			modifier = {
				monthly_population = -0.05
				research_speed_factor = 0.05
			}
		}
		SMA_pro_farmers = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_pro_farmers" }

			picture = agriculture

			modifier = {
				MONTHLY_POPULATION = 0.15
				local_resources_factor = 0.10
			}
		}
		SMA_bastion_of_freedom = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_bastion_of_freedom" }
			allowed = { always = no }
			allowed_civil_war = {
				always = yes
			}
			picture = land_of_the_free
			modifier = {
				stability_factor = 0.1
				crypto_strength = 1
				political_power_factor = 0.1
				drift_defence_factor = 0.5
			}
		}
		SMA_general_field_exercises = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_general_field_exercises" }
			picture = agriculture
			allowed = { original_tag = SMA }
			allowed_civil_war = { always = yes }
			modifier = {
				agricolture_productivity_modifier = 0.05
				total_workforce_modifier = 0.05
			}
		}
		SMA_legacy_of_past = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SMA_legacy_of_past" }

			allowed = { always = no }

			picture = fascism2
			modifier = {
				stability_factor = 0.05
				political_power_factor = 0.05
				conscription_factor = 0.05
				consumer_goods_factor = -0.03
			}
		}
	}
}