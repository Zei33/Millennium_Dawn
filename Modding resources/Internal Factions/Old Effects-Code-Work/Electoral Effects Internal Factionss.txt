
campaign_spending_effects = {
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 1
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 3
				}
			}
		}
		relative_change_neg_20 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 2
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 4
				}
			}
		}
		relative_change_neg_16 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 3
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 5
				}
			}
		}
		relative_change_neg_12 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 4
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 6
				}
			}
		}
		relative_change_neg_10 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 5
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 7
				}
			}
		}
		relative_change_neg_8 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 6
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 8
				}
			}
		}
		relative_change_neg_6 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 7
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 9
				}
			}
		}
		relative_change_neg_4 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 8
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 10
				}
			}
		}
		relative_change_neg_2 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 9
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 11
				}
			}
		}
		random_list = {
					50 = {
						relative_change_pos_2 = yes
					}
					50 = {
						relative_change_neg_2 = yes
					}
		}
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 10
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 12
				}
			}
		}
		relative_change_pos_2 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 11
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 13
				}
			}
		}
		relative_change_pos_4 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 12
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 14
				}
			}
		}
		relative_change_pos_6 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 13
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 15
				}
			}
		}
		relative_change_pos_8 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 14
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 16
				}
			}
		}
		relative_change_pos_10 = yes
	}
	if = {
		limit = {
			and = {
				has_country_flag = {
					flag = campaign_funding_count
					value > 15
				}
				has_country_flag = {
					flag = campaign_funding_count
					value < 17
				}
			}
		}
		relative_change_pos_12 = yes
	}
}

display_election_campaign_status = {
set_country_flag = { flag = campaign_funding_count value = 10 }
	
	if = { limit = { has_idea = irgc }
		display_election_status_IRGC = yes
	}
	if = { limit = { has_idea = small_medium_business_owners }
		display_election_status_small_medium_business_owners = yes
	}
	if = { limit = { has_idea = foreign_jihadis }
		display_election_status_foreign_jihadis = yes
	}
	if = { limit = { has_idea = iranian_quds_force }
		display_election_status_iranian_quds_force = yes
	}
	if = { limit = { has_idea = saudi_royal_family }
		display_election_status_saudi_royal_family = yes
	}
	if = { limit = { has_idea = wahabi_ulema }
		display_election_status_wahabi_ulema = yes
	}
	if = { limit = { has_idea = the_priesthood }
		display_election_status_the_priesthood = yes
	}
	if = { limit = { has_idea = The_Ulema }
		display_election_status_The_Ulema = yes
	}
	if = { limit = { has_idea = The_Clergy }
		display_election_status_The_Clergy = yes
	}
	if = { limit = { has_idea = landowners }
		display_election_status_landowners = yes
	}
	if = { limit = { has_idea = farmers }
		display_election_status_farmers = yes
	}
	if = { limit = { has_idea = vevak }
		display_election_status_VEVAK = yes
	}
	if = { limit = { has_idea = labour_unions }
		display_election_status_labour_unions = yes
	}
	if = { limit = { has_idea = communist_cadres }
		display_election_status_communist_cadres = yes
	}
	if = { limit = { has_idea = isi_pakistan }
		display_election_status_isi_pakistan = yes
	}
	if = { limit = { has_idea = intelligence_community }
		display_election_status_intelligence_community = yes
	}
	if = { limit = { has_idea = the_military }
		display_election_status_the_military = yes
	}
	if = { limit = { has_idea = defense_industry }
		display_election_status_defense_industry = yes
	}
	if = { limit = { has_idea = maritime_industry }
		display_election_status_maritime_industry = yes
	}
	if = { limit = { has_idea = oligarchs }
		display_election_status_oligarchs = yes
	}
	if = { limit = { has_idea = chaebols }
		display_election_status_chaebols = yes
	}
	if = { limit = { has_idea = industrial_conglomerates }
		display_election_status_industrial_conglomerates = yes
	}
	if = { limit = { has_idea = fossil_fuel_industry }
		display_election_status_fossil_fuel_industry = yes
	}
	if = { limit = { has_idea = wall_street }
		display_election_status_wall_street = yes
	}
	if = { limit = { has_idea = international_bankers }
		display_election_status_international_bankers = yes
	}
	if = { limit = { has_idea = the_donju }
		display_election_status_the_donju = yes
	}
	if = { limit = { has_idea = the_bazaar }
		display_election_status_the_bazaar = yes
	}
 
	if = { limit = { has_idea = internal_faction_removed }
		newline = yes internal_faction_removed_msg = yes
	}
}

display_election_status_small_medium_business_owners = {
	if = { limit = { has_idea = small_medium_business_owners }
		Display_small_medium_business_owners = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_small_medium_business_owners }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_small_medium_business_owners }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_small_medium_business_owners }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_small_medium_business_owners }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_small_medium_business_owners }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_saudi_royal_family = {
	if = { limit = { has_idea = saudi_royal_family }
		Display_saudi_royal_family = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_saudi_royal_family }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_saudi_royal_family }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_saudi_royal_family }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_saudi_royal_family }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_saudi_royal_family }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_landowners = {
	if = { limit = { has_idea = landowners }
		Display_landowners = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_landowners }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_landowners }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_landowners }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_landowners }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_landowners }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_defense_industry = {
	if = { limit = { has_idea = defense_industry }
		Display_defense_industry = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_defense_industry }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_defense_industry }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_defense_industry }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_defense_industry }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_defense_industry }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_maritime_industry = {
	if = { limit = { has_idea = maritime_industry }
		Display_maritime_industry = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_maritime_industry }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_maritime_industry }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_maritime_industry }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_maritime_industry }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_maritime_industry }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_oligarchs = {
	if = { limit = { has_idea = oligarchs }
		Display_oligarchs = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_oligarchs }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_oligarchs }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_oligarchs }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_oligarchs }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_oligarchs }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_chaebols = {
	if = { limit = { has_idea = chaebols }
		Display_chaebols = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_chaebols }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_chaebols }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_chaebols }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_chaebols }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_chaebols }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_industrial_conglomerates = {
	if = { limit = { has_idea = industrial_conglomerates }
		Display_industrial_conglomerates = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_industrial_conglomerates }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_industrial_conglomerates }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_industrial_conglomerates }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_industrial_conglomerates }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_industrial_conglomerates }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_fossil_fuel_industry = {
	if = { limit = { has_idea = fossil_fuel_industry }
		Display_fossil_fuel_industry = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_fossil_fuel_industry }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_fossil_fuel_industry }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_fossil_fuel_industry }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_fossil_fuel_industry }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_fossil_fuel_industry }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_wall_street = {
	if = { limit = { has_idea = wall_street }
		Display_wall_street = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_wall_street }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_wall_street }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_wall_street }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_wall_street }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_wall_street }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_international_bankers = {
	if = { limit = { has_idea = international_bankers }
		Display_international_bankers = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_international_bankers }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_international_bankers }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_international_bankers }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_international_bankers }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_international_bankers }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_the_donju = {
	if = { limit = { has_idea = the_donju }
		Display_the_donju = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_the_donju }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_the_donju }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_the_donju }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_the_donju }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_the_donju }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_the_bazaar = {
	if = { limit = { has_idea = the_bazaar }
		Display_the_bazaar = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_the_bazaar }
		the_opposition = yes massive = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_the_bazaar }
		the_opposition = yes large = yes campaign_funding = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_the_bazaar }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_the_bazaar }
		large = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_the_bazaar }
		massive = yes campaign_funding = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_labour_unions = {
	if = { limit = { has_idea = labour_unions }
		Display_labour_unions = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_labour_unions }
		the_opposition = yes massive = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_labour_unions }
		the_opposition = yes large = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_labour_unions }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_labour_unions }
		large = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_labour_unions }
		massive = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_farmers = {
	if = { limit = { has_idea = farmers }
		Display_farmers = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_farmers }
		the_opposition = yes massive = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_farmers }
		the_opposition = yes large = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_farmers }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_farmers }
		large = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_farmers }
		massive = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_The_Clergy = {
	if = { limit = { has_idea = The_Clergy }
		Display_The_Clergy = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_The_Clergy }
		the_opposition = yes massive = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_The_Clergy }
		the_opposition = yes large = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_The_Clergy }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_The_Clergy }
		large = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_The_Clergy }
		massive = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_The_Ulema = {
	if = { limit = { has_idea = The_Ulema }
		Display_The_Ulema = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_The_Ulema }
		the_opposition = yes massive = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_The_Ulema }
		the_opposition = yes large = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_The_Ulema }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_The_Ulema }
		large = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_The_Ulema }
		massive = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_the_priesthood = {
	if = { limit = { has_idea = the_priesthood }
		Display_the_priesthood = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_the_priesthood }
		the_opposition = yes massive = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_the_priesthood }
		the_opposition = yes large = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_the_priesthood }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_the_priesthood }
		large = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_the_priesthood }
		massive = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_wahabi_ulema = {
	if = { limit = { has_idea = wahabi_ulema }
		Display_wahabi_ulema = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_wahabi_ulema }
		the_opposition = yes massive = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_wahabi_ulema }
		the_opposition = yes large = yes volunteers = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_wahabi_ulema }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_wahabi_ulema }
		large = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_wahabi_ulema }
		massive = yes volunteers = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_IRGC = {
	if = { limit = { has_idea = irgc }
		Display_IRGC = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_IRGC }
		the_opposition = yes massive = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_IRGC }
		the_opposition = yes large = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_IRGC }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_IRGC }
		large = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_IRGC }
		massive = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_foreign_jihadis = {
	if = { limit = { has_idea = foreign_jihadis }
		Display_foreign_jihadis = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_foreign_jihadis }
		the_opposition = yes massive = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_foreign_jihadis }
		the_opposition = yes large = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_foreign_jihadis }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_foreign_jihadis }
		large = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_foreign_jihadis }
		massive = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_iranian_quds_force = {
	if = { limit = { has_idea = iranian_quds_force }
		Display_iranian_quds_force = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_iranian_quds_force }
		the_opposition = yes massive = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_iranian_quds_force }
		the_opposition = yes large = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_iranian_quds_force }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_iranian_quds_force }
		large = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_iranian_quds_force }
		massive = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_communist_cadres = {
	if = { limit = { has_idea = communist_cadres }
		Display_communist_cadres = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_communist_cadres }
		the_opposition = yes massive = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_communist_cadres }
		the_opposition = yes large = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_communist_cadres }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_communist_cadres }
		large = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_communist_cadres }
		massive = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_the_military = {
	if = { limit = { has_idea = the_military }
		Display_the_military = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_the_military }
		the_opposition = yes massive = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_the_military }
		the_opposition = yes large = yes intimidation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_the_military }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_the_military }
		large = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_the_military }
		massive = yes intimidation = yes for = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_intelligence_community = {
	if = { limit = { has_idea = intelligence_community }
		Display_intelligence_community = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_intelligence_community }
		the_opposition = yes massive = yes disinformation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_intelligence_community }
		the_opposition = yes large = yes disinformation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_intelligence_community }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_intelligence_community }
		Root_GetLeader = yes large = yes disinformation = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_intelligence_community }
		Root_GetLeader = yes massive = yes disinformation = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_isi_pakistan = {
	if = { limit = { has_idea = isi_pakistan }
		Display_isi_pakistan = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_isi_pakistan }
		the_opposition = yes massive = yes disinformation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_isi_pakistan }
		the_opposition = yes large = yes disinformation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_isi_pakistan }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_isi_pakistan }
		Root_GetLeader = yes large = yes disinformation = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_isi_pakistan }
		Root_GetLeader = yes massive = yes disinformation = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}

display_election_status_VEVAK = {
	if = { limit = { has_idea = vevak }
		Display_VEVAK = yes dash = yes providing = yes
	}
	if = { limit = { has_country_flag = hostile_VEVAK }
		the_opposition = yes massive = yes disinformation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -2 }
	}
	if = { limit = { has_country_flag = negative_VEVAK }
		the_opposition = yes large = yes disinformation = yes against = yes Root_GetLeader = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = -1 }
	}
	if = { limit = { has_country_flag = indifferent_VEVAK }
		election_indifferent = yes newline = yes
	}
	if = { limit = { has_country_flag = positive_VEVAK }
		Root_GetLeader = yes large = yes disinformation = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 1 }
	}
	if = { limit = { has_country_flag = enthusiastic_VEVAK }
		Root_GetLeader = yes massive = yes disinformation = yes newline = yes
		modify_country_flag = { flag = campaign_funding_count value = 2 }
	}
}
