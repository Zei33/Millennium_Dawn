internal_factions_interaction = {
	GIFI_dec_small_medium_business_owners = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = small_medium_business_owners
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_international_bankers = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = international_bankers
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_fossil_fuel_industry = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = fossil_fuel_industry
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_industrial_conglomerates = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = industrial_conglomerates
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_oligarchs = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = oligarchs
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_maritime_industry = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = maritime_industry
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_defense_industry = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = defense_industry
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_the_military = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = the_military
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_intelligence_community = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = intelligence_community
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_labour_unions = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = labour_unions
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_landowners = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = landowners
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_communist_cadres = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = communist_cadres
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_farmers = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = farmers
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_the_priesthood = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = the_priesthood
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_The_Clergy = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = The_Clergy
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_The_Ulema = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = The_Ulema
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_wahabi_ulema = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = wahabi_ulema
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_the_donju = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = the_donju
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_the_bazaar = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = the_bazaar
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_saudi_royal_family = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = saudi_royal_family
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_IRGC = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = irgc
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_iranian_quds_force = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = iranian_quds_force
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_foreign_jihadis = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = foreign_jihadis
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_VEVAK = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = vevak
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_isi_pakistan = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = isi_pakistan
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_wall_street = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = wall_street
		}
		
		complete_effect = {
		}
	}
	GIFI_dec_chaebols = {

		icon = decision

		available = {
		}

		#ai_will_do = {
			#factor = 0
		#}

		visible = {
			has_idea = chaebols
		}
		
		complete_effect = {
		}
	}
}