#Designed and coded by Jlakshan
focus_tree = {

	id = Iraq.txt

	country = {
		factor = 0
		modifier = {
			add = 10
		tag = IRQ
		}
	}


	#Domestic policies
	focus = {
		id = IRQ_the_national_assembly
		icon = volunteer_alliance
		#relative_position_id = ITA_let_salaries_decrease
		x = 14
		y = 0
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
		}

		bypass = {
		}

		completion_reward = {
			add_political_power = 341
			add_ideas = the_national_assembly
			log = "[GetDateText]: [Root.GetName]: Focus IRQ_the_national_assembly"
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_new_pan_arabism
		icon = volunteer_alliance
		relative_position_id = IRQ_the_national_assembly
		x = -8
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_the_national_assembly
		}

		mutually_exclusive = {
			Focus = IRQ_align_with_the_west
		}

		bypass = {
		}

		completion_reward = {
			add_ideas = new_pan_arabism

			log = "[GetDateText]: [Root.GetName]: IRQ_new_pan_arabism"
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_rise_of_the_baath
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 0
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_new_pan_arabism
		}

		bypass = {
		}

		completion_reward = {
			add_ideas = rise_of_the_baath
			log = "[GetDateText]: [Root.GetName]: IRQ_rise_of_the_baath"
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_propaganda_effort
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -2
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_rise_of_the_baath
		}

		bypass = {
		}

		mutually_exclusive = {
			focus = IRQ_conserve_your_wealth
		}

		completion_reward = {
			add_ideas = propaganda_effort
            set_temp_variable = { treasury_change = -19.87 }
            modify_treasury_effect = yes
			log = "[GetDateText]: [Root.GetName]: IRQ_propaganda_effort"
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_conserve_your_wealth
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 2
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_rise_of_the_baath
		}

		bypass = {
		}

		mutually_exclusive = {
			focus = IRQ_propaganda_effort
		}

		completion_reward = {
			add_ideas = conserve_your_wealth
			log = "[GetDateText]: [Root.GetName]: IRQ_conserve_your_wealth"
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_officiate_the_dictorship
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -3
		y = 3
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_propaganda_effort
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_officiate_the_dictorship"
			add_stability = -0.10
			add_war_support = 0.10
			add_popularity = {
				ideology = nationalist
				popularity = 0.10
			}
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_consolidation_of_power
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 0
		y = 3
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_propaganda_effort
			Focus = IRQ_conserve_your_wealth
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_consolidation_of_power"
			add_ideas = consolidation_of_power
			add_stability = 0.05
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_preserve_serency
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 3
		y = 3
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_conserve_your_wealth
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_preserve_serency"
			add_stability = 0.10
			add_ideas = preserve_serency
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_interventionism
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -5
		y = 4
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_officiate_the_dictorship
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_interventionism"
			add_war_support = 0.10
			add_ideas = interventionism

		}

		ai_will_do = {
			factor = 1
		}
	}
#
	focus = {
		id = IRQ_destroy_the_democracy
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -3
		y = 4
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_officiate_the_dictorship
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_destroy_the_democracy"
			add_popularity = {
				ideology = nationalist
				popularity = 0.25
			}
			add_popularity = {
				ideology = democratic
				popularity = -0.25
			}
			add_stability = -0.05
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_establish_the_pan_arab_state
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 0
		y = 4
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_officiate_the_dictorship
			Focus = IRQ_preserve_serency
			}

		prerequisite = {
			Focus = IRQ_consolidation_of_power
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_establish_the_pan_arab_state"
			create_faction = "The Pan Arab State"
			IRQ = { add_to_faction = SYR }
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_strengthen_national_unity
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 3
		y = 4
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_preserve_serency
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_strengthen_national_unity"
			add_stability = 0.10
			add_ideas = strengthen_national_unity
			add_political_power = 100
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_isolationism
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 5
		y = 4
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_preserve_serency
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_isolationism"
			 add_ideas = isolationism
			 add_war_support = 0.05
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_path_of_golden_black_eagle
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -5
		y = 5
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_interventionism
			Focus = IRQ_destroy_the_democracy
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_path_of_golden_black_eagle"
			add_air_experience = 25
			add_ideas = production_speed_air_base_factor
			171 = {
				add_building_construction = {
   				 type = air_base
    			 level = 2
   				 instant_build = yes
				}
			}
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_bribe_the_tribal_leaders
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -3
		y = 5
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_interventionism
			Focus = IRQ_destroy_the_democracy
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_bribe_the_tribal_leaders"
			add_political_power = 100
            set_temp_variable = { treasury_change = -19.87 }
			modify_treasury_effect = yes
			IRQ = { country_event = { id = IraqNews.20 } }      # THIS EVENT need to be supplemented

		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_political_dawn
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 0
		y = 6
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_path_of_golden_black_eagle
			Focus = IRQ_bribe_the_tribal_leaders
			Focus = IRQ_path_to_stability
			Focus = IRQ_establish_tribal_peace_keeping_gathering
		}
		prerequisite = {
			Focus = IRQ_establish_the_pan_arab_state
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_political_dawn"
			add_political_power = 100
			add_ideas political_dawn
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_path_to_stability
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 3
		y = 5
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_strengthen_national_unity
			Focus = IRQ_isolationism
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_political_dawn"
			add_stability = 0.10
			add_ideas path_to_stability
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_establish_tribal_peace_keeping_gathering
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 5
		y = 5
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_strengthen_national_unity
			Focus = IRQ_isolationism
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_establish_tribal_peace_keeping_gathering"
			add_ideas establish_tribal_peace_keeping_gathering
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_the_second_public_purge
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -2
		y = 7
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_path_of_golden_black_eagle
			Focus = IRQ_bribe_the_tribal_leaders
			Focus = IRQ_political_dawn
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_the_second_public_purge"
			add_political_power = -25
			add_ideas the_second_public_purge

		}

		ai_will_do = {
			factor = 1
		}
	}
	#I don't know what to put here -Gabesz
	focus = {
		id = IRQ_reorganize_right_winged_carbinet
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 2
		y = 7
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_path_to_stability
			Focus = IRQ_establish_tribal_peace_keeping_gathering
			Focus = IRQ_political_dawn
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_reorganize_right_winged_carbinet"
			add_ideas reorganize_right_winged_carbinet
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_reform_judicial_system
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -4
		y = 8
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_the_second_public_purge
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_reform_judicial_system"


		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_supreme_judge
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = -2
		y = 8
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_the_second_public_purge
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_supreme_judge"
		add_stability = 0.05
		add_political_power = 25
		IRQ = { country_event = { id = IraqNews.21 } }      # THIS EVENT need to be supplemented
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_supress_foreign_news
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 0
		y = 8
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_the_second_public_purge
			Focus = IRQ_reorganize_right_winged_carbinet
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_supress_foreign_news"
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_limit_foreign_travel
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 0
		y = 9
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_supress_foreign_news
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_limit_foreign_travel"
		}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_monitor_political_parties
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 2
		y = 8
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_reorganize_right_winged_carbinet
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_monitor_political_parties"
			add_ideas IRQ_monitor_political_parties1
	}

		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = IRQ_united_iraq
		icon = volunteer_alliance
		relative_position_id = IRQ_new_pan_arabism
		x = 4
		y = 8
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_reorganize_right_winged_carbinet
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_united_iraq"
			add_opinion_modifier = { target = USA modifier = medium_increase }
			add_political_power = 15
			add_stability = 0.05
			add_manpower = 500
		}

		ai_will_do = {
			factor = 1
		}
	}
	#Align with The West
	focus = {
		id = IRQ_align_with_the_west
		icon = volunteer_alliance
		relative_position_id = IRQ_the_national_assembly
		x = 2
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_the_national_assembly
		}

		bypass = {
		}

		mutually_exclusive = {
			Focus = IRQ_new_pan_arabism
		}

		completion_reward = {
			every_country = {
				limit = {
					OR = {
					has_idea = NATO_member
					has_idea = EU_member
				}
			 }
   				PREV = {
     		  	add_opinion_modifier = {
            target = THIS
            modifier = declaration_of_friendship
        }
    }
}
			log = "[GetDateText]: [Root.GetName]: IRQ_align_with_the_west"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Secure American Funding
	focus = {
		id = IRQ_secure_american_funding
		icon = volunteer_alliance
		relative_position_id = IRQ_align_with_the_west
		x = 0
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_align_with_the_west
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_secure_american_funding"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Propaganda campaign
	focus = {
		id = IRQ_propaganda_campaign
		icon = volunteer_alliance
		relative_position_id = IRQ_secure_american_funding
		x = -2
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_secure_american_funding
		}

		bypass = {
		}

		mutually_exclusive = {
			focus = IRQ_found_pro-west_millitias
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_propaganda_campaign"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Found Pro-West millitias
	focus = {
		id = IRQ_found_pro-west_millitias
		icon = volunteer_alliance
		relative_position_id = IRQ_secure_american_funding
		x = 2
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_secure_american_funding
		}

		bypass = {
		}

		mutually_exclusive = {
			focus = IRQ_propaganda_campaign
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_found_pro-west_millitias"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Anti Goverment radio stations
	focus = {
		id = IRQ_anti_goverment_radio_stations
		icon = volunteer_alliance
		relative_position_id = IRQ_propaganda_campaign
		x = 0
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_propaganda_campaign
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_anti_goverment_radio_stations"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Wok with local Tribes
	focus = {
		id = IRQ_work_with_local_tribes
		icon = volunteer_alliance
		relative_position_id = IRQ_found_pro-west_millitias
		x = 0
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_found_pro-west_millitias
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_work_with_local_tribes"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#US-iraqi arms Deal
	focus = {
		id = IRQ_us-iraqi_arms_deal
		icon = volunteer_alliance
		relative_position_id = IRQ_secure_american_funding
		x = 0
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_work_with_local_tribes
			Focus = IRQ_anti_goverment_radio_stations
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_us-iraqi_arms_deal"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Anti Goverment demonstraitons
	focus = {
		id = IRQ_anti_government_demonstrations
		icon = volunteer_alliance
		relative_position_id = IRQ_anti_goverment_radio_stations
		x = -1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_anti_goverment_radio_stations

		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_anti_government_demonstrations"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Militia recruitment campaign
	focus = {
		id = IRQ_militia_recruitment_campaign
		icon = volunteer_alliance
		relative_position_id = IRQ_work_with_local_tribes
		x = 1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_work_with_local_tribes
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_militia_recruitment_campaign"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Topple Saddam
	focus = {
		id = IRQ_topple_saddam
		icon = volunteer_alliance
		relative_position_id = IRQ_anti_government_demonstrations
		x = 0
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_anti_government_demonstrations
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_topple_saddam"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#us-iraqi drones deal
	focus = {
		id = IRQ_us-iraqi_drones_deal
		icon = volunteer_alliance
		relative_position_id = IRQ_us-iraqi_arms_deal
		x = 0
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_topple_saddam
			Focus = IRQ_gain_american_solidarity
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_us-iraqi_drones_deal"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Gain American Solidarity
	focus = {
		id = IRQ_gain_american_solidarity
		icon = volunteer_alliance
		relative_position_id = IRQ_militia_recruitment_campaign
		x = 0
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_militia_recruitment_campaign
		}

		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_gain_american_solidarity"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Capture Saddam
	focus = {
		id = IRQ_capture_saddam
		icon = volunteer_alliance
		relative_position_id = IRQ_topple_saddam
		x = 1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_topple_saddam
		}

		bypass = {
		}

		mutually_exclusive = {
			Focus = IRQ_secure_basra
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_capture_saddam"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Secure Basra
	focus = {
		id = IRQ_secure_basra
		icon = volunteer_alliance
		relative_position_id = IRQ_gain_american_solidarity
		x = -1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_gain_american_solidarity
		}

		bypass = {
		}

		mutually_exclusive = {
			Focus = IRQ_capture_saddam
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_secure_basra"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Imprison Saddam
	focus = {
		id = IRQ_imprison_saddam
		icon = volunteer_alliance
		relative_position_id = IRQ_capture_saddam
		x = -1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_capture_saddam
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_imprison_saddam"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Declare civil war
	focus = {
		id = IRQ_declare_civil_war
		icon = volunteer_alliance
		relative_position_id = IRQ_secure_basra
		x = 1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_secure_basra
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_declare_civil_war"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Propaganda effort {Increase war support}
	focus = {
		id = IRQ_propaganda_effort1
		icon = volunteer_alliance
		relative_position_id = IRQ_us-iraqi_drones_deal
		x = 0
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_declare_civil_war
			Focus = IRQ_imprison_saddam
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_propaganda_effort1"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Public speech
	focus = {
		id = IRQ_public_speech
		icon = volunteer_alliance
		relative_position_id = IRQ_propaganda_effort1
		x = -1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_propaganda_effort1
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_public_speech"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#The Baghdad Trials (Saddam gets executed)
	focus = {
		id = IRQ_the_baghdad_trials
		icon = volunteer_alliance
		relative_position_id = IRQ_propaganda_effort1
		x = 1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_propaganda_effort1
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_the_baghdad_trials"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Makings of a fairer and Just Iraq
	focus = {
		id = IRQ_makings_of_a_fairer_and_just_iraq
		icon = volunteer_alliance
		relative_position_id = IRQ_propaganda_effort1
		x = 0
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_the_baghdad_trials
		}
		prerequisite = {
			Focus = IRQ_public_speech
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_makings_of_a_fairer_and_just_iraq"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Establish the Democratic Republic
	focus = {
		id = IRQ_establish_the_democratic_republic
		icon = volunteer_alliance
		relative_position_id = IRQ_makings_of_a_fairer_and_just_iraq
		x = 0
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_makings_of_a_fairer_and_just_iraq
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_establish_the_democratic_republic"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Ban The Communist Party
	focus = {
		id = IRQ_ban_the_communist_party
		icon = volunteer_alliance
		relative_position_id = IRQ_establish_the_democratic_republic
		x = -2
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_establish_the_democratic_republic
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_ban_the_communist_party"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Act to protect same-sex marriage
	focus = {
		id = IRQ_act_to_protect_same-sex_marriage
		icon = volunteer_alliance
		relative_position_id = IRQ_establish_the_democratic_republic
		x = 2
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_establish_the_democratic_republic
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_act_to_protect_same-sex_marriage"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#ban The Ba'ath party
	focus = {
		id = IRQ_ban_the_ba'ath_party
		icon = volunteer_alliance
		relative_position_id = IRQ_ban_the_communist_party
		x = -1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_ban_the_communist_party
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_ban_the_ba'ath_party"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#expand the women workforce
	focus = {
		id = IRQ_expand_the_women_workforce
		icon = volunteer_alliance
		relative_position_id = IRQ_ban_the_communist_party
		x = 2
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_ban_the_ba'ath_party
			Focus = IRQ_bill_to_protect_same-sex_marriage
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_expand_the_women_workforce"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Bill to protect same-sex Marriage
	focus = {
		id = IRQ_bill_to_protect_same-sex_marriage
		icon = volunteer_alliance
		relative_position_id = IRQ_act_to_protect_same-sex_marriage
		x = 1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_act_to_protect_same-sex_marriage}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_bill_to_protect_same-sex_marriage"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#A Democratic Cabinet
	focus = {
		id = IRQ_a_democratic_cabinet
		icon = volunteer_alliance
		relative_position_id = IRQ_ban_the_ba'ath_party
		x = 1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_ban_the_ba'ath_party
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_a_democratic_cabinet"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Civil Right
	focus = {
		id = IRQ_civil_rights
		icon = volunteer_alliance
		relative_position_id = IRQ_bill_to_protect_same-sex_marriage
		x = -1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_bill_to_protect_same-sex_marriage
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_civil_rights"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Hold Tribal elections
	focus = {
		id = IRQ_hold_tribal_elections
		icon = volunteer_alliance
		relative_position_id = IRQ_a_democratic_cabinet
		x = -1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_a_democratic_cabinet
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_hold_tribal_elections"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Power of the people
	focus = {
		id = IRQ_power_of_the_people
		icon = volunteer_alliance
		relative_position_id = IRQ_civil_rights
		x = 1
		y = 1
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_civil_rights
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_power_of_the_people"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#free press
	focus = {
		id = IRQ_free_press
		icon = volunteer_alliance
		relative_position_id = IRQ_expand_the_women_workforce
		x = 0
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_power_of_the_people
			Focus = IRQ_hold_tribal_elections
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_free_press"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#Crackdown on corruption
	focus = {
		id = IRQ_crackdown_on_corruption
		icon = volunteer_alliance
		relative_position_id = IRQ_free_press
		x = -2
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_hold_tribal_elections
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_crackdown_on_corruption"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#The Judicial reform
	focus = {
		id = IRQ_the_judicial_reform
		icon = volunteer_alliance
		relative_position_id = IRQ_power_of_the_people
		x = -1
		y = 2
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_power_of_the_people
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_the_judicial_reform"
		}

		ai_will_do = {
			factor = 0
		}
	}
	#A New Constitution
	focus = {
		id = IRQ_a_new_constitution
		icon = volunteer_alliance
		relative_position_id = IRQ_free_press
		x = 0
		y = 3
		cost = 10

		search_filters = {FOCUS_FILTER_POLITICAL }

		prerequisite = {
			Focus = IRQ_crackdown_on_corruption
			Focus = IRQ_the_judicial_reform
		}
		bypass = {
		}

		mutually_exclusive = {
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: IRQ_a_new_constitution"
		}

		ai_will_do = {
			factor = 0
		}
	}
}