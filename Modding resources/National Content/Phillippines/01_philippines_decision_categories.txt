#Philippines MILF Mechanic Category
PHI_milf_decisions_category = {
	icon = political_actions
	visible = {
		original_tag = PHI
		has_country_flag = phi_milf_exists
		NOT = {
			has_country_flag = PHI_milf_defeated
			has_country_flag = PHI_permanent_ceasefire
		}
	}
	allowed = {
		original_tag = PHI
	}
	priority = 10
}
PHI_communist_insurgency_category = {
	icon = revolution
	visible = {
		original_tag = PHI
		NOT = { has_country_flag = PHI_ended_the_communist_insurgency }
	}
	allowed = {
		original_tag = PHI
	}
	priority = 10
}