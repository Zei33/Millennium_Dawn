scripted_gui = {

	tribalism_decision_ui = {
		context_type = decision_category
		window_name = tribalism_decision_ui_window

		effects = {
			tribalism_info_box_click = {
				KEN = {
					country_event = KEN_info.1
				}
			}
		}
		effects = {
			tribalism_refresh_box_click = {
				KEN = {
					KEN_calculate_opinion = yes
				}
			}
		}

	}

	eac_picture_decision_ui = {
		context_type = decision_category
		window_name = eac_picture_decision_ui_window

		effects = {
			eac_info_box_click = {
				ROOT = {
					country_event = eac.1
				}
			}
		}

		properties = {
			custom_picture_icon = {
				frame = eac_decision_banner
			}
		}

	}

}