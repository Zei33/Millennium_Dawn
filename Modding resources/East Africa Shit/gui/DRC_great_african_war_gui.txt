scripted_gui = {
	great_african_war_decision_ui = {
		window_name = "great_african_war_scripted_gui_window"
		context_type = decision_category

		properties = {
			bandundu_icon = { frame = gaw_frame^1 }
			bas_congo_icon = { frame = gaw_frame^2 }
			equateur_icon = { frame = gaw_frame^3 }
			ituri_icon = { frame = gaw_frame^4 }
			kasai_icon = { frame = gaw_frame^5 }
			katanga_icon = { frame = gaw_frame^6 }
			maniema_icon = { frame = gaw_frame^7 }
			nord_kivu_icon = { frame = gaw_frame^8 }
			orientale_icon = { frame = gaw_frame^9 }
			sud_kivu_icon = { frame = gaw_frame^10 }
			tanganyika_icon = { frame = gaw_frame^11 }
			tshopo_icon = { frame = gaw_frame^12 }
			ubangi_icon = { frame = gaw_frame^13 }
		}
	}
}