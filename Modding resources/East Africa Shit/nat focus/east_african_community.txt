	shared_focus = {
		id = EAC_east_african_community
		icon = eac

		search_filters = {FOCUS_FILTER_EAC}

		x = 0
		y = 0

		offset = {
			x = 17
			y = 9
			trigger = {
				tag = TNZ
			}
		}

		offset = {
			x = 19
			y = 9
			trigger = {
				tag = KEN
			}
		}

			#x = 30
			#y = 10

		offset = {
			x = 18
			y = 7
			trigger = {
				tag = UGA
			}
		}

		offset = {
			x = 17
			y = 8
			trigger = {
				tag = RWA
			}
		}

		offset = {
			x = 17
			y = 8
			trigger = {
				tag = BUR
			}
		}

		offset = {
			x = 16
			y = 6
			trigger = {
				tag = COM
			}
		}


		available = {
			has_idea = idea_eac_member_state
		}

		cost = 2.9

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus EAC_east_african_community"
			add_political_power = 20
			hidden_effect = {
				country_event = eac.0
				country_event = eac.0
				country_event = eac.0
			}
		}

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_cultural_exchange_program
		icon = eac_hands


		x = -15
		y = 1

		available = {
			has_idea = idea_eac_member_state
		}

		cost = 5

		relative_position_id = EAC_east_african_community

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_cultural_exchange_program"
			custom_effect_tooltip = EAC_community_influence_1_tt
			add_to_variable = { var = EAC_influence value = 1 }
		}

		prerequisite = {  focus = EAC_east_african_community  }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_kiswahili_commission
		icon = eac_hands


		x = -17
		y = 1

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_kiswahili_commission_won
		}

		cost = 5

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_kiswahili_commission"
			unlock_decision_tooltip = EAC_kiswahili_commission_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_kiswahili_commission_tt
		}

		prerequisite = {  focus = EAC_east_african_community  }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_science_technology
		icon = eac_research


		x = -12
		y = 1

		relative_position_id = EAC_east_african_community

		available = {
			custom_trigger_tooltip = {
				tooltip = EAC_25_influence_tt
				check_variable = {
					var = EAC_influence
					value > 24
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_science_technology_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_science_technology"
			unlock_decision_tooltip = EAC_science_technology_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_science_technology_tt
		}

		prerequisite = { focus = EAC_east_african_community }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_education_agency
		icon = eac_science


		x = -15
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			custom_trigger_tooltip = {
				tooltip = EAC_100_influence_tt
				check_variable = {
					var = EAC_influence
					value > 99
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_education_agency_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_education_agency"
			unlock_decision_tooltip = EAC_education_agency_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_education_agency_tt
		}

		prerequisite = { focus = EAC_science_technology }

		prerequisite = { focus = EAC_cultural_exchange_program }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_higher_education
		icon = eac_research


		x = -15
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_education_agency_won
			has_global_flag = eac_kiswahili_commission_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_higher_education_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_higher_education"
			unlock_decision_tooltip = EAC_higher_education_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_higher_education_tt
		}

		prerequisite = { focus = EAC_education_agency }
		prerequisite = { focus = EAC_kiswahili_commission }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_religious_autonomy
		icon = eac_hands


		x = -17
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_religious_autonomy_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_religious_autonomy"
			unlock_decision_tooltip = EAC_religious_autonomy_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_religious_autonomy_tt
		}

		prerequisite = { focus = EAC_kiswahili_commission }

		prerequisite = { focus = EAC_cultural_exchange_program }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_official_language
		icon = eac_hands


		x = -17
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_education_agency_won
			has_global_flag = eac_kiswahili_commission_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_official_language_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_official_language"
			unlock_decision_tooltip = EAC_official_language_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_official_language_tt
		}

		prerequisite = { focus = EAC_religious_autonomy }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_one_identity
		icon = eac_hands


		x = -17
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_official_language_won
			has_global_flag = eac_religious_autonomy_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_one_identity_won
		}

		cost = 12.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_one_identity"
			unlock_decision_tooltip = EAC_one_identity_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_one_identity_tt
		}

		prerequisite = {  focus = EAC_official_language }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_health_research
		icon = eac_research


		x = -13
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_education_agency_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_health_research_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_health_research"
			unlock_decision_tooltip = EAC_health_research_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_health_research_tt
		}

		prerequisite = { focus = EAC_education_agency }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_fund_merge_bills
		icon = eac_research


		x = -13
		y = 2

		relative_position_id = EAC_east_african_community

		available = { has_idea = idea_eac_member_state }

		bypass = {
			has_global_flag = eac_fund_merge_bills_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_fund_merge_bills"
			unlock_decision_tooltip = EAC_fund_merge_bills_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_fund_merge_bills_tt
		}

		prerequisite = { focus = EAC_science_technology }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_lake_act
		icon = eac_hands


		x = -10
		y = 1

		relative_position_id = EAC_east_african_community

		available = { has_idea = idea_eac_member_state }

		bypass = {
			has_global_flag = eac_lake_act_won
		}

		cost = 5

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_lake_act"
			unlock_decision_tooltip = EAC_lake_act_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_lake_act_tt
		}

		prerequisite = { focus = EAC_east_african_community }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_conservation_act
		icon = eac_conservation


		x = -11
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			OR = {
				has_global_flag = eac_science_technology_won
				has_global_flag = eac_lake_act_won
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_conservation_act_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_conservation_act"
			unlock_decision_tooltip = EAC_conservation_act_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_conservation_act_tt
		}

		prerequisite = { focus = EAC_science_technology focus = EAC_lake_act }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_tourism_agency
		icon = eac_hands


		x = -11
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			OR = {
				has_global_flag = eac_conservation_act_won
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_tourism_agency_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_tourism_agency"
			unlock_decision_tooltip = EAC_tourism_agency_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_tourism_agency_tt
		}

		prerequisite = { focus = EAC_conservation_act }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_development_fund
		icon = trade_with_east_africa


		x = -8
		y = 1

		relative_position_id = EAC_east_african_community

		available = { has_idea = idea_eac_member_state }

		bypass = {
			has_global_flag = eac_development_fund_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_development_fund"
			unlock_decision_tooltip = EAC_development_fund_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_development_fund_tt
		}


		prerequisite = { focus = EAC_east_african_community }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_international_investment_committee
		icon = trade_with_east_africa


		x = -9
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_international_investment_committee_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_international_investment_committee"
			unlock_decision_tooltip = EAC_international_investment_committee_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_international_investment_committee_tt
		}


		prerequisite = { focus = EAC_development_fund }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_competition_authority
		icon = trade_with_east_africa


		x = -9
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_competition_authority_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_competition_authority"
			unlock_decision_tooltip = EAC_competition_authority_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_competition_authority_tt
		}


		prerequisite = { focus = EAC_international_investment_committee }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_eu_eac
		icon = align_to_europe


		x = -14
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_common_market_won
			custom_trigger_tooltip = {
				tooltip = EAC_eu_has_office_tt
				any_of_scopes = {
					array = global.EU_member
					has_idea = EU_foreign_minister
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_eu_eac_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_eu_eac"
			unlock_decision_tooltip = EAC_eu_eac_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_eu_eac_tt
		}

		prerequisite = { focus = EAC_competition_authority }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_growth_pact
		icon = align_to_america


		x = -12
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_common_market_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_growth_pact_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_growth_pact"
			unlock_decision_tooltip = EAC_growth_pact_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_growth_pact_tt
		}

		prerequisite = { focus = EAC_competition_authority }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_china_agreement
		icon = align_to_china


		x = -10
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_common_market_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_china_agreement_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_china_agreement"
			unlock_decision_tooltip = EAC_china_agreement_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_china_agreement_tt
		}

		prerequisite = { focus = EAC_competition_authority }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_tripartite_treaty
		icon = align_to_africa


		x = -8
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_common_market_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_tripartite_treaty_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_tripartite"
			unlock_decision_tooltip = EAC_tripartite_treaty_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_tripartite_treaty_tt
		}

		prerequisite = { focus = EAC_competition_authority }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_development_bank
		icon = eac_bank


		x = -7
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_development_bank_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_development_bank"
			unlock_decision_tooltip = EAC_development_bank_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_development_bank_tt
		}


		prerequisite = { focus = EAC_development_fund }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_central_bank
		icon = eac_bank


		x = -7
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			custom_trigger_tooltip = {
				tooltip = EAC_75_influence_tt
				check_variable = {
					var = EAC_influence
					value > 74
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_central_bank_won
		}

		cost = 12.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_central_bank"
			unlock_decision_tooltip = EAC_central_bank_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_central_bank_tt
		}

		prerequisite = { focus = EAC_development_bank }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_customs_union
		icon = trade_with_east_africa

		x = -5
		y = 1

		relative_position_id = EAC_east_african_community

		available = {
			custom_trigger_tooltip = {
				tooltip = EAC_25_influence_tt
				check_variable = {
					var = EAC_influence
					value > 24
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_customs_union_won
		}

		cost = 12.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_customs_union"
			unlock_decision_tooltip = EAC_customs_union_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_customs_union_tt
		}

		prerequisite = { focus = EAC_east_african_community }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_common_market
		icon = trade_with_east_africa


		x = -5
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_customs_union_won
			custom_trigger_tooltip = {
				tooltip = EAC_50_influence_tt
				check_variable = {
					var = EAC_influence
					value > 49
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_common_market_won
		}

		cost = 12.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_common_market"
			unlock_decision_tooltip = EAC_common_market_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_common_market_tt
		}

		prerequisite = { focus = EAC_customs_union }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_central_managment
		icon = eac_hands


		x = -3
		y = 2

		relative_position_id = EAC_east_african_community

		available = { has_idea = idea_eac_member_state }

		bypass = {
			has_global_flag = eac_central_managment_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_central_managment"
			unlock_decision_tooltip = EAC_central_managment_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_central_managment_tt
		}

		prerequisite = { focus = EAC_east_african_community }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_economic_convergence
		icon = eac_growth


		x = -5
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_common_market_won
			has_global_flag = eac_central_managment_won
			custom_trigger_tooltip = {
				tooltip = EAC_75_influence_tt
				check_variable = {
					var = EAC_influence
					value > 74
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_economic_convergence_won
		}

		cost = 12.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_economic_convergence"
			unlock_decision_tooltip = EAC_economic_convergence_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_economic_convergence_tt
		}

		prerequisite = { focus = EAC_common_market }

		prerequisite = { focus = EAC_central_managment }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_monetary_union
		icon = eac_growth


		x = -6
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_central_bank_won
			has_global_flag = eac_economic_convergence_won
			custom_trigger_tooltip = {
				tooltip = EAC_100_influence_tt
				check_variable = {
					var = EAC_influence
					value > 99
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_monetary_union_won
		}

		cost = 12.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_monetary_union"
			unlock_decision_tooltip = EAC_monetary_union_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_monetary_union_tt
		}

		prerequisite = { focus = EAC_central_bank }

		prerequisite = { focus = EAC_economic_convergence }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_passport
		icon = eac_signing


		x = -3
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			has_global_flag = eac_central_managment_won
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_passport_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_passport"
			unlock_decision_tooltip = EAC_passport_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_passport_tt
		}

		prerequisite = { focus = EAC_central_managment }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_cassoa
		icon = eac_hands


		x = -2
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_cassoa_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_cassoa"
			unlock_decision_tooltip = EAC_cassoa_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_cassoa_tt
		}

		prerequisite = { focus = EAC_passport }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_immigration_agency
		icon = eac_papers


		x = -4
		y = 4

		relative_position_id = EAC_east_african_community

		available = { has_idea = idea_eac_member_state }

		bypass = {
			has_global_flag = eac_immigration_agency_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_immigration_agency"
			unlock_decision_tooltip = EAC_immigration_agency_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_immigration_agency_tt
		}

		prerequisite = { focus = EAC_passport }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_the_first_summit
		icon = eac_hands


		x = 0
		y = 1

		relative_position_id = EAC_east_african_community

		available = {
			NOT = {
				has_active_mission = EAC_summit0
			}
			has_idea = idea_eac_member_state
		}

		cost = 2.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_the_first_summit"
			custom_effect_tooltip = EAC_community_influence_2_tt
			add_to_variable = { var = EAC_influence value = 2 }
		}


		prerequisite = {
			focus = EAC_east_african_community
		}

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_sectoral_committees
		icon = eac_constitution


		x = -1
		y = 2

		relative_position_id = EAC_east_african_community

		available = { has_idea = idea_eac_member_state }

		cost = 5

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_sectoral_committees"
			custom_effect_tooltip = EAC_community_influence_5_tt
			add_to_variable = { var = EAC_influence value = 5 }
		}

		prerequisite = { focus = EAC_the_first_summit }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_secretariat
		icon = eac_constitution


		x = 2
		y = 1

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		cost = 5

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_secretariat"
			custom_effect_tooltip = EAC_community_influence_3_tt
			add_to_variable = { var = EAC_influence value = 3 }
		}

		prerequisite = {
			focus = EAC_east_african_community
		}

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_council_of_ministers
		icon = eac_constitution


		x = 4
		y = 1

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		cost = 5

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_council_of_ministers"
			custom_effect_tooltip = EAC_community_influence_3_tt
			add_to_variable = { var = EAC_influence value = 3 }
		}

		prerequisite = { focus = EAC_east_african_community }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_proto_parliament
		icon = eac_democracy


		x = 1
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			custom_trigger_tooltip = {
				tooltip = EAC_50_influence_tt
				check_variable = {
					var = EAC_influence
					value > 49
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_proto_parliament_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_proto_parliament"
			unlock_decision_tooltip = EAC_proto_parliament_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_proto_parliament_tt
		}

		prerequisite = { focus = EAC_the_first_summit }

		prerequisite = { focus = EAC_secretariat }

		prerequisite = { focus = EAC_council_of_ministers }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_constitution
		icon = eac_constitution


		x = 0
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_constitution_won
		}

		cost = 15

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_constitution"
			unlock_decision_tooltip = EAC_constitution_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_constitution_tt
		}

		prerequisite = { focus = EAC_sectoral_committees }

		prerequisite = { focus = EAC_proto_parliament }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_parliament_elections
		icon = eac_constitution


		x = 0
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_parliament_elections_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_parliament_elections"
			unlock_decision_tooltip = EAC_parliament_election_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_parliament_elections_tt
		}

		prerequisite = { focus = EAC_constitution }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_begin_confederation
		icon = eac_confederation


		x = 0
		y = 5

		relative_position_id = EAC_east_african_community

		cost = 10

		available = {
			is_puppet = no
			custom_trigger_tooltip = {
				tooltip = EAC_200_influence_tt
				ROOT = {
					check_variable = { EAC_influence > 199 }
				}
			}
			has_idea = idea_eac_member_state
		}

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_begin_confederation"
			unlock_decision_tooltip = EAC_confederation_dec
		}

		prerequisite = { focus = EAC_monetary_union }

		prerequisite = { focus = EAC_parliament_elections }

		prerequisite = { focus = EAC_common_defence }


		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_the_confederation
		icon = east_african_federation_icon


		x = 0
		y = 6

		relative_position_id = EAC_east_african_community

		cost = 20

		prerequisite = {  focus = EAC_begin_confederation  }

		available = {
			is_puppet = no
			has_idea = idea_eac_confederate_leader
		}

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_the_confederation"
			if = {
				limit = {
					TNZ = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = TNZ
					autonomy_state = autonomy_confederate_state
				}
			}
			if = {
				limit = {
					KEN = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = KEN
					autonomy_state = autonomy_confederate_state
				}
			}
			if = {
				limit = {
					UGA = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = UGA
					autonomy_state = autonomy_confederate_state
				}
			}
			if = {
				limit = {
					RWA = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = RWA
					autonomy_state = autonomy_confederate_state
				}
			}
			if = {
				limit = {
					BUR = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = BUR
					autonomy_state = autonomy_confederate_state
				}
			}
			if = {
				limit = {
					SSU = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = SSU
					autonomy_state = autonomy_confederate_state
				}
			}
			if = {
				limit = {
					COM = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = COM
					autonomy_state = autonomy_confederate_state
				}
			}
			if = {
				limit = {
					ZAN = {
						has_idea = idea_eac_confederate_member
					}
				}
				set_autonomy = {
					target = ZAN
					autonomy_state = autonomy_confederate_state
				}
			}
		}

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_form_federation
		icon = east_african_federation_icon

		x = 1
		y = 7

		relative_position_id = EAC_east_african_community

		cost = 52.2

		select_effect = {}

		prerequisite = {  focus = EAC_the_confederation  }

		available = {
			is_puppet = no
			has_idea = idea_eac_confederate_leader
		}

		continue_if_invalid = yes

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
			 log = "[GetDateText]: [Root.GetName]: Focus EAC_form_federation"
			 if = {
				limit = {
					TNZ = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = TNZ
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					KEN = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = KEN
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					UGA = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = UGA
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					RWA = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = RWA
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					BUR = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = BUR
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					SSU = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = SSU
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					COM = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = COM
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					ZAN = {
						has_idea = idea_eac_confederate_member
						is_puppet_of = ROOT
					}
				}
				set_autonomy = {
					target = ZAN
					autonomy_state = autonomy_federal_state
				}
			}
			if = {
				limit = {
					tag = TNZ
				}
				news_event = eac.11
			}
			if = {
				limit = {
					tag = KEN
				}
				news_event = eac.12
			}
			if = {
				limit = {
					tag = UGA
				}
				news_event = eac.13
			}
			hidden_effect = {
				add_ideas = EAF_military_disarray
				unlock_decision_category_tooltip = EAF_federation_decisions
				load_focus_tree = east_african_federation_tree
				set_country_flag = EAF_FORMED
			}
		}

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_legislative_assembly
		icon = eac_signing

		x = 3
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_legislative_assembly_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_legislative_assembly"
			unlock_decision_tooltip = EAC_legislative_assembly_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_legislative_assembly_tt
		}

		prerequisite = { focus = EAC_council_of_ministers }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_court_of_justice
		icon = eac_justice


		x = 5
		y = 2

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_court_of_justice_won
		}

		cost = 7.2

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_court_of_justice"
			unlock_decision_tooltip = EAC_court_of_justice_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_court_of_justice_tt
		}

		prerequisite = { focus = EAC_council_of_ministers }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_police_agency
		icon = eac_justice


		x = 5
		y = 3

		relative_position_id = EAC_east_african_community

		available = {
			custom_trigger_tooltip = {
				tooltip = EAC_50_influence_tt
				check_variable = {
					var = EAC_influence
					value > 49
				}
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_police_agency_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_police_agency"
			unlock_decision_tooltip = EAC_police_agency_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_police_agency_tt
		}

		prerequisite = { focus = EAC_court_of_justice }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_common_defence
		icon = forces_east_africa


		x = 4
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			NOT = {
				has_idea = no_military
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_common_defence_won
		}

		cost = 12.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_common_defence"
			unlock_decision_tooltip = EAC_common_defence_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_common_defence_tt
		}

		prerequisite = { focus = EAC_police_agency }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_east_african_navy
		icon = eac_navy


		x = 6
		y = 4

		relative_position_id = EAC_east_african_community

		available = {
			any_owned_state = {
				is_coastal = yes
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_east_african_navy_won
		}

		cost = 10

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_east_african_navy"
			unlock_decision_tooltip = EAC_east_african_navy_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_east_african_navy_tt
		}

		prerequisite = { focus = EAC_police_agency }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_seek_partners
		icon = align_to_east_africa


		x = 8
		y = 1

		relative_position_id = EAC_east_african_community

		available = {
			has_idea = idea_eac_member_state
		}

		cost = 2.9

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_seek_partners"
			 custom_effect_tooltip = EAC_community_influence_1_tt
			 add_to_variable = { var = EAC_influence value = 1 }
		}

		prerequisite = {  focus = EAC_east_african_community  }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_lake_states
		icon = eac_expansion_1


		x = 7
		y = 2

		relative_position_id = EAC_east_african_community

		cost = 10

		available = {
			OR = {
				country_exists = RWA
				country_exists = BUR
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_lake_states_won
		}

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
 			log = "[GetDateText]: [Root.GetName]: Focus EAC_lake_states"
			unlock_decision_tooltip = EAC_lake_states_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_lake_states_tt
		}

		prerequisite = { focus = EAC_seek_partners }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_south_sudan
		icon = eac_expansion_2


		x = 9
		y = 2

		relative_position_id = EAC_east_african_community

		cost = 10

		bypass = {
			has_global_flag = eac_south_sudan_won
		}

		available = {
			country_exists = SSU
			SSU = {
				NOT = {
					has_idea = Non_State_Actor
				}
			}
			has_idea = idea_eac_member_state
		}

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus EAC_south_sudan"
			unlock_decision_tooltip = EAC_south_sudan_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_south_sudan_tt
		}

		prerequisite = { focus = EAC_seek_partners }

		ai_will_do = {
			base = 10
		}
	}

	shared_focus = {
		id = EAC_indian_ocean
		icon = eac_expansion_5


		x = 11
		y = 2

		relative_position_id = EAC_east_african_community

		cost = 10

		available = {
			OR = {
				country_exists = COM
				country_exists = ZAN
			}
			has_idea = idea_eac_member_state
		}

		bypass = {
			has_global_flag = eac_indian_ocean_won
		}

		search_filters = {FOCUS_FILTER_EAC}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus EAC_indian_ocean"
			unlock_decision_tooltip = EAC_indian_ocean_dec
			custom_effect_tooltip = EAC_voting_effects
			custom_effect_tooltip = EAC_indian_ocean_tt
		}

		prerequisite = { focus = EAC_seek_partners }

		ai_will_do = {
			base = 10
		}
	}