1) Take an svg file from wikipedia and convert it into a txt file called "locations.txt" in this folder.
2) Set up input/output/offset of x,y and name of the legislature. (input should be taken from ' width="[input_x_width]" height="[input_y_width]"> ' in the svg).
3) Run the program. Note fails are the lines it skips. You can double check these if there are any missing dots in the result.