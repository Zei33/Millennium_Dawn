PUN = {
	fleet_names_template = FLEET_NAME_PUN

	corvette = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"The Kraken" "Devil's Whore" "Somalian Executioner" "The Bloody Shame" "Neptune's Whore" "Satan's Mermaid" "Black Charlatan" "Devil's Maw" "Privateer's Death" "Rogue Wave" "Kiss Of Death" "Night's Dirty Lightning" "Devil's Room" "Monkeebutt" "Privateer's Scream" "Devil's Cursed Storm" "The Black Heart Wench" "Cursed Sea-Dog" "The Howling Lusty Wench" "Scourge Of The Seven Seas" "Murderer's Knave" "The Sea Whore" "The Hail Mary" "Privateer's Shameful Gold" "Mourning Star" "Sea's Heelish Plague" "Captain's Horrid Treasure" "The Salty Bastard" "Hell And High-Water" "Neptune's Plague" "The Evil Strumpet" "Ghostly Seadog" "The Wicked Wench" "Sea Nymph"
		}
	}
	frigate = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"The Kraken" "Devil's Whore" "Somalian Executioner" "The Bloody Shame" "Neptune's Whore" "Satan's Mermaid" "Black Charlatan" "Devil's Maw" "Privateer's Death" "Rogue Wave" "Kiss Of Death" "Night's Dirty Lightning" "Devil's Room" "Monkeebutt" "Privateer's Scream" "Devil's Cursed Storm" "The Black Heart Wench" "Cursed Sea-Dog" "The Howling Lusty Wench" "Scourge Of The Seven Seas" "Murderer's Knave" "The Sea Whore" "The Hail Mary" "Privateer's Shameful Gold" "Mourning Star" "Sea's Heelish Plague" "Captain's Horrid Treasure" "The Salty Bastard" "Hell And High-Water" "Neptune's Plague" "The Evil Strumpet" "Ghostly Seadog" "The Wicked Wench" "Sea Nymph"
		}
	}
	destroyer = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"The Kraken" "Devil's Whore" "Somalian Executioner" "The Bloody Shame" "Neptune's Whore" "Satan's Mermaid" "Black Charlatan" "Devil's Maw" "Privateer's Death" "Rogue Wave" "Kiss Of Death" "Night's Dirty Lightning" "Devil's Room" "Monkeebutt" "Privateer's Scream" "Devil's Cursed Storm" "The Black Heart Wench" "Cursed Sea-Dog" "The Howling Lusty Wench" "Scourge Of The Seven Seas" "Murderer's Knave" "The Sea Whore" "The Hail Mary" "Privateer's Shameful Gold" "Mourning Star" "Sea's Heelish Plague" "Captain's Horrid Treasure" "The Salty Bastard" "Hell And High-Water" "Neptune's Plague" "The Evil Strumpet" "Ghostly Seadog" "The Wicked Wench" "Sea Nymph"
		}
	}
	missile_submarine = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"The Kraken" "Devil's Whore" "Somalian Executioner" "The Bloody Shame" "Neptune's Whore" "Satan's Mermaid" "Black Charlatan" "Devil's Maw" "Privateer's Death" "Rogue Wave" "Kiss Of Death" "Night's Dirty Lightning" "Devil's Room" "Monkeebutt" "Privateer's Scream" "Devil's Cursed Storm" "The Black Heart Wench" "Cursed Sea-Dog" "The Howling Lusty Wench" "Scourge Of The Seven Seas" "Murderer's Knave" "The Sea Whore" "The Hail Mary" "Privateer's Shameful Gold" "Mourning Star" "Sea's Heelish Plague" "Captain's Horrid Treasure" "The Salty Bastard" "Hell And High-Water" "Neptune's Plague" "The Evil Strumpet" "Ghostly Seadog" "The Wicked Wench" "Sea Nymph"
		}
	}
	attack_submarine = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"The Kraken" "Devil's Whore" "Somalian Executioner" "The Bloody Shame" "Neptune's Whore" "Satan's Mermaid" "Black Charlatan" "Devil's Maw" "Privateer's Death" "Rogue Wave" "Kiss Of Death" "Night's Dirty Lightning" "Devil's Room" "Monkeebutt" "Privateer's Scream" "Devil's Cursed Storm" "The Black Heart Wench" "Cursed Sea-Dog" "The Howling Lusty Wench" "Scourge Of The Seven Seas" "Murderer's Knave" "The Sea Whore" "The Hail Mary" "Privateer's Shameful Gold" "Mourning Star" "Sea's Heelish Plague" "Captain's Horrid Treasure" "The Salty Bastard" "Hell And High-Water" "Neptune's Plague" "The Evil Strumpet" "Ghostly Seadog" "The Wicked Wench" "Sea Nymph"
		}
	}
	diesel_attack_submarine = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"The Kraken" "Devil's Whore" "Somalian Executioner" "The Bloody Shame" "Neptune's Whore" "Satan's Mermaid" "Black Charlatan" "Devil's Maw" "Privateer's Death" "Rogue Wave" "Kiss Of Death" "Night's Dirty Lightning" "Devil's Room" "Monkeebutt" "Privateer's Scream" "Devil's Cursed Storm" "The Black Heart Wench" "Cursed Sea-Dog" "The Howling Lusty Wench" "Scourge Of The Seven Seas" "Murderer's Knave" "The Sea Whore" "The Hail Mary" "Privateer's Shameful Gold" "Mourning Star" "Sea's Heelish Plague" "Captain's Horrid Treasure" "The Salty Bastard" "Hell And High-Water" "Neptune's Plague" "The Evil Strumpet" "Ghostly Seadog" "The Wicked Wench" "Sea Nymph"
		}
	}
	nuclear_carrier = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"Bachelor's Delight" "Royal Fortune" "Golden Hind" "Roebuck" "Queen Elizabeth's Revenge" "The Black Pearl" "Satisfaction" "Harardhere's Revenge" "The Adventure Galley" "The Golden Fleece" "CSS Puntland" "The Whydah" "The Flying Dragon" "The Rose Pink" "The Revenge" "The Fancy" "The Squirrel" "The Ranger" "The Freedom" "Happy Adventure" "The Gabriel"
		}
	}
	Carrier = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"Bachelor's Delight" "Royal Fortune" "Golden Hind" "Roebuck" "Queen Elizabeth's Revenge" "The Black Pearl" "Satisfaction" "Harardhere's Revenge" "The Adventure Galley" "The Golden Fleece" "CSS Puntland" "The Whydah" "The Flying Dragon" "The Rose Pink" "The Revenge" "The Fancy" "The Squirrel" "The Ranger" "The Freedom" "Happy Adventure" "The Gabriel"
		}
	}
	nuclear_cruiser = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"Bachelor's Delight" "Royal Fortune" "Golden Hind" "Roebuck" "Queen Elizabeth's Revenge" "The Black Pearl" "Satisfaction" "Harardhere's Revenge" "The Adventure Galley" "The Golden Fleece" "CSS Puntland" "The Whydah" "The Flying Dragon" "The Rose Pink" "The Revenge" "The Fancy" "The Squirrel" "The Ranger" "The Freedom" "Happy Adventure" "The Gabriel"
		}
	}
	LPD = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"Bachelor's Delight" "Royal Fortune" "Golden Hind" "Roebuck" "Queen Elizabeth's Revenge" "The Black Pearl" "Satisfaction" "Harardhere's Revenge" "The Adventure Galley" "The Golden Fleece" "CSS Puntland" "The Whydah" "The Flying Dragon" "The Rose Pink" "The Revenge" "The Fancy" "The Squirrel" "The Ranger" "The Freedom" "Happy Adventure" "The Gabriel"
		}
	}
	LHA = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"Bachelor's Delight" "Royal Fortune" "Golden Hind" "Roebuck" "Queen Elizabeth's Revenge" "The Black Pearl" "Satisfaction" "Harardhere's Revenge" "The Adventure Galley" "The Golden Fleece" "CSS Puntland" "The Whydah" "The Flying Dragon" "The Rose Pink" "The Revenge" "The Fancy" "The Squirrel" "The Ranger" "The Freedom" "Happy Adventure" "The Gabriel"
		}
	}
	cruiser = {
		prefix = ""
		generic = { "Pirate ship" }
		unique = {
			"Bachelor's Delight" "Royal Fortune" "Golden Hind" "Roebuck" "Queen Elizabeth's Revenge" "The Black Pearl" "Satisfaction" "Harardhere's Revenge" "The Adventure Galley" "The Golden Fleece" "CSS Puntland" "The Whydah" "The Flying Dragon" "The Rose Pink" "The Revenge" "The Fancy" "The Squirrel" "The Ranger" "The Freedom" "Happy Adventure" "The Gabriel"
		}
	}
	infantry = {
		prefix = ""
		generic = { "Pirates From Puntland" }
		generic_pattern = "UNIT_GENERIC_NAME_PUN"
		unique = { }
	}
}
