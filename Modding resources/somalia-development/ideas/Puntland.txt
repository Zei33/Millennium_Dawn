ideas = {
	country = {
		PUN_pirate_navy = {
			picture = coastal_defense_ships2
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_pirate_navy" }
			allowed_civil_war = { always = yes }
			modifier = {
				war_support_factor = 0.10
				industrial_capacity_dockyard = 0.20
			}
			equipment_bonus = {
				corvette = {
					build_cost_ic = -0.30
					instant = yes
				}
			}
		}

		PUN_mercenaries_in_government = {
			picture = german_advisors
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_mercenaries_in_government" }
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.2
			}
		}

		PUN_sea_piracy1 = {
			picture = coastal_navy
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_sea_piracy1" }
			allowed_civil_war = { always = yes }
			modifier = {
				stability_factor = -0.15
				political_power_factor = -0.1
				min_export = 0
				trade_opinion_factor = -0.1
			}
		}

		PUN_sea_piracy = {
			picture = coastal_navy
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_sea_piracy" }
			allowed_civil_war = { always = yes }
			modifier = {
				stability_factor = -0.3
				political_power_factor = -0.2
				min_export = 0
				trade_opinion_factor = -0.2
			}
		}

		PUN_widespread_money_forgery = {
			picture = great_depression
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_widespread_money_forgery" }
			allowed_civil_war = { always = yes }
			modifier = {
				industrial_capacity_factory = -0.05
				consumer_goods_factor = -0.02
			}

		}

		PUN_militias = {
			picture = inexperienced_puntish_army
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_militias" }
			allowed_civil_war = { always = yes }
			modifier = {
				stability_factor = -0.15
				war_support_factor = -0.15
				consumer_goods_factor = -0.15
			}

		}
		PUN_militias1 = {
			picture = inexperienced_puntish_army
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_militias1" }
			allowed_civil_war = { always = yes }
			modifier = {
				stability_factor = -0.1
				war_support_factor = -0.1
				consumer_goods_factor = -0.1
			}

		}
		PUN_militias2 = {
			picture = inexperienced_puntish_army
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_militias2" }
			allowed_civil_war = { always = yes }
			modifier = {
				stability_factor = -0.05
				war_support_factor = -0.05
				consumer_goods_factor = -0.05
			}
		}

		PUN_migration_crisis = {
			picture = Migrant_Crisis_Europe
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_migration_crisis" }
			allowed_civil_war = { always = yes }
			modifier = {
				stability_factor = -0.1
				monthly_population = 0.05
			}
		}

		pirate_rebuild_efforts = {
			picture = production_bonus
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pirate_rebuild_efforts" }
			allowed_civil_war = { always = yes }
			modifier = {
				industrial_capacity_dockyard = 0.2
			}
		}
		pirate_rebuild_efforts_2 = {
			picture = GFX_idea_production_bonus
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pirate_rebuild_efforts_2" }
			allowed_civil_war = { always = yes }
			modifier = {
				industrial_capacity_dockyard = 0.2
				production_speed_dockyard_factor = 0.25
			}
		}

		PUN_remnants_idea = {
			picture = construction
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_remnants_idea" }
			allowed_civil_war = { always = yes }
			modifier = {
				industry_repair_factor = 0.1
				monthly_population = 0.15
				consumer_goods_factor = 0.02 ##Burden on the people
				stability_weekly = -0.001
			}
		}

		PUN_democratisation_id = {
			picture = democracy
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_democratisation_id" }
			allowed_civil_war = { always = yes }
			modifier = {
				democratic_drift = 0.03
				democratic_acceptance = 20
			}

		}

		PUN_rebuild_idea = {
			picture = resource_production
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_rebuild_idea" }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_industrial_complex_factor = 0.1
				production_speed_infrastructure_factor = 0.1
			}
		}

		PUN_rebuild_idea1 = {
			picture = resource_production
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_rebuild_idea1" }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_industrial_complex_factor = 0.1
				production_speed_infrastructure_factor = 0.1
				industry_repair_factor = 0.1
				industry_free_repair_factor = 0.2
			}
		}

		PUN_illegal_fishing = {
			picture = liberty_ships
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_illegal_fishing" }
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = -0.1
			}

		}

		PUN_reformed_pirates = {
			picture = consumer_goods
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_reformed_pirates" }
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = 0.05
			}
		}

		PUN_supported_by_cia = {
			picture = usa_cia
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_supported_by_cia" }
			allowed_civil_war = { always = yes }
			modifier = {
				encryption_factor = 0.2
				democratic_acceptance = 20
				democratic_drift = 0.05
			}

		}

		PUN_supported_by_mss = {
			picture = chi_mss
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_supported_by_mss" }
			allowed_civil_war = { always = yes }
			modifier = {
				decryption_factor = 0.2
				communism_acceptance = 20
				communism_drift = 0.05
			}

		}

		PUN_supported_by_niss = {
			picture = eth_niss
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_supported_by_niss" }
			allowed_civil_war = { always = yes }
			modifier = {
				encryption_factor = 0.1
				decryption_factor = 0.1
				drift_defence_factor = 0.1
			}

		}

		PUN_support_id = {
			picture = resource_production
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_support_id" }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_infrastructure_factor = 0.05
				production_speed_naval_base_factor = 0.05
				production_speed_air_base_factor = 0.05
				industry_repair_factor = 0.05
			}

		}

		PUN_support_id_1 = {
			picture = resource_production
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_support_id" }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_infrastructure_factor = 0.05
				production_speed_naval_base_factor = 0.05
				production_speed_air_base_factor = 0.05
				industry_repair_factor = 0.05
				consumer_goods_factor = -0.05
			}
		}

		PUN_mubarak_id = {
			picture = mubarak_group
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PUN_support_id" }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_infrastructure_factor = 0.05
				production_speed_air_base_factor = 0.05
				production_speed_industrial_complex_factor = 0.05
			}
		}
	}
}
